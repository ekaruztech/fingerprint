@extends('admin.layout.default')

@section('page-level-css')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('layout-style')
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{ asset('assets/pages/css/profile-2.min.css') }}" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
@endsection

@section('title', 'Modify User')

@section('page-title')
<!-- BEGIN PAGE TITLE -->
<div class="page-title">
    <h1>Modify User</h1>
</div>
<!-- END PAGE TITLE -->
@endsection

@section('breadcrumb')
    <li>
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>Modify User</span>
    </li>
@stop

@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
    <!-- END PAGE HEADER-->
    <div class="profile">
        <div class="tabbable-line tabbable-full-width">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab_1_1" data-toggle="tab"> Modify User Info </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1_1">
                    <div class="row profile-account">
                        <div class="col-md-3">
                            <ul class="ver-inline-menu tabbable margin-bottom-10">
                                <li class="{{ (session('active') == 'info') ? 'active' : '' }} {{ (!session()->has('active')) ? 'active' : '' }}">
                                    <a data-toggle="tab" href="#tab_1-1">
                                        <i class="fa fa-cog"></i> User info </a>
                                    <span class="after"> </span>
                                </li>
                                <li class="{{ (session('active') == 'avatar') ? 'active' : '' }}">
                                    <a data-toggle="tab" href="#tab_2-2">
                                        <i class="fa fa-picture-o"></i> Change Avatar </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            @include('errors.errors')
                            <div class="tab-content">
                                <div id="tab_1-1" class="tab-pane {{ (session('active') == 'info') ? 'active' : '' }} {{ (!session()->has('active')) ? 'active' : '' }} )">
                                    {!! Form::model(
                                            $user, [
                                            'method'=>'PATCH',
                                            'route'=>['users.update', $hashIds->encode($user->user_id)],
                                            'class'=>'form-horizontal',
                                            'role'=>'form'
                                        ])
                                    !!}
                                        {!! Form::hidden('user_id', $user->user_id) !!}
                                        <div class="form-group">
                                            <label>Full name</label>
                                            <div class="input-icon input-icon-lg">
                                                <i class="fa fa-user"></i>
                                                <input type="text" class="form-control input-lg" required name="name" placeholder="Full Name" value="{{ $user->name }}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Email</label>
                                            <div class="input-icon input-icon-lg">
                                                <i class="fa fa-envelope"></i>
                                                <input type="email" class="form-control input-lg" required placeholder="Email" name="email" value="{{ $user->email }}"> </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Status</label>
                                            {!! Form::select('status', [''=>'Nothing Selected', '1'=>'Activate', '2'=>'Deactivate'], $user->status, ['class'=>'form-control selectpicker input-lg', 'required'=>'required']) !!}
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">User's Category</label>
                                            <select multiple class="form-control selectpicker input-lg" name="category_id[]">
                                                <option value="">Select Category</option>
                                                @foreach($categories as $category)
                                                    <optgroup label="{{ strtoupper($category->name) }}">
                                                        @if(Auth::user()->hasRole(Role::OWNER))
                                                            @if(in_array($category->category_id, $user->categories()->get()->pluck('category_id')->toArray()))
                                                                <option selected value="{{$category->category_id}}"> {{ strtoupper($category->name) }}</option>
                                                            @else
                                                                <option value="{{$category->category_id}}"> {{ strtoupper($category->name) }}</option>
                                                            @endif
                                                        @endif

                                                        @foreach($category->getImmediateDescendants() as $cat)
                                                            @if(in_array($cat->category_id, $user->categories()->get()->pluck('category_id')->toArray()))
                                                                <option selected value="{{$cat->category_id}}"> {{ strtoupper($cat->name) }}</option>
                                                            @else
                                                                <option value="{{$cat->category_id}}"> {{ strtoupper($cat->name) }}</option>
                                                            @endif

                                                            @foreach($cat->getImmediateDescendants() as $sub)
                                                                @if(in_array($sub->category_id, $user->categories()->get()->pluck('category_id')->toArray()))
                                                                    <option selected value="{{$sub->category_id}}"> > {{$sub->name}}</option>
                                                                @else
                                                                    <option value="{{$sub->category_id}}"> > {{$sub->name}}</option>
                                                                @endif

                                                                @foreach($sub->getImmediateDescendants() as $sub1)
                                                                    @if(in_array($sub1->category_id, $user->categories()->get()->pluck('category_id')->toArray()))
                                                                        <option selected value="{{$sub1->category_id}}"> > > {{$sub1->name}}</option>
                                                                    @else
                                                                        <option value="{{$sub1->category_id}}"> > > {{$sub1->name}}</option>
                                                                    @endif

                                                                    @foreach($sub1->getImmediateDescendants() as $sub2)
                                                                        @if(in_array($sub2->category_id, $user->categories()->get()->pluck('category_id')->toArray()))
                                                                            <option selected value="{{$sub2->category_id}}"> > > > {{$sub2->name}}</option>
                                                                        @else
                                                                            <option value="{{$sub2->category_id}}"> > > > {{$sub2->name}}</option>
                                                                        @endif
                                                                    @endforeach
                                                                @endforeach
                                                            @endforeach
                                                        @endforeach
                                                    </optgroup>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">User's Type: <span class="font-red">*</span></label>
                                            {!! Form::select('user_type_id', $user_types, $user->user_type_id, ['class'=>'form-control selectpicker input-lg', 'required'=>'required']) !!}
                                        </div>
                                        @if(Auth::user()->hasRole(Role::OWNER))
                                            <div class="form-group">
                                                <label class="control-label">Client: <span class="font-red">*</span></label>
                                                {!! Form::select('client_id', $clients, $user->client_id, ['class'=>'form-control selectpicker input-lg', 'required'=>'required']) !!}
                                            </div>
                                        @endif
                                        {{--<div class="form-group">--}}
                                            {{--<label class="control-label">User's Roles</label>--}}
                                            {{--<select multiple class="form-control selectpicker" name="role_id[]">--}}
                                                {{--@foreach($roles as $key => $value)--}}
                                                    {{--@if(in_array($key, $user->roles()->get()->pluck('role_id')->toArray()))--}}
                                                        {{--<option selected value="{{$key}}">{{$value}}</option>--}}
                                                    {{--@else--}}
                                                        {{--<option value="{{$key}}">{{$value}}</option>--}}
                                                    {{--@endif--}}
                                                {{--@endforeach--}}
                                            {{--</select>--}}
                                        {{--</div>--}}
                                        <div class="margiv-top-10">
                                            <button class="btn green"> Update Info</button>
                                        </div>
                                    </form>
                                </div>
                                <div id="tab_2-2" class="tab-pane {{ (session('active') == 'avatar') ? 'active' : '' }}">
                                    <form action="/users/avatar" role="form" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        {!! Form::hidden('user_id', $user->user_id) !!}
                                        <div class="form-group">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                    @if($user->avatar)
                                                        <img src="{{ $user->getAvatarPath() }}" class="img-responsive pic-bordered" alt="{{ $user->fullNames() }}"/>
                                                    @else
                                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                                    @endif
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                <div>
                                                    <span class="btn default btn-file">
                                                    <span class="fileinput-new"> Select image </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="file" name="avatar"></span>
                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="margin-top-10">
                                            <button type="submit" class="btn green"> Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!--end col-md-9-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
@endsection

@section('page-level-js')
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection

@section('layout-script')
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/users/create"]');
        });
    </script>
    <!-- END PAGE LEVEL SCRIPTS -->
@endsection



