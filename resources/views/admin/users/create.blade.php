@extends('admin.layout.default')

@section('page-level-css')
    <link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('title', 'Create User')

@section('page-title')
        <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Create User</h1>
    </div>
    <!-- END PAGE TITLE -->
@endsection

@section('breadcrumb')
    <li>
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>Create User</span>
    </li>
@stop


@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-user font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> Create User</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    @include('errors.errors')
                    <form method="POST" action="/users" accept-charset="UTF-8" class="form-horizontal" role="form">
                        {!! csrf_field() !!}
                        <div class="form-body">
                            <div class="form-group">
                                <label>Full Name: <span class="font-red">*</span></label>
                                <div class="input-icon input-icon-lg">
                                    <i class="fa fa-user"></i>
                                    <input type="text" class="form-control input-lg" required name="name" placeholder="Full Name" value="{{ old('name') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Email: <span class="font-red">*</span></label>
                                <div class="input-icon input-icon-lg">
                                    <i class="fa fa-envelope"></i>
                                    <input type="email" class="form-control input-lg" required placeholder="Email" name="email" value="{{ old('email') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">User's Category: </label>
                                <select multiple class="form-control selectpicker" name="category_id[]">
                                    <option value="">Select Category</option>
                                    @foreach($categories as $category)
                                        <optgroup label="{{ strtoupper($category->name) }}">
                                            @if(Auth::user()->hasRole(Role::OWNER_DEV))
                                                <option value="{{$category->category_id}}"> {{ strtoupper($category->name) }}</option>
                                            @endif

                                            @foreach($category->getImmediateDescendants() as $cat)
                                                <option value="{{$cat->category_id}}">{{ strtoupper($cat->name) }}</option>

                                                @foreach($cat->getImmediateDescendants() as $sub)
                                                    <option value="{{$sub->category_id}}"> > {{$sub->name}}</option>

                                                    @foreach($sub->getImmediateDescendants() as $sub1)
                                                        <option value="{{$sub1->category_id}}"> > > {{$sub1->name}}</option>

                                                        @foreach($sub1->getImmediateDescendants() as $sub2)
                                                            <option value="{{$sub2->category_id}}"> > > > {{$sub2->name}}</option>
                                                        @endforeach
                                                    @endforeach
                                                @endforeach
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">User's Type: <span class="font-red">*</span></label>
                                {!! Form::select('user_type_id', $user_types, '', ['class'=>'form-control', 'required'=>'required']) !!}
                            </div>
                            @if(Auth::user()->hasRole(Role::OWNER_DEV))
                                <div class="form-group">
                                    <label class="control-label">Client: <span class="font-red">*</span></label>
                                    {!! Form::select('client_id', $clients, '', ['class'=>'form-control', 'required'=>'required']) !!}
                                </div>
                            @else
                                {!! Form::hidden('client_id', Auth::user()->client_id, ['class'=>'form-control']) !!}
                            @endif
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue pull-right">Create</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
@endsection

@section('page-level-js')
    <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script>
@endsection

@section('layout-script')
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/users/create"]');
        });
    </script>
@endsection
