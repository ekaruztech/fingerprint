@if(Auth::check())
<!-- BEGIN PAGE TOP -->
<div class="page-top">
    <!-- BEGIN HEADER SEARCH BOX -->
    <!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
    <form class="search-form" action="#" method="GET">
        <div class="input-group">
            <input type="text" class="form-control input-sm" placeholder="Search..." name="query">
            <span class="input-group-btn">
                <a href="javascript:;" class="btn submit">
                    <i class="icon-magnifier"></i>
                </a>
            </span>
        </div>
    </form>
    <!-- END HEADER SEARCH BOX -->
    <!-- BEGIN TOP NAVIGATION MENU -->
    <div class="top-menu">
        <ul class="nav navbar-nav pull-right">
            <li class="separator hide"> </li>
            <!-- BEGIN USER LOGIN DROPDOWN -->
            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
            <li class="dropdown dropdown-user dropdown-dark">
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    @if(Auth::user()->getAvatarPath())
                        <img alt="" class="img-circle" src="{{ Auth::user()->getAvatarPath() }}" />
                    @else
                        <img alt="" class="img-circle" src="{{ asset('/uploads/no-image.jpg') }}" />
                    @endif
                    <span class="username username-hide-on-mobile">
                        {{ Auth::user()->simpleName() }}
                    </span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-default">
                    <li>
                        <a href="{{ url('/profiles') }}">
                            <i class="icon-user"></i> My Profile </a>
                    </li>
                    <li class="divider"> </li>
                    <li>
                        <a href="{{ url('/users/change') }}">
                            <i class="icon-lock"></i> Change Password </a>
                    </li>
                    <li>
                        <a href="{{ url('/logout') }}">
                            <i class="fa fa-power-off"></i> Log Out </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
<!-- END TOP NAVIGATION MENU -->
</div>
<!-- END PAGE TOP -->
@endif
