<!DOCTYPE html>
<html lang="en">
<head>
    <title>:: {{ env('APP_NAME') }} || @yield('title') ::</title>
    <meta name="viewport" content="width=device-width" />

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
        body {
            padding-top: 0 !important;
            padding-bottom: 0 !important;
            padding-top: 0 !important;
            padding-bottom: 0 !important;
            margin: 0 !important;
            width: 100% !important;
            -webkit-text-size-adjust: 100% !important;
            -ms-text-size-adjust: 100% !important;
            -webkit-font-smoothing: antialiased !important;
        }

        .tableContent img {
            border: 0 !important;
            display: block !important;
            outline: none !important;
        }

        a {
            color: #382F2E;
        }

        p, h1, h2, ul, ol, li, div {
            margin: 0;
            padding: 0;
        }

        h1, h2 {
            font-weight: normal;
            background: transparent !important;
            border: none !important;
        }

        .contentEditable h2.big, .contentEditable h1.big {
            font-size: 26px !important;
        }

        .contentEditable h2.bigger, .contentEditable h1.bigger {
            font-size: 37px !important;
        }

        td, table {
            vertical-align: top;
        }

        td.middle {
            vertical-align: middle;
        }

        a.link1 {
            font-size: 13px;
            color: #27A1E5;
            line-height: 24px;
            text-decoration: none;
        }

        a {
            text-decoration: none;
        }

        .link2 {
            color: #ffffff;
            border-top: 10px solid #27A1E5;
            border-bottom: 10px solid #27A1E5;
            border-left: 18px solid #27A1E5;
            border-right: 18px solid #27A1E5;
            border-radius: 3px;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            background: #27A1E5;
        }

        .link3 {
            color: #555555;
            border: 1px solid #cccccc;
            padding: 10px 18px;
            border-radius: 3px;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            background: #ffffff;
        }

        .link4 {
            color: #27A1E5;
            line-height: 24px;
        }

        h2, h1 {
            line-height: 20px;
        }

        p {
            font-size: 14px;
            line-height: 21px;
            color: #AAAAAA;
        }

        .contentEditable li {

        }

        .appart p {

        }

        .bgItem {
            background: #ffffff;
        }

        .bgBody {
            background: #ffffff;
        }

        img {
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
            width: auto;
            max-width: 100%;
            clear: both;
            display: block;
            float: none;
        }

    </style>


    <script type="colorScheme" class="swatch active">
{
    "name":"Default",
    "bgBody":"ffffff",
    "link":"27A1E5",
    "color":"AAAAAA",
    "bgItem":"ffffff",
    "title":"444444"
}





    </script>
</head>
<body bgcolor="#FFFFFF" style="font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; margin:0; padding:0;">

<table style="width: 100%;">
    <tr>
        <td></td>
        <td class="header container" style="display:block!important; max-width:800px!important; margin:0 auto!important; clear:both!important;">

            <div class="content" style="padding:15px; max-width:800px; margin:0 auto; display:block;">
                <table bgcolor="#EAE5EF">
                    <tr>
                        <td style="color: white; font-size: xx-large">
                            <a target='_blank' href="{{ env('APP_URL') }}">
                                <img src="{{ $message->embed(realpath('assets/layouts/layout/img/logo-big.png')) }}"
                                        alt="Logo" width='378' data-default="placeholder" data-max-width="800">
                            </a>
                        </td>
                    </tr>
                </table>
            </div>

        </td>
        <td></td>
    </tr>
</table><!-- /HEADER -->
<table class="body-wrap" style="width: 100%;">
    <tr>
        <td></td>
        <td class="container" style="display:block!important; max-width:600px!important; margin:0 auto!important; clear:both!important;" bgcolor="#FFFFFF">

            <div class="content" style="padding:15px; max-width:600px; margin:0 auto; display:block;">
                <table>
                    <tr>
                        <td>
                            <div class="row">
                                @yield('content')
                            </div>
                            <!-- Callout Panel -->
                            {{--<a style="text-decoration: none;" href="{{ env('APP_URL') }}">--}}
                                {{--<p class="callout" style="margin-bottom: 16px; font-weight: normal; font-size:14px; line-height:1.6; padding:15px; background-color:#ECF8FF;">--}}
                                    {{--Click Here To Access The Application--}}
                                {{--</p>--}}
                            {{--</a>--}}
                            {{--<p style="margin-bottom: 15px; font-weight: normal; font-size:14px; line-height:1.6; padding:15px; background-color:#ECF8FF;">--}}
                                {{--<br>Date and Time: {{ date('g:ia \o\n l jS F Y') }}--}}
                                {{--<br>Note: This is an auto generated email. if you did not initiate this action kindly ignore this mail.--}}
                                {{--<br>We recommend that you contact us at--}}
                                {{--<a href="{{ env('DEVELOPER_SITE_ADDRESS') }}">{{ env('DEVELOPER_SITE_NAME') }}</a> for further assistance.--}}
                            {{--</p>--}}
                            <!-- /Callout Panel -->

                            <!-- social & contact -->
                            <table class="social" width="100%">
                                <tr>
                                    <td>

                                        <!-- column 2 -->
                                        <table align="left" class="column">
                                            <tr>
                                                <td>
                                                    <h4>
                                                        Should you need assistance in any form, do send us an email at
                                                        <a href="emailto:{{ env('CLIENT_SITE_EMAIL') }}">{{ env('CLIENT_SITE_EMAIL') }}</a>
                                                        or reach out to us via any of our social media accounts.
                                                    </h4>

                                                    <h5 class="">Contact Info:</h5>
                                                    <p style="margin-bottom: 10px; font-weight: normal; font-size:14px; line-height:1.6;">
                                                        Email: <strong><a href="emailto:{{ env('CLIENT_SITE_EMAIL') }}">{{ env('CLIENT_SITE_EMAIL') }}</a></strong><br>
                                                        Mobile(s): <strong>{{ env('CLIENT_SITE_NUMBER') }}</strong>
                                                    </p>
                                                </td>
                                            </tr>
                                        </table><!-- /column 2 -->

                                        <span class="clear" style="display: block; clear: both;"></span>

                                    </td>
                                </tr>
                            </table><!-- /social & contact -->

                        </td>
                    </tr>
                </table>
            </div><!-- /content -->

        </td>
        <td></td>
    </tr>
</table><!-- /BODY -->

<!-- FOOTER -->
<table class="footer-wrap" style="width: 100%;	clear:both!important;">
    <tr>
        <td></td>
        <td class="container" style="display:block!important; max-width:600px!important; margin:0 auto!important; clear:both!important;">

            <!-- content -->
            <div class="content" style="padding:15px; max-width:600px; margin:0 auto; display:block;">
                <table>
                    <tr>
                        <td align="center">
                            <p style="border-top: 1px solid rgb(215,215,215); padding-top:15px; font-size:14px; font-weight: bold;">
                                This email was sent by {{ env('APP_NAME') }}.<br>
                                {{--<a style="color: #009CB3;" href="#">Terms</a> |--}}
                                {{--<a style="color: #009CB3;" href="#">Privacy</a> |--}}
                                {{--<a style="color: #009CB3;" href="#"><unsubscribe>Unsubscribe</unsubscribe></a>--}}
                                &copy; {{ date("Y") }} <a href="{{ env('APP_URL') }}">{{ env('APP_NAME') }}</a> All rights reserved
                            </p>
                        </td>
                    </tr>
                </table>
            </div><!-- /content -->

        </td>
        <td></td>
    </tr>
</table><!-- /FOOTER -->

</body>
</html>