@extends('admin.layout.default')

@section('page-level-css')
    <link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('title', 'Manage Category Level Five')

@section('page-title')
        <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Manage Category: Level Five</h1>
    </div>
    <!-- END PAGE TITLE -->
@endsection

@section('breadcrumb')
    <li>
        <a href="{{ url('/') }}">Home</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="{{ url('/categories') }}">Categories</a>
        <i class="fa fa-circle"></i>
    </li>
    <li><span class="active">Level Five</span></li>
@stop


@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-list font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Category Level Five</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12 margin-bottom-10">
                            <div class="btn-group">
                                <button class="btn green add_category"> Add New
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-12">
                            {!! Form::open([
                                   'method'=>'POST',
                                   'class'=>'form-horizontal',
                                   'role'=>'form',
                                ])
                            !!}
                            <div class="table-responsive">
                                <table class="table table-bordered" id="category_table">
                                    <thead>
                                    <tr>
                                        <th style="width: 10%;" class="text-center">#</th>
                                        <th style="width: 45%;">Category Level Five</th>
                                        <th style="width: 30%;">Parent Level</th>
                                        <th style="width: 15%;">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($level_fours) > 0)
                                        <?php $i = 1; ?>
                                        @foreach($level_fours as $level_four)
                                            @foreach($level_four->getImmediateDescendants() as $category)
                                                <tr>
                                                    <td class="text-center">{{$i++}} </td>
                                                    <td>
                                                        {!! Form::text('name[]', $category->name, ['placeholder'=>'Category Name', 'class'=>'form-control', 'required'=>'required']) !!}
                                                        {!! Form::hidden('category_id[]', $category->category_id, ['class'=>'form-control']) !!}
                                                    </td>
                                                    <td>
                                                        <select name="parent_id[]" class="bs-select form-control" required>
                                                            <option value="">Select Parent Level</option>
                                                            @foreach($level_threes as $type)
                                                                @if(count($type->getImmediateDescendants()) > 0)
                                                                    <optgroup label="{{ $type->parent()->first()->name }} > {{ $type->name }}">
                                                                        @foreach($type->getImmediateDescendants() as $cat)
                                                                            @if($category->parent_id == $cat->category_id)
                                                                                <option selected value="{{ $cat->category_id }}">{{ $cat->name }}</option>
                                                                            @else
                                                                                <option value="{{ $cat->category_id }}">{{ $cat->name }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </optgroup>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-danger btn-rounded btn-condensed btn-xs delete_category">
                                                            <span class="fa fa-trash-o"></span> Delete
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endforeach
                                        @if($sub < 1)
                                            <tr>
                                                <td class="text-center">1</td>
                                                <td>
                                                    {!! Form::text('name[]', '', ['placeholder'=>'Category Name', 'class'=>'form-control', 'required'=>'required']) !!}
                                                    {!! Form::hidden('category_id[]', '-1', ['class'=>'form-control']) !!}
                                                </td>
                                                <td>
                                                    <select name="parent_id[]" class="bs-select form-control" required="required" data-live-search="true" data-size="8">
                                                        <option value="">Select Parent Level</option>
                                                        @foreach($level_threes as $type)
                                                            @if(count($type->getImmediateDescendants()) > 0)
                                                                <optgroup label="{{ $type->parent()->first()->name }} > {{ $type->name }}">
                                                                    @foreach($type->getImmediateDescendants() as $category)
                                                                        <option value="{{ $category->category_id }}">{{ $category->name }}</option>
                                                                    @endforeach
                                                                </optgroup>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                <td>
                                                    <button type="button" class="btn btn-danger btn-rounded btn-condensed btn-xs remove_category">
                                                        <span class="fa fa-times"></span> Remove
                                                    </button>
                                                </td>
                                            </tr>
                                        @endif
                                    @endif
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th style="width: 10%;" class="text-center">#</th>
                                        <th style="width: 45%;">Category Level Five</th>
                                        <th style="width: 30%;">Parent Level</th>
                                        <th style="width: 15%;">Actions</th>
                                    </tr>
                                    </tfoot>
                                </table>
                                <div class="form-actions noborder">
                                    <button class="btn green pull-left add_category"> Add New
                                        <i class="fa fa-plus"></i>
                                    </button>
                                    <button type="submit" class="btn blue pull-right">Submit</button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END PAGE BASE CONTENT -->
@endsection

@section('page-level-js')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{{ asset('assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ asset('assets/pages/scripts/ui-bootbox.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@endsection

@section('layout-script')
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="{{ asset('assets/custom/js/categories/levels.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/categories/level-5"]');
        });
    </script>
@endsection