@extends('admin.layout.default')

@section('page-level-css')
    <link href="{{ asset('assets/global/plugins/jstree/dist/themes/default/style.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('title', 'Display Categories')

@section('page-title')
        <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Display Categories: Levels By Levels</h1>
    </div>
    <!-- END PAGE TITLE -->
@endsection

@section('breadcrumb')
    <li>
        <a href="{{ url('/') }}">Home</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li><span class="active">Categories</span></li>
@stop


@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-blue-sharp"></i>
                        <span class="caption-subject font-green bold uppercase">Display Categories</span>
                    </div>
                </div>
                <div id="categories_tree" class="tree-demo">
                    <ul>
                        @if(count($categories) > 0)
                            <!-- BEGIN LEVEL ONE -->
                            @foreach($categories as $one)
                                @if(count($one->getImmediateDescendants()) > 0)
                                    <li data-jstree='{ "opened" : true, "type" : "one" }'> {{ $one->name }}
                                        <ul>
                                            <!-- BEGIN LEVEL TWO -->
                                            @foreach($one->getImmediateDescendants() as $two)
                                                @if(count($two->getImmediateDescendants()) > 0)
                                                    <li data-jstree='{ "opened" : true, "type" : "two" }'> {{ $two->name }}
                                                        <ul>
                                                            <!-- BEGIN LEVEL THREE -->
                                                            @foreach($two->getImmediateDescendants() as $three)
                                                                @if(count($three->getImmediateDescendants()) > 0)
                                                                    <li data-jstree='{ "opened" : true, "type" : "three" }'> {{ $three->name }}
                                                                        <ul>
                                                                            <!-- BEGIN LEVEL FOUR -->
                                                                            @foreach($three->getImmediateDescendants() as $four)
                                                                                @if(count($four->getImmediateDescendants()) > 0)
                                                                                    <li data-jstree='{ "opened" : true, "type" : "four" }'> {{ $four->name }}
                                                                                        <ul>
                                                                                            <!-- BEGIN LEVEL FIVE -->
                                                                                            @foreach($four->getImmediateDescendants() as $five)
                                                                                                <li data-jstree='{ "opened" : true, "type" : "five" }'> {{ $five->name }}</li>
                                                                                            @endforeach
                                                                                            <!-- END LEVEL FIVE -->
                                                                                        </ul>
                                                                                    </li>
                                                                                @else
                                                                                    <li data-jstree='{ "type" : "file" }'> {{ $four->name }}</li>
                                                                                @endif
                                                                            @endforeach
                                                                            <!-- END LEVEL FOUR -->
                                                                        </ul>
                                                                    </li>
                                                                @else
                                                                    <li data-jstree='{ "type" : "file" }'> {{ $three->name }}</li>
                                                                @endif
                                                            @endforeach
                                                            <!-- END LEVEL THREE -->
                                                        </ul>
                                                    </li>
                                                @else
                                                    <li data-jstree='{ "type" : "file" }'> {{ $two->name }}</li>
                                                @endif
                                            @endforeach
                                            <!-- END LEVEL TWO -->
                                        </ul>
                                    </li>
                                @else
                                    <li data-jstree='{ "type" : "file" }'> {{ $one->name }}</li>
                                @endif
                            @endforeach
                            <!-- END LEVEL ONE -->
                            @for($i = 0; $i <= $max; $i++)
                                <li data-jstree='{ "type" : "add" }'>
                                    <a href="/categories/level-{{ $i + 1 }}"> Add New Level {{ $i + 1 }}</a>
                                </li>
                            @endfor
                        @else
                            <li data-jstree='{ "type" : "add" }'>
                                <a href="/categories/level-1"> No Category was created...create some!</a>
                            </li>
                        @endif
                        {{--<li> Root node 1--}}
                            {{--<ul>--}}
                                {{--<li data-jstree='{ "selected" : true }'>--}}
                                    {{--<a href="javascript:;"> Initially selected </a>--}}
                                {{--</li>--}}
                                {{--<li data-jstree='{ "icon" : "fa fa-briefcase icon-state-success " }'> custom icon URL </li>--}}
                                {{--<li data-jstree='{ "opened" : true }'> initially open--}}
                                    {{--<ul>--}}
                                        {{--<li data-jstree='{ "disabled" : true }'> Disabled Node </li>--}}
                                        {{--<li data-jstree='{ "type" : "file" }'> Another node </li>--}}
                                    {{--</ul>--}}
                                {{--</li>--}}
                                {{--<li data-jstree='{ "icon" : "fa fa-warning icon-state-danger" }'> Custom icon class (bootstrap) </li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                        {{--<li data-jstree='{ "type" : "file" }'>--}}
                            {{--<a href="http://www.jstree.com"> Clickanle link node </a>--}}
                        {{--</li>--}}
                    </ul>

                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END PAGE BASE CONTENT -->
@endsection

@section('page-level-js')
    <script src="{{ asset('assets/global/plugins/jstree/dist/jstree.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@endsection

@section('layout-script')
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="{{ asset('assets/custom/js/categories/levels.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/categories"]');
            UITree.init();
        });
    </script>
@endsection