@extends('admin.layout.default')

@section('page-level-css')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('layout-style')
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{ asset('assets/pages/css/profile-2.min.css') }}" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
@endsection

@section('title', 'Edit Profile')

@section('page-title')
        <!-- BEGIN PAGE TITLE -->
<div class="page-title">
    <h1>Modify Profile</h1>
</div>
<!-- END PAGE TITLE -->
@endsection

@section('breadcrumb')
    <li>
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>Modify Profile</span>
    </li>
@stop


@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
        <!-- END PAGE HEADER-->
    <div class="profile">
        <div class="tabbable-line tabbable-full-width">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab_1_1" data-toggle="tab"> Edit Profile </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1_1">
                    <div class="row profile-account">
                        <div class="col-md-3">
                            <ul class="ver-inline-menu tabbable margin-bottom-10">
                                <li class="{{ (session('active') == 'info') ? 'active' : '' }} {{ (!session()->has('active')) ? 'active' : '' }}">
                                    <a data-toggle="tab" href="#tab_1-1">
                                        <i class="fa fa-cog"></i> User info </a>
                                    <span class="after"> </span>
                                </li>
                                @if(!Auth::user()->hasRole(Role::USER))
                                    <li class="{{ (session('active') == 'avatar') ? 'active' : '' }}">
                                        <a data-toggle="tab" href="#tab_2-2">
                                            <i class="fa fa-picture-o"></i> Change Avatar </a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                        <div class="col-md-7">
                            @include('errors.errors')
                            <div class="tab-content">
                                <div id="tab_1-1" class="tab-pane {{ (session('active') == 'info') ? 'active' : '' }} {{ (!session()->has('active')) ? 'active' : '' }} )">
                                    <form method="post" action="/profiles/edit" role="form" class="form-horizontal">
                                        <div class="form-body">
                                            {!! csrf_field() !!}
                                            <div class="caption font-green-haze">
                                                <span class="caption-subject bold uppercase">Basic Information</span>
                                                <hr>
                                            </div>
                                        </div>
                                        @if(!($record))
                                            <div class="form-group">
                                                <label>Full name</label>
                                                <div class="input-icon input-icon-lg">
                                                    <i class="fa fa-user"></i>
                                                    <input type="text" class="form-control input-lg" required name="name" placeholder="Full Name" value="{{ $user->name }}">
                                                </div>
                                            </div>
                                        @endif

                                        <div class="form-group">
                                            <label>Email</label>
                                            <div class="input-icon input-icon-lg">
                                                <i class="fa fa-envelope"></i>
                                                <input type="email" class="form-control input-lg" required placeholder="Email" disabled name="email" value="{{ $user->email }}"> </div>
                                        </div>

                                        @if($record)
                                            <div class="form-body">
                                                <input type="hidden" name="record_id" value="{{$record->_id}}">
                                                <div class="form-group">
                                                    <label>First Name: <span class="font-red">*</span></label>
                                                    <input type="text" class="form-control" required name="first_name" placeholder="First Name" value="{{ $record->first_name }}">
                                                </div>
                                                <div class="form-group">
                                                    <label>Last Name: <span class="font-red">*</span></label>
                                                    <input type="text" class="form-control" required name="last_name" placeholder="Last Name" value="{{ $record->last_name }}">
                                                </div>
                                                <div class="form-group">
                                                    <label>Other Name: </label>
                                                    <input type="text" class="form-control" name="other_name" placeholder="Other Name" value="{{ $record->other_name }}">
                                                </div>
                                                <div class="form-group">
                                                    <label>Personal I.D: <span class="font-red">*</span></label>
                                                    <input type="text" class="form-control" required name="unique_id" placeholder="Personal I.D" value="{{ $record->unique_id }}">
                                                </div>
                                                <div class="form-group">
                                                    <label>Gender: <span class="font-red">*</span></label>
                                                    {!! Form::select('gender', [''=>'Nothing Selected', 'Male'=>'Male', 'Female'=>'Female'], $record->gender, ['class'=>'form-control selectpicker', 'required'=>'required']) !!}
                                                </div>
                                                <div class="form-group">
                                                    <label>Date of Birth: <span class="font-red">*</span></label>
                                                    <input type="text" class="form-control date-picker" required data-date-format="yyyy-mm-dd" name="dob"
                                                           value="{{ ($record->dob) ? $record->dob->format('Y-m-d') : '' }}" placeholder="Date of Birth">
                                                </div>
                                                <div class="form-group">
                                                    <label>Mobile Number: </label>
                                                    <input type="text" class="form-control" name="phone_no" placeholder="Mobile Number" value="{{ $record->phone_no }}">
                                                </div>

                                                @if(count($categories) > 0)
                                                    <div class="form-group">
                                                        <label class="control-label">Category: <span class="font-red">*</span></label>
                                                        <select class="form-control category-levels" required name="category_id">
                                                            <option value="">Nothing Selected</option>
                                                            @foreach($categories as $category)
                                                                <option value="{{$category->category_id}}">{{$category->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group"> </div>
                                                    <div class="form-group"> </div>
                                                    <div class="form-group"> </div>
                                                    <div class="form-group"> </div>
                                                    <div class="form-group"> </div>
                                                @endif<hr>
                                                <div class="caption font-green-haze">
                                                    <span class="caption-subject bold uppercase">Other Information</span>
                                                    <hr>
                                                </div>
                                                @if($builder)
                                                    @foreach($builder->schema as $element)
                                                        <?php $required = (isset($element['required']) and $element['required']) ? true : false; ?>
                                                        <div class="form-group">
                                                            <label>{{ $element['label'] }}: {!! ($required) ? '<span class="font-red">*</span>' : '' !!}</label>
                                                            @if($element['type'] == 'number' || $element['type'] == 'text')
                                                                <input type="{{$element['type']}}" class="form-control" name="data[{{$element['name']}}]"
                                                                       value="{{ $record->data[$element['name']] }}" {!! ($required) ? 'required' : '' !!} placeholder="{{$element['label']}}">
                                                            @elseif($element['type'] == 'date')
                                                                <input type="text" class="form-control date-picker" data-date-format="yyyy-mm-dd" name="data[{{$element['name']}}]"
                                                                       value="{{ $record->data[$element['name']] }}" {!! ($required) ? 'required' : '' !!} placeholder="{{$element['label']}}">
                                                            @elseif($element['type'] == 'radio-group')
                                                                <div class="radio-list">
                                                                    @foreach($element['values'] as $radio)
                                                                        <label class="radio-inline">
                                                                            <span><input type="radio" {{ ($record->data[$element['name']] == $radio['value']) ? 'checked' : '' }}
                                                                                     value="{{$radio['value']}}" name="data[{{$element['name']}}]"></span> {{$radio['label']}}
                                                                        </label>
                                                                    @endforeach
                                                                </div>
                                                            @elseif($element['type'] == 'checkbox-group')
                                                                <div class="checkbox-list">
                                                                    @foreach($element['values'] as $checkbox)
                                                                        <label class="checkbox-inline">
                                                                            <span><input {{ (in_array($checkbox['value'], $record->data[$element['name']])) ? 'checked' : '' }}
                                                                                     type="checkbox" value="{{$checkbox['value']}}" name="data[{{$element['name']}}][]"></span>
                                                                            {{$checkbox['label']}}
                                                                        </label>
                                                                    @endforeach
                                                                </div>
                                                            @elseif($element['type'] == 'select')
                                                                <select  class="form-control selectpicker" name="data[{{$element['name']}}]" >
                                                                    <option value="">{{ $element['label'] }}</option>
                                                                    @foreach($element['values'] as $option)
                                                                        @if($record->data[$element['name']] == $option['value'])
                                                                            <option selected value="{{$option['value']}}"> {{$option['label']}} </option>
                                                                        @else
                                                                            <option value="{{$option['value']}}"> {{$option['label']}} </option>
                                                                        @endif
                                                                    @endforeach
                                                                </select>
                                                            @elseif($element['type'] == 'textarea')
                                                                <textarea class="form-control" rows="4" name="data[{{$element['name']}}]" {!! ($required) ? 'required' : '' !!}
                                                                placeholder="{{$element['label']}}">{{ $record->data[$element['name']] }}</textarea>
                                                            @endif
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        @endif
                                        <div class="margiv-top-10">
                                            <button class="btn green"> Update Info</button>
                                        </div>
                                    </form>
                                </div>
                                @if(!Auth::user()->hasRole(Role::USER))
                                    <div id="tab_2-2" class="tab-pane {{ (session('active') == 'avatar') ? 'active' : '' }}">
                                        <form action="/profiles/avatar" role="form" method="post" enctype="multipart/form-data">
                                            <div class="form-group">
                                                {{ csrf_field() }}
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        @if($user)
                                                            <img src="{{ $user->getAvatarPath() }}" class="img-responsive pic-bordered" alt="{{ $user->fullNames() }}"/>
                                                        @else
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                                        @endif
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                        <span class="fileinput-new"> Select image </span>
                                                        <span class="fileinput-exists"> Change </span>
                                                        <input type="file" name="avatar"></span>
                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="margin-top-10">
                                                <button type="submit" class="btn green"> Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <!--end col-md-9-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
@endsection

@section('page-level-js')
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection

@section('layout-script')
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/custom/js/users/profile.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/profiles/edit"]');
        });
    </script>
    <!-- END PAGE LEVEL SCRIPTS -->
@endsection