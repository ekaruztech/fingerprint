@extends('admin.layout.default')

@section('page-level-css')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
@endsection
@section('layout-style')
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{ asset('assets/pages/css/profile-2.min.css') }}" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
@endsection

@section('title', 'View Profile')

@section('page-title')
<!-- BEGIN PAGE TITLE -->
<div class="page-title">
    <h1>My Profile</h1>
</div>
<!-- END PAGE TITLE -->
@endsection

@section('breadcrumb')
    <li>
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>My Profile</span>
    </li>
@stop

@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
    <!-- END PAGE HEADER-->
    <div class="profile">
        <div class="tabbable-line tabbable-full-width">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab_1_1" data-toggle="tab"> Overview </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1_1">
                    <div class="row">
                        <div class="col-md-3">
                            <ul class="list-unstyled profile-nav">
                                <li>
                                    @if(!$user->getAvatarPath())
                                        <img src="{{ asset('/uploads/no-image.jpg') }}" class="img-responsive pic-bordered" alt="{{ $user->fullNames() }}"/>
                                    @else
                                        <img src="{{ $user->getAvatarPath() }}" class="img-responsive pic-bordered" alt="{{ $user->fullNames() }}"/>
                                    @endif
                                    <a href="{{ url('/profiles/edit') }}" class="profile-edit"> edit </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-8 profile-info">
                                    <h1 class="font-green sbold uppercase">{{ $user->fullNames() }}</h1>
                                    <h4>
                                        {{ $user->userType()->first()->user_type }}
                                    </h4>
                                    <ul class="list-inline">
                                        <li>
                                            <i class="fa fa-map-marker"></i> Nigeria
                                        </li>
                                        <li>
                                            <i class="fa fa-envelope"></i> {{ $user->email }}
                                        </li>
                                    </ul>
                                    <div class="portlet sale-summary">
                                        <div class="portlet-title">
                                            <div class="caption font-red sbold"> Account Information
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <table class="table table-stripped table-bordered">
                                                <tr>
                                                    <td>Name</td>
                                                    <td>{{ $user->name }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td>{{ $user->email }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Status</td>
                                                    <td>{!! ($user->status == 1) ? '<span class="label label-success">Activated</span>' : '<span class="label label-danger">Deactivated</span>' !!}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    @if($record)
                                        <div class="portlet sale-summary">
                                            <div class="portlet-title">
                                                <div class="caption font-red sbold"> Record Information
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <table class="table table-stripped table-bordered">
                                                    <tr>
                                                        <th>Full Name</th>
                                                        <td>{{ $record->fullNames() }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Personal I.D</th>
                                                        <td>{!! ($record->unique_id) ? $record->unique_id : '<span class="label label-danger">nil</span>' !!}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Gender</th>
                                                        <td>{!! ($record->gender) ? $record->gender : '<span class="label label-danger">nil</span>' !!}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Date of Birth</th>
                                                        <td>{!! ($record->dob) ? $record->dob->format('jS M, Y') : '<span class="label label-danger">nil</span>' !!}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Age</th>
                                                        <td>{!! ($record->dob) ? $record->dob->age : '<span class="label label-danger">nil</span>' !!}</td>
                                                    </tr>
                                                    <tr>
                                                        <t>Mobile Number</t>
                                                        <td>{!! ($record->phone_no) ? $record->phone_no : '<span class="label label-danger">nil</span>' !!}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Verification Code: </th>
                                                        <td>{!! ($record->hashed_id) ? $record->hashed_id : '<span class="label label-danger">nil</span>' !!}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>{!! ($record->category()) ? $record->category()->parent()->first()->name : 'Category' !!}</th>
                                                        <td>{!! ($record->category()) ? $record->category()->name : '<span class="label label-danger">nil</span>' !!}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
@endsection

@section('page-level-js')
    <script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection

@section('layout-script')
    <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/gmaps/gmaps.min.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/profiles"]');
        });
    </script>
@endsection
