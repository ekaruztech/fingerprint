@extends('admin.layout.default')

@section('page-level-css')
<!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/global/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
@endsection
@section('layout-style')
<!-- BEGIN PAGE LEVEL STYLES -->
    <link href="{{ asset('assets/pages/css/profile-2.min.css') }}" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
@endsection

@section('title', 'Record Details')

@section('page-title')
<!-- BEGIN PAGE TITLE -->
<div class="page-title">
    <h1>Record Details</h1>
</div>
<!-- END PAGE TITLE -->
@endsection

@section('breadcrumb')
    <li>
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="{{ url('/records') }}">Records</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>Record Details</span>
    </li>
@stop


@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
    <!-- END PAGE HEADER-->
    <div class="profile">
        <div class="tabbable-line tabbable-full-width">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab_1_1" data-toggle="tab"> Overview </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1_1">
                    <div class="row">
                        <div class="col-md-3">
                            <ul class="list-unstyled profile-nav">
                                <li>
                                    @if($record->getAvatarPath())
                                        <img src="{{ $record->getAvatarPath() }}" class="img-responsive pic-bordered" alt="{{ $record->fullNames() }}"/>
                                        <a href="{{ $record->getAvatarPath() }}" class="btn btn-circle btn-sm btn-info"> <i class="fa fa-download"></i> Download Image</a>
                                    @else
                                        <img src="{{ asset('/uploads/no-image.jpg') }}" class="img-responsive pic-bordered" alt="{{ $record->fullNames() }}"/>
                                    @endif
                                    <a href="{{ url('/records/'.$record->_id.'/edit') }}" class="profile-edit"> edit </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-8 profile-info">
                                    <h1 class="font-green sbold uppercase">{{ $record->fullNames() }}</h1>
                                    {!! ($record->unique_id) ? '<h3 class="font-green sbold uppercase">' . $record->unique_id . '</h3>' : '' !!}
                                    <ul class="list-inline">
                                        <li> <i class="fa fa-envelope"></i> {!! ($record->email) ? $record->email : '' !!} </li>
                                        <li><i class="fa fa-phone-square"></i> {!! ($record->phone_no) ? $record->phone_no : '' !!} </li>
                                    </ul>
                                    <div class="portlet sale-summary">
                                        <div class="portlet-title">
                                            <div class="caption font-red sbold"> Record Information
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <table class="table table-stripped table-bordered">
                                                <tr>
                                                    <th>Full Names: </th>
                                                    <td>{{ $record->fullNames() }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Gender: </th>
                                                    <td>{{ $record->gender }}</td>
                                                </tr>
                                                <tr>
                                                    <th>I.D: </th>
                                                    <td>{!! ($record->unique_id) ? $record->unique_id : '<span class="label label-danger">nil</span>' !!}</td>
                                                </tr>
                                                <tr>
                                                    <th>Date of Birth: </th>
                                                    <td>
                                                        {!! ($record->dob) ? $record->dob : '<span class="label label-danger">nil</span>' !!}
                                                        {!! ($record->dob) ? $record->dob->age . 'Years' : '' !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Email Address: </th>
                                                    <td>{!! ($record->email) ? $record->email : '<span class="label label-danger">nil</span>' !!}</td>
                                                </tr>
                                                <tr>
                                                    <th>Mobile Number: </th>
                                                    <td>{!! ($record->phone_no) ? $record->phone_no : '<span class="label label-danger">nil</span>' !!}</td>
                                                </tr>
                                                <tr>
                                                    <th>Category: </th>
                                                    <td>{!! ($record->category_id) ? $record->category()->name : '<span class="label label-danger">nil</span>' !!}</td>
                                                </tr>
                                                @if(isset($record->subjects))
                                                    <tr>
                                                        <th>Subject Combinations</th>
                                                        <td>
                                                            @foreach($record->subjects as $value)
                                                                <span style="padding-right: 20px;"><i class="fa fa-book"></i> {{ $value }}</span>
                                                            @endforeach
                                                        </td>
                                                    </tr>
                                                @endif
                                                <tr>
                                                    <th>Verification Code: </th>
                                                    <td>{!! ($record->hashed_id) ? $record->hashed_id : '<span class="label label-danger">nil</span>' !!}</td>
                                                </tr>
                                                <tr>
                                                    <th>{!! ($record->client_id) ? $record->client()->categoryType()->first()->category_type : 'Client' !!} : </th>
                                                    <td>{!! ($record->client_id) ? $record->client()->name : '<span class="label label-danger">nil</span>' !!}</td>
                                                </tr>
                                                <tr>
                                                    <th>Status</th>
                                                    <td>{!! ($record->status == 1) ? '<span class="label label-success">Activated</span>' : '<span class="label label-danger">Deactivated</span>' !!}</td>
                                                </tr>
                                                @if(isset($record->data))
                                                    @foreach($record->data as $data => $value)
                                                        <tr>
                                                            <th>{{ $data }}</th>
                                                            <td>
                                                                @if(is_array($value))
                                                                    @foreach($value as $val)
                                                                        <span class="font-blue"><i class="fa fa-tag"></i> {{ $val }}</span>
                                                                    @endforeach
                                                                @else
                                                                    {!! ($value) ? $value: '<span class="label label-danger">nil</span>' !!}
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row widget-row">
        <div class="col-md-5">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Log Types</span>
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="javascript:;" class="fullscreen"> </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="log_types" class="chart"> </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <!-- BEGIN CHART PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-green-haze"></i>
                        <span class="caption-subject bold uppercase font-green-haze"> Logs Based on Log Types</span>
                        <span class="caption-helper">Summary of User's Log</span>
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a href="javascript:;" class="fullscreen"> </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="logs_records" class="chart" style="height: 400px;"> </div>
                </div>
            </div>
            <!-- END CHART PORTLET-->
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
@endsection

@section('page-level-js')
    <script src="{{ asset('assets/global/plugins/flot/jquery.flot.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/flot/jquery.flot.pie.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/global/plugins/amcharts/amcharts/amcharts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/amcharts/amcharts/serial.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/amcharts/amcharts/themes/light.js') }}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->

    <script src="{{ asset('assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/custom/js/logs/log.js') }}" type="text/javascript"></script>
@endsection

@section('layout-script')
    <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/gmaps/gmaps.min.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/records"]');

            ChartsUserLogTypes.initPieCharts('<?php echo $record->_id; ?>');
            ChartsAmchartsUsers.init('<?php echo $record->_id; ?>');
        });
    </script>
@endsection
