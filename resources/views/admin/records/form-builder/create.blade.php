@extends('admin.layout.default')

@section('page-level-css')
        <!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('lib/bower_components/formBuilder/dist/form-builder.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('title', 'Dynamic Form Builder')

@section('page-title')
<!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>{!! ($client) ? $client->name . ': ' : '' !!}Dynamic Form Builder</h1>
    </div>
<!-- END PAGE TITLE -->
@endsection

@section('breadcrumb')
    <li>
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="{{ url('/records') }}">Records</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>Form Builder</span>
    </li>
@stop


@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
    <div class="row">
        <div class="col-md-offset-1 col-md-10">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    @if($client)
                        <div class="caption font-red-sunglo">
                            <span class="caption-subject font-green-haze bold uppercase"><i class="fa fa-link fa-2x"></i> Public URL:</span>
                        </div>
                        <button class="btn green-sharp btn-outline pull-right" id="demo"
                                onclick="copyToClipboard(document.getElementById('demo').innerHTML)">{{ env('APP_URL') . 'records/create/' . $hashIds->encode($client->client_id) }}</button>
                    @endif
                </div>
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <span class="caption-subject bold uppercase">
                            <i class="fa fa-building fa-2x"></i>
                            {!! ($client) ? $client->name . ': ' : '' !!} Dynamic Form Builder
                        </span>
                    </div>
                </div>
                <div class="portlet-body form">
                    @if($client)
                        <div class="form-body">
                            <div id="editor">

                            </div>
                        </div>
                    @else
                        <div class="alert alert-info"> Kindly Contact Your Service Provide, as no client has been assigned to you!!! Thank you</div>
                    @endif
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
@endsection

@section('page-level-js')
    <script src="{{ asset('assets/global/plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
    <script src="{{ asset('lib/bower_components/formBuilder/dist/form-builder.js') }}" type="text/javascript"></script>
@endsection

@section('layout-script')
{{--    <script src="{{ asset('assets/custom/js/records/form.js') }}" type="text/javascript"></script>--}}
    <script>
        jQuery(document).ready(function($) {
            'use strict';

            var schema = ('<?php echo $schema; ?>') ? JSON.parse('<?php echo $schema; ?>') : [];
            var client_id = '<?php echo $client->client_id; ?>';
//            console.log(schema);
            var options = {
                dataType: 'json',
                editOnAdd: false,
                defaultFields: schema, // [{type: 'text', name: 'first-name', label: 'First Name'}],
                disableFields: ['autocomplete', 'hidden', 'checkbox', 'header', 'file', 'button'],
                sortableControls: true,
                fieldRemoveWarn: true,
                messages: {
                    select: 'Select Menu'
//                    text: 'Input Field'
                }
            };
            var formBuilder = $('#editor').formBuilder(options).data('formBuilder');

            $(".form-builder-save").click(function(e) {
                e.preventDefault();

                var values = JSON.stringify(formBuilder.formData);
                $.ajax({
                    type: "POST",
                    data: values,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    url: '/form-builders/' + client_id,
                    success: function (data) {
                        console.log('Result: ', data);
                        window.location.reload();
                    }
                });

            });

            $.ajaxSetup({ headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' } });
            setTabActive('[href="/form-builders"]');
        });
    </script>
@endsection
