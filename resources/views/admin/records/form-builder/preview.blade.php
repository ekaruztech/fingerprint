@extends('admin.layout.default')

@section('page-level-css')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('title', $client->name . ' Form Preview')

@if(Auth::check())
    @section('page-title')
    <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>{{ $client->name }}: Form Preview</h1>
        </div>
    <!-- END PAGE TITLE -->
    @endsection

    @section('breadcrumb')
        <li>
            <a href="{{ url('/dashboard') }}">Dashboard</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ url('/records') }}">Records</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Form Preview</span>
        </li>
    @stop
@endif


@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                @if($client)
                    <div class="portlet-title">
                        <div class="caption font-red-sunglo">
                            <span class="caption-subject font-green-haze bold uppercase"><i class="fa fa-link fa-2x"></i> Public URL:</span>
                        </div>
                        <button class="btn green-sharp btn-outline pull-right" id="demo"
                            onclick="copyToClipboard(document.getElementById('demo').innerHTML)">{{ env('APP_URL') . 'records/create/' . $hashIds->encode($client->client_id) }}</button>
                    </div>
                    <div class="portlet-title">
                        <div class="caption font-red-sunglo pull-left">
                            <span class="caption-subject bold uppercase"><i class="fa fa-user fa-2x"></i> {{ $client->name }}: Form Preview</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        {!! Form::hidden('client_id', $client->client_id, ['class'=>'form-control']) !!}
                        <div class="form-body">
                            <div class="caption font-green-haze">
                                <span class="caption-subject bold uppercase">Basic Information</span>
                                <hr>
                            </div>
                            <div class="form-group">
                                <label>Surname: <span class="font-red">*</span></label><small class="pull-right font-blue-sharp">i.e Your Last Name</small>
                                <input type="text" class="form-control" required name="last_name" placeholder="Surname" value="{{ old('last_name') }}">
                            </div>
                            <div class="form-group">
                                <label>Other Names: <span class="font-red">*</span></label><small class="pull-right font-blue-sharp">i.e Your First and Middle Name</small>
                                <input type="text" class="form-control" required name="first_name" placeholder="Other Names" value="{{ old('first_name') }}">
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<label>Other Name: </label><small class="pull-right font-blue-sharp">i.e Your Middle Name</small>--}}
                                {{--<input type="text" class="form-control" name="other_name" placeholder="Other Name" value="{{ old('other_name') }}">--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <label>Foundation/Programme Number: <span class="font-red">*</span></label><small class="pull-right font-blue-sharp">i.e JUPEB/2016/001</small>
                                <input type="text" class="form-control" required name="unique_id" placeholder="Foundation/Programme Number" value="{{ old('unique_id') }}">
                            </div>
                            <div class="form-group">
                                <label>Gender: <span class="font-red">*</span></label>
                                {!! Form::select('gender', [''=>'Nothing Selected', 'Male'=>'Male', 'Female'=>'Female'], '', ['class'=>'form-control selectpicker', 'required'=>'required']) !!}
                            </div>
                            <div class="form-group">
                                <label>Date of Birth: <span class="font-red">*</span></label><small class="pull-right font-blue-sharp">i.e YYYY-MM-DD</small>
                                <input type="text" class="form-control date-picker" required data-date-format="yyyy-mm-dd" name="dob" value="{{ old('dob') }}" placeholder="Date of Birth">
                            </div>
                            <div class="form-group">
                                <label>Email Address: <span class="font-red">*</span></label> <small class="pull-right font-blue-sharp">yourname@domain.com</small>
                                <input type="email" class="form-control" required name="email" placeholder="Email Address" value="{{ old('email') }}">
                            </div>
                            <div class="form-group">
                                <label>Mobile Number: </label><small class="pull-right font-blue-sharp">i.e 08011223344</small>
                                <input type="text" class="form-control" name="phone_no" placeholder="Mobile Number" value="{{ old('phone_no') }}">
                            </div>

                            @if($categories->count() > 0)
                                <div class="form-group">
                                    <label class="control-label">Category: <span class="font-red">*</span></label>
                                    <select class="form-control category-levels" required name="category_id">
                                        <option value="">Nothing Selected</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->category_id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group"> </div>
                                <div class="form-group"> </div>
                                <div class="form-group"> </div>
                                <div class="form-group"> </div>
                                <div class="form-group"> </div>
                                {{--<div class="form-group">--}}
                                    {{--<label>Checkboxes</label>--}}
                                    {{--<div class="checkbox-list">--}}
                                        {{--<label>--}}
                                            {{--<input type="checkbox"> Checkbox 1 </label>--}}
                                        {{--<label>--}}
                                            {{--<input type="checkbox"> Checkbox 2 </label>--}}
                                        {{--<label>--}}
                                            {{--<input type="checkbox" disabled> Disabled </label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            @endif<hr>
                            @if($builder)
                                <div class="caption font-green-haze">
                                    <span class="caption-subject bold uppercase">Other Information</span>
                                    <hr>
                                </div>
                                @foreach($builder->schema as $element)
                                    <?php $required = (isset($element['required']) and $element['required']) ? true : false; ?>
                                    <?php $des = (isset($element['description'])) ? '<small class="pull-right font-blue-sharp">'.$element['description'].'</small>' : ''; ?>
                                    <div class="form-group">
                                        <label>{{ $element['label'] }}: {!! ($required) ? '<span class="font-red">*</span>' : '' !!} </label>{!! $des !!}
                                        @if($element['type'] == 'number' || $element['type'] == 'text')
                                            <input type="{{$element['type']}}" class="form-control" name="{{$element['name']}}"
                                                   value="{{ old($element['name']) }}" {!! ($required) ? 'required' : '' !!} placeholder="{{$element['label']}}">
                                        @elseif($element['type'] == 'date')
                                            <input type="text" class="form-control date-picker" data-date-format="yyyy-mm-dd" name="{{$element['name']}}"
                                                   value="{{ old($element['name']) }}" {!! ($required) ? 'required' : '' !!} placeholder="{{$element['label']}}">
                                        @elseif($element['type'] == 'radio-group')
                                            <div class="radio-list">
                                                @foreach($element['values'] as $radio)
                                                    <label class="radio-inline">
                                                        <span><input type="radio" checked="" value="{{$radio['value']}}" name="{{$element['name']}}"></span> {{$radio['label']}}
                                                    </label>
                                                @endforeach
                                            </div>
                                        @elseif($element['type'] == 'checkbox-group')
                                            <div class="checkbox-list">
                                                @foreach($element['values'] as $checkbox)
                                                    <label class="checkbox-inline">
                                                        <span><input type="checkbox" value="{{$checkbox['value']}}" name="{{$element['name']}}[]"></span>
                                                        {{$checkbox['label']}}
                                                    </label>
                                                @endforeach
                                            </div>
                                        @elseif($element['type'] == 'select')
                                            <select  class="form-control selectpicker" name="{{$element['name']}}" >
                                                <option value="">{{ $element['label'] }}</option>
                                                @foreach($element['values'] as $option)
                                                    <option value="{{$option['value']}}"> {{$option['label']}} </option>
                                                @endforeach
                                            </select>
                                        @elseif($element['type'] == 'textarea')
                                            <textarea class="form-control" rows="4" name="{{$element['name']}}" {!! ($required) ? 'required' : '' !!}
                                            placeholder="{{$element['label']}}">{{ old($element['name']) }}</textarea>
                                        @endif
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                @else
                    <div class="alert alert-info"> Kindly Contact Your Administrator, as the form has not been setup!!! Thank you</div>
                @endif
            </div>
            <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
@endsection

@section('page-level-js')
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection

@section('layout-script')
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/custom/js/records/record.js') }}" type="text/javascript"></script>
    <script>
        function copyToClipboard(text) {
            window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
        }
    </script>
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/form-builders/preview"]');
        });
    </script>
@endsection
