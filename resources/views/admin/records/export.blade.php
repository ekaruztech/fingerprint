@extends('admin.layout.default')

@section('page-level-css')
<!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->
@endsection

@section('title', 'Export Record')

@section('page-title')
<!-- BEGIN PAGE TITLE -->
<div class="page-title">
    <h1>Manage Record</h1>
</div>
<!-- END PAGE TITLE -->
@endsection

@section('breadcrumb')
    <li>
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <i class="fa fa-dashboard"></i>
    </li>
    <li>
        <a href="{{ url('/records') }}">Manage Records</a>
        <i class="fa fa-th"></i>
    </li>
    <li><span class="active">Export Records</span></li>
@stop

@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-list font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Registered</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                                <i class="fa fa-share"></i>
                                <span class="hidden-xs"> Tool Actions</span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right" id="records_datatable_tools">
                                <li>
                                    <a href="javascript:;" data-action="0" class="tool-action">
                                        <i class="icon-printer"></i> Print</a>
                                </li>
                                <li>
                                    <a href="javascript:;" data-action="1" class="tool-action">
                                        <i class="icon-check"></i> Copy</a>
                                </li>
                                {{--<li>--}}
                                    {{--<a href="javascript:;" data-action="2" class="tool-action">--}}
                                        {{--<i class="icon-doc"></i> PDF</a>--}}
                                {{--</li>--}}
                                <li class="divider"> </li>
                                <li>
                                    <a href="javascript:;" data-action="3" class="tool-action">
                                        <i class="icon-paper-clip"></i> Excel</a>
                                </li>
                                <li>
                                    <a href="javascript:;" data-action="4" class="tool-action">
                                        <i class="icon-cloud-upload"></i> CSV</a>
                                </li>
                                {{--<li>--}}
                                    {{--<a href="javascript:;" data-action="5" class="tool-action">--}}
                                        {{--<i class="icon-user"></i> Columns</a>--}}
                                {{--</li>--}}
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <div class="table-actions-wrapper">
                            <span> </span>
                            Search: <input type="text" class="form-control input-inline input-small input-sm" id="search_param"/>
                        </div>
                        <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" cellpadding="0" id="records_datatable">
                            <thead>
                                <tr role="row" class="heading">
                                    <th>#</th>
                                    <th>Surname</th>
                                    <th>Other Names</th>
                                    <th>Foundation No.</th>
                                    <th>Subjects</th>
                                    <th>Date of Birth</th>
                                    <th>Gender</th>
                                    <th>Exam No.</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr role="row" class="heading">
                                    <th>#</th>
                                    <th>Surname</th>
                                    <th>Other Names</th>
                                    <th>Foundation No.</th>
                                    <th>Subjects</th>
                                    <th>Date of Birth</th>
                                    <th>Gender</th>
                                    <th>Exam No.</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php $j=1; ?>
                                @foreach ($records as $record)
                                    <tr>
                                        <td>{{ $j++ }}</td>
                                        <td>{{ $record->last_name }}</td>
                                        <td>{{ $record->first_name }}</td>
                                        <td>{!! isset($record->unique_id) ? $record->unique_id : '<span class="label label-danger">nil</span>' !!}</td>
                                        <td>{{ implode($record->subjects, ', ') }}</td>
                                        <td>{!! isset($record->dob) ? $record->dob->format('d/M/Y') : '<span class="label label-danger">nil</span>' !!}</td>
                                        <td>{!! isset($record->gender) ? $record->gender : '<span class="label label-danger">nil</span>' !!}</td>
                                        <td></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
@endsection
@section('page-level-js')
<!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
@endsection

@section('layout-script')
    <script src="{{ asset('assets/custom/js/records/record.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/records/export"]');
            setTableDataTools($('#records_datatable'), 'records_datatable_tools').init();
            {{--$.ajaxSetup({ headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' } });--}}
//            TableDatatablesAjax.init();
        });
    </script>
@endsection
