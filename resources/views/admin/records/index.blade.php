@extends('admin.layout.default')

@section('page-level-css')
<!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->
@endsection

@section('title', 'Manage Record')

@section('page-title')
<!-- BEGIN PAGE TITLE -->
<div class="page-title">
    <h1>Manage Record</h1>
</div>
<!-- END PAGE TITLE -->
@endsection

@section('breadcrumb')
    <li>
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <i class="fa fa-dashboard"></i>
    </li>
    <li><span class="active">Manage Records</span></li>
@stop

@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-list font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Registered</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <div class="table-actions-wrapper">
                            <span> </span>
                            Search: <input type="text" class="form-control input-inline input-small input-sm" id="search_param"/>
                        </div>
                        <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" cellpadding="0" id="records_datatable">
                            <thead>
                                <tr role="row" class="heading">
                                    <th>#</th>
                                    <th>Names</th>
                                    <th>Category</th>
                                    <th>Date of Birth</th>
                                    <th>I.D</th>
                                    <th>Gender</th>
                                    <th>Email</th>
                                    <th>Created</th>
                                    <th>Client</th>
                                    <th>Phone No.</th>
                                    @if($builder)
                                        @foreach($builder->schema as $element)
                                            <th>{{ (isset($element['label'])) ? $element['label'] : $element['name'] }}</th>
                                        @endforeach
                                    @else
                                        <th>Data</th>
                                    @endif
                                    <th>View</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr role="row" class="heading">
                                    <th>#</th>
                                    <th>Names</th>
                                    <th>Category</th>
                                    <th>Date of Birth</th>
                                    <th>I.D</th>
                                    <th>Gender</th>
                                    <th>Email</th>
                                    <th>Created</th>
                                    <th>Client</th>
                                    <th>Phone No.</th>
                                    @if($builder)
                                        @foreach($builder->schema as $element)
                                            <th>{{ (isset($element['label'])) ? $element['label'] : $element['name'] }}</th>
                                        @endforeach
                                    @else
                                        <th>Data</th>
                                    @endif
                                    <th>View</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php $j=1;
                                    foreach ($records as $record):
                                        $data = '';
                                        if(isset($record->data)){
                                            foreach ($record->data as $key => $value){
                                                if($value){
                                                    $data .= '<b>'.$key . ':</b> ';
                                                    $data .= (is_array($value)) ? '[' . join($value, ', ') . '], ': $value . ', ';
                                                }
                                            }
                                        }
                                ?>
                                    <tr>
                                        <td>{{ $j++ }}</td>
                                        <td>{{ $record->fullNames() }}</td>
                                        <td>{!! isset($record->category_id) ? $record->category()->name : '<span class="label label-danger">nil</span>' !!}</td>
                                        <td>{!! isset($record->dob) ? $record->dob->format('d/M/Y') : '<span class="label label-danger">nil</span>' !!}</td>
                                        <td>{!! isset($record->unique_id) ? $record->unique_id : '<span class="label label-danger">nil</span>' !!}</td>
                                        <td>{!! isset($record->gender) ? $record->gender : '<span class="label label-danger">nil</span>' !!}</td>
                                        <td>{!! isset($record->email) ? $record->email : '<span class="label label-danger">nil</span>' !!}</td>
                                        <td>{!! isset($record->created_at) ? $record->created_at->format('jS, M Y') : '<span class="label label-danger">nil</span>' !!}</td>
                                        <td>{!! isset($record->client_id) ? $record->client()->name : '<span class="label label-danger">nil</span>' !!}</td>
                                        <td>{!! isset($record->phone_no) ? $record->phone_no : '<span class="label label-danger">nil</span>' !!}</td>
                                        @if($builder)
                                            @foreach($builder->schema as $element)
                                                <td>
                                                    {!! (isset($record->data[$element['name']]))
                                                    ? ((is_array($record->data[$element['name']])) ? join($record->data[$element['name']], ', ') : $record->data[$element['name']])
                                                    : '<span class="label label-danger">nil</span>' !!}
                                                </td>
                                            @endforeach
                                        @else
                                            <td>{!! isset($record->data) ? $data : '<span class="label label-danger">nil</span>' !!}</td>
                                        @endif
                                        <td>
                                            <a target="_blank" href="/records/{{$record->_id}}" class="btn btn-info btn-rounded btn-condensed btn-xs">
                                                <span class="fa fa-eye-slash"></span>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="/records/{{$record->_id}}/edit" class="btn btn-warning btn-rounded btn-condensed btn-xs">
                                                <span class="fa fa-edit"></span>
                                            </a>
                                        </td>
                                        <td>
                                            <button class="btn btn-danger btn-rounded btn-xs delete_record" value="{{$record->_id}}">
                                                <span class="fa fa-trash-o"></span>
                                            </button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
@endsection
@section('page-level-js')
<!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-bootbox.min.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
@endsection

@section('layout-script')
    <script src="{{ asset('assets/custom/js/records/record.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/records"]');
            setTableData($('#records_datatable')).init();
            {{--$.ajaxSetup({ headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' } });--}}
//            TableDatatablesAjax.init();
        });
    </script>
@endsection
