@extends('admin.layout.default')

@section('page-level-css')
        <!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('title', 'Modify Record')

@section('page-title')
<!-- BEGIN PAGE TITLE -->
<div class="page-title">
    <h1>Modify Record</h1>
</div>
<!-- END PAGE TITLE -->
@endsection

@section('breadcrumb')
    <li>
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="{{ url('/records') }}">Records</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>Modify Record</span>
    </li>
@stop

@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
    <!-- END PAGE HEADER-->
    <div class="profile">
        <div class="tabbable-line tabbable-full-width">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab_1_1" data-toggle="tab"> Modify Record Info </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1_1">
                    <div class="row profile-account">
                        <div class="col-md-3">
                            <ul class="ver-inline-menu tabbable margin-bottom-10">
                                <li class="{{ (session('active') == 'info') ? 'active' : '' }} {{ (!session()->has('active')) ? 'active' : '' }}">
                                    <a data-toggle="tab" href="#tab_1-1">
                                        <i class="fa fa-cog"></i> Record info </a>
                                    <span class="after"> </span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            @include('errors.errors')
                            <div class="tab-content">
                                <div id="tab_1-1" class="tab-pane {{ (session('active') == 'info') ? 'active' : '' }} {{ (!session()->has('active')) ? 'active' : '' }} )">
                                    <form method="POST" action="/records/edit" accept-charset="UTF-8" class="form-horizontal" role="form">
                                        {!! csrf_field() !!}
                                        {!! Form::hidden('record_id', $record->_id) !!}
                                        <div class="caption font-green-haze">
                                            <span class="caption-subject bold uppercase">Basic Information</span>
                                            <hr>
                                        </div>
                                        <div class="form-group">
                                            <label>Surname: <span class="font-red">*</span></label><small class="pull-right font-blue-sharp">i.e Your Last Name</small>
                                            <input type="text" class="form-control" required name="last_name" placeholder="Surname" value="{{ $record->last_name  }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Other Names: <span class="font-red">*</span></label><small class="pull-right font-blue-sharp">i.e Your First and Middle Name</small>
                                            <input type="text" class="form-control" required name="first_name" placeholder="Other Names" value="{{ $record->first_name  }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Foundation/Programme Number: <span class="font-red">*</span></label><small class="pull-right font-blue-sharp">i.e JUPEB/2016/001</small>
                                            <input type="text" class="form-control" required name="unique_id" placeholder="Personal I.D" value="{{ $record->unique_id }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Gender: <span class="font-red">*</span></label>
                                            {!! Form::select('gender', [''=>'Nothing Selected', 'Male'=>'Male', 'Female'=>'Female'], $record->gender, ['class'=>'form-control selectpicker', 'required'=>'required']) !!}
                                        </div>
                                        <div class="form-group">
                                            <label>Date of Birth: <span class="font-red">*</span></label><small class="pull-right font-blue-sharp">i.e YYYY-MM-DD</small>
                                            <input type="text" class="form-control date-picker" required data-date-format="yyyy-mm-dd" name="dob" value="{{ $record->dob->format('Y-m-d') }}" placeholder="Date of Birth">
                                        </div>
                                        <div class="form-group">
                                            <label>Email Address: <span class="font-red">*</span></label>
                                            <input type="email" class="form-control" required name="email" placeholder="Email Address" value="{{ $record->email }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Mobile Number: </label>
                                            <input type="text" class="form-control" name="phone_no" placeholder="Mobile Number" value="{{ $record->phone_no }}">
                                        </div>
                                        {{--@if($categories->count() > 0)--}}
                                            {{--<div class="form-group">--}}
                                                {{--<label class="control-label">Category: <span class="font-red">*</span></label>--}}
                                                {{--<select class="form-control category-levels" required name="category_id">--}}
                                                    {{--<option value="">Nothing Selected</option>--}}
                                                    {{--@foreach($categories as $category)--}}
                                                        {{--<option value="{{$category->category_id}}">{{$category->name}}</option>--}}
                                                    {{--@endforeach--}}
                                                {{--</select>--}}
                                            {{--</div>--}}
                                            {{--<div class="form-group"> </div>--}}
                                            {{--<div class="form-group"> </div>--}}
                                            {{--<div class="form-group"> </div>--}}
                                            {{--<div class="form-group"> </div>--}}
                                            {{--<div class="form-group"> </div>--}}

                                        {{--@endif<hr>--}}
                                        @if($builder)
                                            <div class="caption font-green-haze">
                                                <span class="caption-subject bold uppercase">Other Information</span>
                                                <hr>
                                            </div>

                                            @foreach($builder->schema as $element)
                                                <?php $required = (isset($element['required']) and $element['required']) ? true : false; ?>
                                                <div class="form-group">
                                                    <label>{{ $element['label'] }}: {!! ($required) ? '<span class="font-red">*</span>' : '' !!}</label>
                                                    @if($element['type'] == 'number' || $element['type'] == 'text')
                                                        <input type="{{$element['type']}}" class="form-control" name="{{$element['name']}}"
                                                               value="{{ old($element['name']) }}" {!! ($required) ? 'required' : '' !!} placeholder="{{$element['label']}}">
                                                    @elseif($element['type'] == 'date')
                                                        <input type="text" class="form-control date-picker" data-date-format="yyyy-mm-dd" name="{{$element['name']}}"
                                                               value="{{ old($element['name']) }}" {!! ($required) ? 'required' : '' !!} placeholder="{{$element['label']}}">
                                                    @elseif($element['type'] == 'radio-group')
                                                        <div class="radio-list">
                                                            @foreach($element['values'] as $radio)
                                                                <label class="radio-inline">
                                                                    <span><input type="radio" checked="" value="{{$radio['value']}}" name="{{$element['name']}}"></span> {{$radio['label']}}
                                                                </label>
                                                            @endforeach
                                                        </div>
                                                    @elseif($element['type'] == 'checkbox-group')
                                                        <div class="checkbox-list">
                                                            @foreach($element['values'] as $checkbox)
                                                                <label class="checkbox-inline">
                                                                    <span><input type="checkbox" value="{{$checkbox['value']}}" name="{{$element['name']}}[]"></span>
                                                                    {{$checkbox['label']}}
                                                                </label>
                                                            @endforeach
                                                        </div>
                                                    @elseif($element['type'] == 'select')
                                                        <select  class="form-control selectpicker" name="{{$element['name']}}" >
                                                            <option value="">{{ $element['label'] }}</option>
                                                            @foreach($element['values'] as $option)
                                                                <option value="{{$option['value']}}"> {{$option['label']}} </option>
                                                            @endforeach
                                                        </select>
                                                    @elseif($element['type'] == 'textarea')
                                                        <textarea class="form-control" rows="4" name="{{$element['name']}}" {!! ($required) ? 'required' : '' !!}
                                                        placeholder="{{$element['label']}}">{{ old($element['name']) }}</textarea>
                                                    @endif
                                                </div>
                                            @endforeach

                                            <div class="form-group">
                                            <label class="control-label">Record's Category</label>
                                            <select multiple class="form-control selectpicker" name="category_id[]">
                                                <option value="">Select Category</option>
                                                @foreach($categories as $category)
                                                    <optgroup label="{{ strtoupper($category->name) }}">
                                                        @if(Auth::user()->hasRole('developer'))
                                                            @if(in_array($category->category_id, $record->categories()->get()->pluck('category_id')->toArray()))
                                                                <option selected value="{{$category->category_id}}"> {{ strtoupper($category->name) }}</option>
                                                            @else
                                                                <option value="{{$category->category_id}}"> {{ strtoupper($category->name) }}</option>
                                                            @endif
                                                        @endif

                                                        @foreach($category->getImmediateDescendants() as $cat)
                                                            @if(in_array($cat->category_id, $record->categories()->get()->pluck('category_id')->toArray()))
                                                                <option selected value="{{$cat->category_id}}"> {{ strtoupper($cat->name) }}</option>
                                                            @else
                                                                <option value="{{$cat->category_id}}"> {{ strtoupper($cat->name) }}</option>
                                                            @endif

                                                            @foreach($cat->getImmediateDescendants() as $sub)
                                                                @if(in_array($sub->category_id, $record->categories()->get()->pluck('category_id')->toArray()))
                                                                    <option selected value="{{$sub->category_id}}"> > {{$sub->name}}</option>
                                                                @else
                                                                    <option value="{{$sub->category_id}}"> > {{$sub->name}}</option>
                                                                @endif

                                                                @foreach($sub->getImmediateDescendants() as $sub1)
                                                                    @if(in_array($sub1->category_id, $record->categories()->get()->pluck('category_id')->toArray()))
                                                                        <option selected value="{{$sub1->category_id}}"> > > {{$sub1->name}}</option>
                                                                    @else
                                                                        <option value="{{$sub1->category_id}}"> > > {{$sub1->name}}</option>
                                                                    @endif

                                                                    @foreach($sub1->getImmediateDescendants() as $sub2)
                                                                        @if(in_array($sub2->category_id, $record->categories()->get()->pluck('category_id')->toArray()))
                                                                            <option selected value="{{$sub2->category_id}}"> > > > {{$sub2->name}}</option>
                                                                        @else
                                                                            <option value="{{$sub2->category_id}}"> > > > {{$sub2->name}}</option>
                                                                        @endif
                                                                    @endforeach
                                                                @endforeach
                                                            @endforeach
                                                        @endforeach
                                                    </optgroup>
                                                @endforeach
                                            </select>
                                        </div>
                                        @endif
                                        <div class="margiv-top-10">
                                            <button class="btn green"> Update Record</button>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                        <!--end col-md-9-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
@endsection

@section('page-level-js')
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection

@section('layout-script')
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/custom/js/records/record.js') }}" type="text/javascript"></script>    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/records"]');
        });
    </script>
    <!-- END PAGE LEVEL SCRIPTS -->
@endsection



