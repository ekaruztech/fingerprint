@extends('admin.layout.default')

@section('page-level-css')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('title', $client->name . ' Registration')

@if(Auth::check())
    @section('page-title')
    <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>{{ $client->name }}: Registration</h1>
        </div>
    <!-- END PAGE TITLE -->
    @endsection

    @section('breadcrumb')
        <li>
            <a href="{{ url('/dashboard') }}">Dashboard</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ url('/records') }}">Records</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Create New Record</span>
        </li>
    @stop
@endif


@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-user font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">{{ $client->full_name }}: Registration Form</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    @include('errors.errors')
                    @if(session()->has('registration_message'))
                        <div class="alert alert-info" role="alert">
                            <button type="button" class="close" data-dismiss="alert">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <i class="fa fa-thumbs-o-up fa-2x"></i> {!! session()->get('registration_message') !!}
                        </div>
                    @endif
                    @if(isset($client))
                        {{--<form method="POST" action="/records" accept-charset="UTF-8" class="form-horizontal" role="form" enctype="multipart/form-data">--}}
                        <form method="POST" action="/records" accept-charset="UTF-8" class="form-horizontal" role="form">
                            {!! csrf_field() !!}
                            {!! Form::hidden('client_id', $client->client_id, ['class'=>'form-control']) !!}
                            <div class="form-body">
                                <div class="caption font-green-haze">
                                    <span class="caption-subject bold uppercase">Basic Information</span>
                                    <hr>
                                </div>
                                <div class="form-group">
                                    <label>Surname: <span class="font-red">*</span></label><small class="pull-right font-blue-sharp">i.e Your Last Name</small>
                                    <input type="text" class="form-control" required name="last_name" placeholder="Surname" value="{{ old('last_name') }}">
                                </div>
                                <div class="form-group">
                                    <label>Other Names: <span class="font-red">*</span></label><small class="pull-right font-blue-sharp">i.e Your First and Middle Name</small>
                                    <input type="text" class="form-control" required name="first_name" placeholder="Other Names" value="{{ old('first_name') }}">
                                </div>
                                {{--<div class="form-group">--}}
                                {{--<label>Other Name: </label><small class="pull-right font-blue-sharp">i.e Your Middle Name</small>--}}
                                {{--<input type="text" class="form-control" name="other_name" placeholder="Other Name" value="{{ old('other_name') }}">--}}
                                {{--</div>--}}
                                <div class="form-group">
                                    <label>Foundation/Programme Number: <span class="font-red">*</span></label><small class="pull-right font-blue-sharp">i.e JUPEB/2016/001</small>
                                    <input type="text" class="form-control" required name="unique_id" placeholder="Foundation/Programme Number" value="{{ old('unique_id') }}">
                                </div>
                                <div class="form-group">
                                    <label>Gender: <span class="font-red">*</span></label>
                                    {!! Form::select('gender', [''=>'Nothing Selected', 'Male'=>'Male', 'Female'=>'Female'], '', ['class'=>'form-control selectpicker', 'required'=>'required']) !!}
                                </div>
                                <div class="form-group">
                                    <label>Date of Birth: <span class="font-red">*</span></label><small class="pull-right font-blue-sharp">i.e YYYY-MM-DD</small>
                                    <input type="text" class="form-control date-picker" required data-date-format="yyyy-mm-dd" name="dob" value="{{ old('dob') }}" placeholder="Date of Birth">
                                </div>
                                <div class="form-group">
                                    <label>Email Address: <span class="font-red">*</span></label> <small class="pull-right font-blue-sharp">yourname@domain.com</small>
                                    <input type="email" class="form-control" required name="email" placeholder="Email Address" value="{{ old('email') }}">
                                </div>
                                <div class="form-group">
                                    <label>Mobile Number: </label><small class="pull-right font-blue-sharp">i.e 08011223344</small>
                                    <input type="text" class="form-control" name="phone_no" placeholder="Mobile Number" value="{{ old('phone_no') }}">
                                </div>
                                @if($categories->count() > 0)
                                    <div class="form-group">
                                        <label class="control-label">Category: <span class="font-red">*</span></label>
                                        <select class="form-control category-levels" required name="category_id">
                                            <option value="">Nothing Selected</option>
                                            @foreach($categories as $category)
                                                <option value="{{$category->category_id}}">{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group"> </div>
                                    <div class="form-group"> </div>
                                    <div class="form-group"> </div>
                                    <div class="form-group"> </div>
                                    <div class="form-group"> </div>

                                @endif<hr>
                                @if($builder)
                                    <div class="caption font-green-haze">
                                        <span class="caption-subject bold uppercase">Other Information</span>
                                        <hr>
                                    </div>
                                    @foreach($builder->schema as $element)
                                        <?php $required = (isset($element['required']) and $element['required']) ? true : false; ?>
                                        <?php $des = (isset($element['description'])) ? '<small class="pull-right font-blue-sharp">'.$element['description'].'</small>' : ''; ?>
                                        <div class="form-group">
                                            <label>{{ $element['label'] }}: {!! ($required) ? '<span class="font-red">*</span>' : '' !!}</label> {!! $des !!}
                                            @if($element['type'] == 'number' || $element['type'] == 'text')
                                                <input type="{{$element['type']}}" class="form-control" name="data[{{$element['name']}}]"
                                                       value="{{ old($element['name']) }}" {!! ($required) ? 'required' : '' !!} placeholder="{{$element['label']}}">
                                            @elseif($element['type'] == 'date')
                                                <input type="text" class="form-control date-picker" data-date-format="yyyy-mm-dd" name="data[{{$element['name']}}]"
                                                       value="{{ old($element['name']) }}" {!! ($required) ? 'required' : '' !!} placeholder="{{$element['label']}}">
                                            @elseif($element['type'] == 'radio-group')
                                                <div class="radio-list">
                                                    @foreach($element['values'] as $radio)
                                                        <label class="radio-inline">
                                                            <span><input type="radio" checked="" value="{{$radio['value']}}" name="data[{{$element['name']}}]"></span> {{$radio['label']}}
                                                        </label>
                                                    @endforeach
                                                </div>
                                            @elseif($element['type'] == 'checkbox-group')
                                                <div class="checkbox-list">
                                                    @foreach($element['values'] as $checkbox)
                                                        <label class="checkbox-inline">
                                                            <span><input type="checkbox" value="{{$checkbox['value']}}" name="data[{{$element['name']}}][]"></span>
                                                            {{$checkbox['label']}}
                                                        </label>
                                                    @endforeach
                                                </div>
                                            @elseif($element['type'] == 'select')
                                                <select  class="form-control selectpicker" name="data[{{$element['name']}}]" >
                                                    <option value="">{{ $element['label'] }}</option>
                                                    @foreach($element['values'] as $option)
                                                        <option value="{{$option['value']}}"> {{$option['label']}} </option>
                                                    @endforeach
                                                </select>
                                            @elseif($element['type'] == 'textarea')
                                                <textarea class="form-control" rows="4" name="data[{{$element['name']}}]" {!! ($required) ? 'required' : '' !!}
                                                placeholder="{{$element['label']}}">{{ old($element['name']) }}</textarea>
                                            @endif
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn blue pull-right">Register</button>
                            </div>
                        </form>
                    @else
                        <div class="alert alert-info"> Kindly Contact Your Administrator, as the form has not been setup!!! Thank you</div>
                    @endif
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
@endsection

@section('page-level-js')
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection

@section('layout-script')
    <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/custom/js/records/record.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/records/form-builder"]');

            // Ajax Get Categories Based on the Parent ID
//            getDependentListBox($('#level1_id'), $('#level2_id'), '/list-box/categories/');
//            getDependentListBox($('#level2_id'), $('#level3_id'), '/list-box/categories/');
        });
    </script>
@endsection
