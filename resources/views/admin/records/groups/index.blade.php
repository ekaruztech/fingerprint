@extends('admin.layout.default')

@section('page-level-css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->
@endsection

@section('title', 'Manage Record Groupings')

@section('page-title')
        <!-- BEGIN PAGE TITLE -->
<div class="page-title">
    <h1>Manage Record</h1>
</div>
<!-- END PAGE TITLE -->
@endsection

@section('breadcrumb')
    <li>
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <i class="fa fa-dashboard"></i>
    </li>
    <li><span class="active">Manage Record Groupings</span></li>
@stop

@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-list font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Record Groupings</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <div class="table-actions-wrapper">
                            <span> </span>
                            Search: <input type="text" class="form-control input-inline input-small input-sm" id="search_param"/>
                        </div>
                        <table class="table table-striped table-bordered table-hover" id="records_datatable">
                            <thead>
                                <tr role="row" class="heading">
                                    <th width="2%">#</th>
                                    <th width="45%">Title</th>
                                    <th width="25%">Created By</th>
                                    <th width="10%">Record(s)</th>
                                    <th width="6%">View</th>
                                    <th width="6%">Edit</th>
                                    <th width="6%">Delete</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr role="row" class="heading">
                                    <th width="2%">#</th>
                                    <th width="45%">Title</th>
                                    <th width="25%">Created By</th>
                                    <th width="10%">Record(s)</th>
                                    <th width="6%">View</th>
                                    <th width="6%">Edit</th>
                                    <th width="6%">Delete</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php $i=1; ?>
                                @if($groups)
                                    @foreach ($groups as $group)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $group->title }}</td>
                                            <td>{!! ($group->user_id) ? $group->user()->fullNames() : '<span class="label label-danger">nil</span>' !!}</td>
                                            <td>{!! ($group->records) ? count($group->records) : '<span class="label label-danger">nil</span>' !!}</td>
                                            <td>
                                                <a href="/record-groups/view/{{$group->_id}}" class="btn btn-info btn-rounded btn-condensed btn-xs">
                                                    <span class="fa fa-eye-slash"></span>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="/record-groups/edit/{{$group->_id}}" class="btn btn-warning btn-rounded btn-condensed btn-xs">
                                                    <span class="fa fa-edit"></span>
                                                </a>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger btn-rounded btn-xs delete_record" value="{{$group->_id}}">
                                                    <span class="fa fa-trash-o"></span>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
@endsection
@section('page-level-js')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-bootbox.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
@endsection

@section('layout-script')
    <script src="{{ asset('assets/custom/js/records/group.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/record-groups"]');
            setTableData($('#records_datatable')).init();
            {{--$.ajaxSetup({ headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' } });--}}
//            TableDatatablesAjax.init();
        });
    </script>
@endsection
