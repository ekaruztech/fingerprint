@extends('admin.layout.default')

@section('page-level-css')
<!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->
@endsection

@section('title', 'Record Group Details')

@section('page-title')
<!-- BEGIN PAGE TITLE -->
<div class="page-title">
    <h1>Manage Record</h1>
</div>
<!-- END PAGE TITLE -->
@endsection

@section('breadcrumb')
    <li>
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <i class="fa fa-dashboard"></i>
    </li>
    <li>
        <a href="{{ url('/records/list-groups') }}">Records Groups</a>
        <i class="fa fa-circle"></i>
    </li>
    <li><span class="active">Record Group Details</span></li>
@stop

@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-list font-green"></i>
                        <span class="caption-subject font-green bold uppercase">{{ $group->title }}: Logs</span>
                        <input type="hidden" name="group_id" id="group_id" value="{{$group->_id}}">
                    </div>
                </div>
                <div class="portlet-title margin-bottom-30">
                    <div class="caption">
                        <div class="alert alert-info text-center font-md">
                            <i class="fa fa-info"></i> <small>Kindly select a date from the field below to filter logs based on the date it was taken.</small>
                        </div>
                        <div class="input-group date date-picker margin-bottom-5 col-md-6 col-md-offset-3" data-date-format="yyyy-mm-dd">
                            <input type="text" class="form-control form-filter input-sm" readonly name="filterBy_date" id="filterBy_date" placeholder="Filter by Date">
                            <span class="input-group-btn">
                                <button class="btn btn-sm default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div id="log_error_msg"></div>
                        <div id="log_results" class="hide">
                            <div class="col-md-7">
                                <div class="table-container">
                                    <div class="caption">
                                        <i class="fa fa-check-square-o font-green"></i>
                                        <span class="caption-subject font-green bold uppercase">Presents</span>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" cellpadding="0">
                                        <thead>
                                        <tr role="row" class="heading">
                                            <th>#</th>
                                            <th>ID.</th>
                                            <th>Name</th>
                                            <th>Gender</th>
                                            <th>Type</th>
                                        </tr>
                                        </thead>
                                        <tbody id="log_present"> </tbody>
                                    </table>
                                    <div class="caption">
                                        <i class="fa fa-times-circle-o font-red"></i>
                                        <span class="caption-subject font-red bold uppercase">Absent</span>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" cellpadding="0">
                                        <thead>
                                        <tr role="row" class="heading">
                                            <th>#</th>
                                            <th>Type</th>
                                            <th>Name</th>
                                            <th>Gender</th>
                                        </tr>
                                        </thead>
                                        <tbody id="log_absent"> </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row widget-row">
                                <div class="col-md-5">
                                    <div class="portlet light portlet-fit bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class=" icon-layers font-green"></i>
                                                <span class="caption-subject font-green bold uppercase">Log Representation</span>
                                            </div>
                                            <div class="tools">
                                                <a href="javascript:;" class="collapse"> </a>
                                                <a href="javascript:;" class="fullscreen"> </a>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div id="record_log_chart" class="chart"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><hr><hr>
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-list font-green"></i>
                        <span class="caption-subject font-green bold uppercase">{{ $group->title }}: Records</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <div class="table-actions-wrapper">
                            <span> </span>
                            Search: <input type="text" class="form-control input-inline input-small input-sm" id="search_param"/>
                        </div>
                        <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" cellpadding="0" id="records_datatable">
                            <thead>
                                <tr role="row" class="heading">
                                    <th>#</th>
                                    <th>Full Name</th>
                                    <th>Category</th>
                                    <th>I.D</th>
                                    <th>Gender</th>
                                    <th>Email</th>
                                    <th>Phone No.</th>
                                    @if(!Auth::user()->hasRole(Role::USER)) <th>View</th> @endif
                                </tr>
                            </thead>
                            <tfoot>
                                <tr role="row" class="heading">
                                    <th>#</th>
                                    <th>Full Name</th>
                                    <th>Category</th>
                                    <th>I.D</th>
                                    <th>Gender</th>
                                    <th>Email</th>
                                    <th>Phone No.</th>
                                    @if(!Auth::user()->hasRole(Role::USER)) <th>View</th> @endif
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php $j=1;
                                    foreach ($group->records as $id):
                                        $record = Record::find($id);
                                        if(isset($record)): ?>
                                            <tr>
                                                <td>{{ $j++ }}</td>
                                                <td>{{ $record->simpleName() }}</td>
                                                <td>{!! ($record->category_id) ? $record->category()->name : '<span class="label label-danger">nil</span>' !!}</td>
                                                <td>{!! ($record->unique_id) ? $record->unique_id : '<span class="label label-danger">nil</span>' !!}</td>
                                                <td>{!! ($record->gender) ? $record->gender : '<span class="label label-danger">nil</span>' !!}</td>
                                                <td>{!! ($record->email) ? $record->email : '<span class="label label-danger">nil</span>' !!}</td>
                                                <td>{!! ($record->phone_no) ? $record->phone_no : '<span class="label label-danger">nil</span>' !!}</td>
                                                @if(!Auth::user()->hasRole(Role::USER))
                                                    <td>
                                                        <a target="_blank" href="/records/{{$record->_id}}" class="btn btn-info btn-rounded btn-condensed btn-xs">
                                                            <span class="fa fa-eye-slash"></span>
                                                        </a>
                                                    </td>
                                                @endif
                                            </tr>
                                <?php   endif; ?>
                            <?php   endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

@endsection
@section('page-level-js')
<!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/global/plugins/flot/jquery.flot.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/flot/jquery.flot.pie.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/global/plugins/amcharts/amcharts/amcharts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/amcharts/amcharts/serial.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/amcharts/amcharts/themes/light.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-bootbox.min.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
@endsection

@section('layout-script')
    <script src="{{ asset('assets/custom/js/records/group.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/record-groups"]');
            setTableData($('#records_datatable')).init();
            setTableData($('#logs_datatable')).init();

            ComponentsDatePicker.init();
            $.ajaxSetup({ headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' } });
        });
    </script>
@endsection
