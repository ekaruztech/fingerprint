@extends('admin.layout.default')

@section('page-level-css')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ asset('assets/global/plugins/jquery-multi-select/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('layout-style')
    <style type="text/css">
        .multi-select-records{
            width: 560px;
            /*min-height: 500px;*/
        }
    </style>
@endsection

@section('title',  ($client) ? $client->name : '' . ' Grouping')

@section('page-title')
<!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>{{ ($client) ? $client->name : '' }}: Grouping</h1>
    </div>
<!-- END PAGE TITLE -->
@endsection

@section('breadcrumb')
    <li>
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="{{ url('/records/list-groups') }}">Records Grouping</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>Grouping</span>
    </li>
@stop

@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
    <div class="row">
        <div class="col-md-offset-1 col-md-10">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                @if($client)
                    <div class="portlet-title">
                        <div class="caption font-red-sunglo">
                            <i class="icon-user font-red-sunglo"></i>
                            <span class="caption-subject bold uppercase">{{ $client->name }}: Grouping</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        @include('errors.errors')
                        <form method="POST" action="/record-groups/create" accept-charset="UTF-8" class="form-horizontal" role="form">
                            {!! csrf_field() !!}
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Grouping Title: <span class="font-red">*</span></label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" required name="title" placeholder="Grouping Title" value="{{ old('title') }}">
                                    </div>
                                </div><hr>

                                <div class="form-group">
                                    <div class="col-md-10 col-md-offset-1">
                                        <select multiple="multiple" class="multi-select" id="record_multi_select" name="records[]">
                                            <?php $check = [] ?>
                                            @foreach($categories as $category)
                                                @foreach($category->getDescendantsAndSelf() as $sub)
                                                    <optgroup label="{{ $sub->name }}">
                                                        @foreach($sub->records() as $record)
                                                            @if($record->client_id == Auth::user()->client_id)
                                                                @if(!in_array($record->hashed_id, $check))
                                                                    <?php $check[] = $record->hashed_id?>
                                                                    <option value="{{ $record->_id }}">
                                                                        {{ ($record->unique_id) ? $record->unique_id . ': ' . $record->simpleName() : $record->simpleName() }}
                                                                    </option>
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    </optgroup>
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                </div><hr>
                                <div class="form-group">
                                    <label class="control-label col-md-3">More Records?</label>
                                    <div class="col-md-8">
                                        <input type="checkbox" id="more_records" class="make-switch" data-on-text="&nbsp;Yes&nbsp;&nbsp;" data-off-text="&nbsp;No!&nbsp;">
                                    </div>
                                </div><hr>
                                <div class="hide" id="show_records">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Browse Records</label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="category_id">
                                                <option value="">Select Category</option>
                                                @foreach($cate as $cat)
                                                    <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div><hr>
                                    <div class="form-group hide" id="records_table">
                                        <div class="table-container col-md-12">
                                            <table class="table table-bordered">
                                                <tr>
                                                    <td>
                                                        <table class="table table-striped table-bordered table-hover">
                                                            <caption class="text-center">List of Available Record(s)</caption>
                                                            <thead>
                                                            <tr role="row" class="heading">
                                                                <th>I.D</th>
                                                                <th>Name</th>
                                                                <th>Add</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="available-records">

                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td>
                                                        <table class="table table-striped table-bordered table-hover">
                                                            <caption class="text-center">List of Selected Record(s)</caption>
                                                            <thead>
                                                            <tr role="row" class="heading">
                                                                <th>I.D</th>
                                                                <th>Name</th>
                                                                <th>Remove</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="selected-records">

                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                @if(empty($check))
                                    <div class="alert alert-warning"> Kindly Contact Your Administrator, as no client record has been assigned to you!!! Thank you</div>
                                @else
                                    <button type="submit" class="btn blue pull-right">Save Grouping</button>
                                @endif
                            </div>
                        </form>
                    </div>
                @else
                    <div class="alert alert-warning"> Kindly Contact Your Administrator, as no client record has been assigned to you!!! Thank you</div>
                @endif
            </div>
            <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
@endsection

@section('page-level-js')
    <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection

@section('layout-script')
    <script src="{{ asset('assets/custom/js/records/group.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/record-groups"]');

            ComponentsDropdowns.init();
        });
    </script>
@endsection
