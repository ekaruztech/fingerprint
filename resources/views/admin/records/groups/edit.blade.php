@extends('admin.layout.default')

@section('page-level-css')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ asset('assets/global/plugins/jquery-multi-select/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('layout-style')
    <style type="text/css">
        .multi-select-records{
            width: 560px;
            /*min-height: 500px;*/
        }
    </style>
@endsection

@section('title', $client->name . ' Edit Grouping')

@section('page-title')
<!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>{{ $client->name }}: Edit Grouping</h1>
    </div>
<!-- END PAGE TITLE -->
@endsection

@section('breadcrumb')
    <li>
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="{{ url('/records/list-groups') }}">Records Groups</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>Edit Grouping</span>
    </li>
@stop

@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
    <div class="row">
        <div class="col-md-offset-1 col-md-10">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-user font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">{{ $client->name }}: Edit Grouping</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    @include('errors.errors')
                    <form method="POST" action="/record-groups/edit" accept-charset="UTF-8" class="form-horizontal" role="form">
                        {!! csrf_field() !!}
                        <input type="hidden" value="{{ $group->_id }}" name="group_id">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">Grouping Title: <span class="font-red">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" required name="title" placeholder="Grouping Title" value="{{ $group->title }}">
                                </div>
                            </div><hr>

                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1">
                                    <select multiple="multiple" required class="multi-select" id="record_multi_select" name="records[]">
                                        <?php $check = []; $selected = [] ?>
                                        @foreach($categories as $category)
                                            @foreach($category->getDescendantsAndSelf() as $sub)
                                                <optgroup label="{{ $sub->name }}">
                                                    @foreach($sub->records() as $record)
                                                        @if($record->client_id == Auth::user()->client_id)
                                                            @if(!in_array($record->hashed_id, $check))
                                                                <?php $check[] = $record->hashed_id?>
                                                                @if(in_array($record->_id, $group->records))
                                                                    <?php $selected[] = $record->_id?>
                                                                    <option selected value="{{ $record->_id }}">
                                                                        {{ ($record->unique_id) ? $record->unique_id . ': ' . $record->simpleName() : $record->simpleName() }}
                                                                    </option>
                                                                @else
                                                                    <option value="{{ $record->_id }}">
                                                                        {{ ($record->unique_id) ? $record->unique_id . ': ' . $record->simpleName() : $record->simpleName() }}
                                                                    </option>
                                                                @endif
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        @endforeach
                                    </select>
                                </div>
                            </div><hr>
                            <?php $rec = array_diff($group->records, $selected); ?>
                            <div class="form-group">
                                <label class="control-label col-md-3">More Records?</label>
                                <div class="col-md-8">
                                    <input type="checkbox" id="more_records" {{ (empty($rec)) ? '' : 'checked' }} class="make-switch" data-on-text="&nbsp;Yes&nbsp;&nbsp;" data-off-text="&nbsp;No!&nbsp;">
                                </div>
                            </div><hr>
                            <div class="{{ (empty($rec)) ? 'hide' : '' }}" id="show_records">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Browse Records</label>
                                    <div class="col-md-8">
                                        <select class="form-control" id="category_id">
                                            <option value="">Select Category</option>
                                            @foreach($cate as $cat)
                                                <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div><hr>
                                <div class="form-group {{ (empty($rec)) ? 'hide' : '' }}" id="records_table">
                                    <div class="table-container col-md-12">
                                        <table class="table table-bordered">
                                            <tr>
                                                <td>
                                                    <table class="table table-striped table-bordered">
                                                        <caption class="text-center">List of Available Record(s)</caption>
                                                        <thead>
                                                        <tr role="row" class="heading">
                                                            <th>I.D</th>
                                                            <th>Name</th>
                                                            <th>Add</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="available-records">

                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td>
                                                    <table class="table table-striped table-bordered">
                                                        <caption class="text-center">List of Selected Record(s)</caption>
                                                        <thead>
                                                        <tr role="row" class="heading">
                                                            <th>I.D</th>
                                                            <th>Name</th>
                                                            <th>Remove</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="selected-records">
                                                        <?php
                                                            if(!empty($rec)):
                                                                foreach ($rec as $id):
                                                                    $record = Record::find($id);
                                                                    if(isset($record)): ?>
                                                                        <tr>
                                                                            <td>{!! ($record->unique_id) ? $record->unique_id : '<span class="label label-danger">nil</span>' !!}</td>
                                                                            <td>{{ $record->simpleName() }}</td>
                                                                            <td><input name="records[]" checked="checked" type="checkbox" class="each-record" value="{{ $record->_id }}"></td>
                                                                        </tr>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            @if(empty($check))
                                <div class="alert alert-warning"> Kindly Contact Your Administrator, as no client record has been assigned to you!!! Thank you</div>
                            @else
                                <button type="submit" class="btn blue pull-right">Save Grouping</button>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
@endsection

@section('page-level-js')
    <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection

@section('layout-script')
    <script src="{{ asset('assets/custom/js/records/group.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/record-groups/create"]');

//            jQuery('#selected-records tr td span').remove();

            ComponentsDropdowns.init();
        });
    </script>
@endsection
