@extends('admin.layout.default')

@section('page-level-css')
{{--    <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>--}}
{{--    <link href="{{ asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>--}}
    <link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('title', 'Manage Users Roles')

@section('page-title')
        <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Manage Users Roles</h1>
    </div>
    <!-- END PAGE TITLE -->
@endsection

@section('breadcrumb')
    <li>
        <a href="{{ url('/') }}">Home</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="{{ url('/roles') }}">Roles</a>
        <i class="fa fa-circle"></i>
    </li>
    <li><span class="active">Users Roles</span></li>
@stop


@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-list font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Users: <span class="text-danger">Roles </span></span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <form method="post" action="/roles/users" role="form" class="form-horizontal">
                            {!! csrf_field() !!}
                            <div class="col-md-12">
                                <div class="table-container">
                                    <div class="table-actions-wrapper">
                                        <span> </span>
                                        Search: <input type="text" class="form-control input-inline input-small input-sm" id="search_param"/>
                                    </div>
                                    <table class="table table-bordered" id="user_role_table">
                                        <thead>
                                        <tr role="row" class="heading">
                                            <th style="width: 1%;">#</th>
                                            <th style="width: 16%;">Names</th>
                                            <th style="width: 16%;">Email</th>
                                            <th style="width: 13%;">User Type</th>
                                            <th style="width: 36%;">Roles</th>
                                            <th style="width: 5%;">View</th>
                                            <th style="width: 5%;">Edit</th>
                                        </tr>
                                        <tr role="row" class="filter">
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                {!! Form::select('user_type_id', $user_types, '', ['class'=>'form-control input-inline input-sm search-params', 'id'=>'user_type_id']) !!}
                                            </td>
                                            <td>
                                                {!! Form::select('role_id', $roles, '', ['class'=>'form-control input-inline input-sm search-params', 'id'=>'role_id']) !!}
                                            </td>
                                            <td colspan="2"></td>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th style="width: 1%;">#</th>
                                            <th style="width: 16%;">Names</th>
                                            <th style="width: 16%;">Email</th>
                                            <th style="width: 13%;">User Type</th>
                                            <th style="width: 36%;">Roles</th>
                                            <th style="width: 5%;">View</th>
                                            <th style="width: 5%;">Edit</th>
                                        </tr>
                                        </tfoot>
                                        <tbody> </tbody>
                                    </table>
                                    <div class="form-actions noborder">
                                        <button type="submit" class="btn blue pull-right">Save Record</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
@endsection

@section('page-level-js')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
{{--    <script src="{{ asset('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>--}}
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <!-- END PAGE LEVEL SCRIPTS -->
@endsection
@section('layout-script')
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="{{ asset('assets/custom/js/roles-permissions/roles.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/roles/users"]');
//            setTableData($('#user_role_table')).init();
//            SetComponentsSelect2('Select a Role').init();
            $.ajaxSetup({ headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' } });
            TableDatatablesAjax.init();
        });
    </script>
@endsection
{{--<div class="input-group select2-bootstrap-append">--}}
    {{--<select id="multi-append'.$user->user_id.'" class="form-control select2" multiple name="role_id[role'.$user->user_id.'][]">--}}
        {{--<option></option>--}}
        {{--'.$select.'--}}
    {{--</select>--}}
    {{--<span class="input-group-btn">--}}
        {{--<button class="btn btn-default" type="button" data-select2-open="multi-append'.$user->user_id.'">--}}
            {{--<span class="glyphicon glyphicon-search"></span>--}}
        {{--</button>--}}
    {{--</span>--}}
{{--</div>--}}
{{--<input name="user_id[]" type="hidden" value="'.$user->user_id.'">--}}