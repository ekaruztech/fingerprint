@extends('admin.layout.default')

@section('page-level-css')
    <link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('title', 'Manage Roles')

@section('page-title')
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Manage Roles</h1>
    </div>
    <!-- END PAGE TITLE -->
@endsection

@section('breadcrumb')
    <li>
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li><span class="active">Roles</span></li>
@stop

@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-list font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Roles</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12 margin-bottom-10">
                            <div class="btn-group">
                                <button class="btn green add_role"> Add New <i class="fa fa-plus"></i> </button>
                            </div>
                        </div>
                        <div class="col-md-12">
                            {!! Form::open([
                                   'method'=>'POST',
                                   'class'=>'form',
                                   'role'=>'form'
                                ])
                            !!}
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-actions" id="role_table">
                                    <thead>
                                        <tr>
                                            <th width="2%">#</th>
                                            <th width="23%">Role (Unique)</th>
                                            <th width="25%">Display Name</th>
                                            <th width="30%">Description</th>
                                            <th width="15%">User Type</th>
                                            <th width="5%">Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th width="2%">#</th>
                                            <th width="23%">Role (Unique)</th>
                                            <th width="25%">Display Name</th>
                                            <th width="30%">Description</th>
                                            <th width="15%">User Type</th>
                                            <th width="5%">Actions</th>
                                        </tr>
                                    </tfoot>
                                    @if(count($roles) > 0)
                                        <tbody>
                                        <?php $i = 1; ?>
                                        @foreach($roles as $role)
                                            <tr>
                                                <td class="text-center">{{$i++}}</td>
                                                <td>
                                                    {!! Form::text('name[]', $role->name, ['placeholder'=>'Role Name (Unique)', 'class'=>'form-control', 'required'=>'required']) !!}
                                                    {!! Form::hidden('role_id[]', $role->role_id, ['class'=>'form-control']) !!}
                                                </td>
                                                <td>{!! Form::text('display_name[]', $role->display_name  , ['placeholder'=>'Role Display name', 'class'=>'form-control','required'=>'required']) !!} </td>
                                                <td>{!! Form::text('description[]', $role->description  , ['placeholder'=>'Role Description', 'class'=>'form-control']) !!} </td>
                                                <td>{!! Form::select('user_type_id[]', $user_types, $role->user_type_id, ['class'=>'form-control selectpicker']) !!} </td>
                                                <td>
                                                    <button class="btn btn-danger btn-rounded btn-condensed btn-xs delete_role">
                                                        <span class="fa fa-trash-o"></span> Delete
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    @else
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td>
                                                {!! Form::text('name[]', '', ['placeholder'=>'Role Name (Unique)', 'class'=>'form-control', 'required'=>'required']) !!}
                                                {!! Form::hidden('role_id[]', '-1', ['class'=>'form-control']) !!}
                                            </td>
                                            <td>{!! Form::text('display_name[]', ''  , ['placeholder'=>'Role Display name', 'class'=>'form-control','required'=>'required']) !!} </td>
                                            <td>{!! Form::text('description[]', ''  , ['placeholder'=>'Role Description', 'class'=>'form-control']) !!} </td>
                                            <td>{!! Form::select('user_type_id[]', $user_types, '', ['class'=>'form-control selectpicker']) !!} </td>
                                            <td>
                                                <button class="btn btn-danger btn-rounded btn-condensed btn-xs">
                                                    <span class="fa fa-times"></span> Remove
                                                </button>
                                            </td>
                                        </tr>
                                    @endif
                                </table>
                                <div class="form-actions noborder">
                                    <button class="btn green add_role"> Add New <i class="fa fa-plus"></i> </button>
                                    <button type="submit" class="btn blue pull-right">Submit</button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hide" id="new_user_types">
        <select class="form-control" name="user_type_id[]">
            @foreach($user_types as $key => $value)
                <option value="{{ $key }}">{{ $value }}</option>
            @endforeach
        </select>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END PAGE BASE CONTENT -->
@endsection

@section('page-level-js')
    <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset('assets/global/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{{ asset('assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ asset('assets/pages/scripts/ui-bootbox.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@endsection

@section('layout-script')
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="{{ asset('assets/custom/js/roles-permissions/roles.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/roles"]');
//            setTableData($('#role_table')).init();
        });
    </script>
@endsection
