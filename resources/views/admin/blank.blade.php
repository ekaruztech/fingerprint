@extends('admin.layout.default')

@section('page-level-css')

@endsection

@section('layout-style')

@endsection

@section('title', 'Blank Page')

@section('page-title')
<!-- BEGIN PAGE TITLE -->
<div class="page-title">
    <h1>Blank Page Layout
        <small>blank page layout</small>
    </h1>
</div>
<!-- END PAGE TITLE -->
@endsection

@section('breadcrumb')
    <li>
        <a href="index.html">Home</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="#">Blank Page</a>
        <i class="fa fa-circle"></i>
    </li>
    <li><span class="active">Page Layouts</span></li>
@stop



@section('content')
    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="note note-info">
        <p> A black page template with a minimal dependency assets to use as a base for any custom page you create </p>
    </div>
    <!-- END PAGE BASE CONTENT -->
@endsection

@section('page-level-js')

@endsection

@section('layout-script')

@endsection
