@extends('admin.layout.default')

@section('title', 'Log Types')

@section('page-title')
        <!-- BEGIN PAGE TITLE -->
<div class="page-title">
    <h1>Manage Log Types</h1>
</div>
<!-- END PAGE TITLE -->
@endsection

@section('breadcrumb')
    <li>
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li><span class="active">Log Types</span></li>
@stop

@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-list font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Log Types</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-10 margin-bottom-10">
                            <div class="btn-group">
                                <button class="btn green add_log_type"> Add New <i class="fa fa-plus"></i> </button>
                            </div>
                        </div>
                        <div class="col-md-12">
                            {!! Form::open([
                                    'method'=>'POST',
                                    'class'=>'form',
                                    'role'=>'form',
                                ])
                            !!}
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-actions" id="log_table">
                                    <thead>
                                        <tr>
                                            <th style="width: 10%;">#</th>
                                            <th style="width: 60%;">Log Type</th>
                                            <th style="width: 10%;">Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th style="width: 10%;">#</th>
                                            <th style="width: 60%;">Log Type</th>
                                            <th style="width: 10%;">Actions</th>
                                        </tr>
                                    </tfoot>
                                    @if(count($log_types) > 0)
                                        <tbody>
                                        <?php $i = 1; ?>
                                        @foreach($log_types as $log_type)
                                            <tr>
                                                <td class="text-center">{{$i++}} </td>
                                                <td>
                                                    {!! Form::text('log_type[]', $log_type->log_type, ['placeholder'=>'Log Type', 'class'=>'form-control', 'required'=>'required']) !!}
                                                    {!! Form::hidden('_id[]', $log_type->_id, ['class'=>'form-control']) !!}
                                                </td>
                                                <td>
                                                    <button class="btn btn-danger btn-rounded btn-condensed btn-xs delete_log_type">
                                                        <span class="fa fa-trash-o"></span> Delete
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    @else
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td>
                                                {!! Form::text('log_type[]', '', ['placeholder'=>'Log Type', 'class'=>'form-control', 'required'=>'required']) !!}
                                                {!! Form::hidden('_id[]', '-1', ['class'=>'form-control']) !!}
                                            </td>
                                            <td>
                                                <button class="btn btn-danger btn-rounded btn-condensed btn-xs">
                                                    <span class="fa fa-times"></span> Remove
                                                </button>
                                            </td>
                                        </tr>
                                    @endif
                                </table>
                                <div class="form-actions noborder">
                                    <button class="btn green add_log_type"> Add New <i class="fa fa-plus"></i> </button>
                                    <button type="submit" class="btn blue pull-right">Submit</button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END PAGE BASE CONTENT -->
@endsection

@section('page-level-js')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset('assets/global/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{{ asset('assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ asset('assets/pages/scripts/ui-bootbox.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@endsection

@section('layout-script')
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="{{ asset('assets/custom/js/master-records/log_type.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/log-types"]');
        });
    </script>
@endsection
