@extends('admin.layout.default')

@section('page-level-css')
<!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL PLUGINS -->
@endsection
@section('layout-style')
<!-- BEGIN PAGE LEVEL STYLES -->
    <link href="{{ asset('assets/pages/css/profile-2.min.css') }}" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
@endsection

@section('title', 'Client Details')

@section('page-title')
<!-- BEGIN PAGE TITLE -->
<div class="page-title">
    <h1>Client Details</h1>
</div>
<!-- END PAGE TITLE -->
@endsection

@section('breadcrumb')
    <li>
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="{{ url('/clients') }}">Clients</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>Client Details</span>
    </li>
@stop


@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
    <!-- END PAGE HEADER-->
    <div class="profile">
        <div class="tabbable-line tabbable-full-width">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab_1_1" data-toggle="tab"> <i class="fa fa-table"></i> Overview </a>
                </li>
                <li>
                    <a href="{{ url('/form-builders/preview/' . $hashIds->encode($client->client_id)) }}"> <i class="fa fa-eye-slash"></i> Preview Form </a>
                </li>
                <li>
                    <a href="{{ url('/form-builders/' . $hashIds->encode($client->client_id)) }}"> <i class="fa fa-edit"></i> Edit Dynamic Form </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1_1">
                    <div class="row">
                        <div class="col-md-3">
                            <ul class="list-unstyled profile-nav">
                                <li>
                                    @if($client->getLogoPath())
                                        <img src="{{ $client->getLogoPath() }}" class="img-responsive pic-bordered" alt="{{ $client->name }}"/>
                                    @else
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                    @endif
                                    <a href="/clients/{{$hashIds->encode($client->client_id)}}/edit" class="profile-edit"> edit </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-8 profile-info">
                                    <h1 class="font-green sbold uppercase">{{ $client->name }}</h1>
                                    <ul class="list-inline">
                                        <li>
                                            <i class="fa fa-phone"></i> {!! ($client->phone_no) ? $client->phone_no : '' !!}
                                        </li>
                                    </ul>
                                        <div class="portlet sale-summary">
                                            <div class="portlet-title">
                                                <div class="caption font-red sbold"> Client Information
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <table class="table table-stripped table-bordered">
                                                    <tr>
                                                        <td>Short Name: </td>
                                                        <td><i class="fa fa-user"></i> {{ $client->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Full Name: </td>
                                                        <td><i class="fa fa-users"></i> {!! ($client->full_name) ? $client->full_name : '<span class="label label-danger">nil</span>' !!}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Email: </td>
                                                        <td><i class="fa fa-envelope"></i> {!! ($client->email) ? $client->email : '<span class="label label-danger">nil</span>' !!}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Mobile Number(s): </td>
                                                        <td><i class="fa fa-phone"></i> {!! ($client->phone_no) ? $client->phone_no : '<span class="label label-danger">nil</span>' !!}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Contact Address</td>
                                                        <td><i class="fa fa-envelope"></i> {!! ($client->address) ? $client->address : '<span class="label label-danger">nil</span>' !!}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
@endsection

@section('page-level-js')
    <script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection

@section('layout-script')
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/clients"]');
        });
    </script>
@endsection
