@extends('admin.layout.default')

@section('page-level-css')
        <!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('title', 'Create New Client')

@section('page-title')
<!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Create New Client</h1>
    </div>
<!-- END PAGE TITLE -->
@endsection

@section('breadcrumb')
    <li>
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="{{ url('/clients') }}">Clients</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>Create New Client</span>
    </li>
@stop


@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-user font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> Create New Client</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    @include('errors.errors')
                    <form method="POST" action="/clients" accept-charset="UTF-8" class="form-horizontal" role="form" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label">Category Type: <span class="font-red">*</span></label>
                                <select class="form-control selectpicker" name="category_type_id" required>
                                    <option value="">Select Category Type</option>
                                    @foreach($category_types as $category)
                                        <option value="{{$category->category_type_id}}"> {{$category->category_type}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Client's Short Name: <span class="font-red">*</span></label>
                                <div class="input-icon input-icon-lg">
                                    <i class="fa fa-user"></i>
                                    <input type="text" class="form-control input-lg" required name="name" placeholder="Short Name" value="{{ old('name') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Client's Full Name:</label>
                                <div class="input-icon input-icon-lg">
                                    <i class="fa fa-user"></i>
                                    <input type="text" class="form-control input-lg" name="full_name" placeholder="Full Name" value="{{ old('full_name') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Email:  <span class="font-red">*</span></label>
                                <div class="input-icon input-icon-lg">
                                    <i class="fa fa-envelope"></i>
                                    <input type="email" required class="form-control input-lg" placeholder="Email" name="email" value="{{ old('email') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Mobile Number(s):  <span class="font-red">*</span></label>
                                <div class="input-icon input-icon-lg">
                                    <i class="fa fa-phone"></i>
                                    <input type="text" class="form-control input-lg" required name="phone_no" placeholder="Mobile Number" value="{{ old('phone_no') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Contact Address: <span class="font-red">*</span></label>
                                <textarea class="form-control input-lg" rows="4" required placeholder="Contact Address" name="address"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Client Logo: </label>
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                    <div>
                                        <span class="btn default btn-file">
                                        <span class="fileinput-new"> Select Logo </span>
                                        <span class="fileinput-exists"> Change Logo</span>
                                        <input type="file" name="logo"></span>
                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove Logo </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue pull-right">Create Client</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
@endsection

@section('page-level-js')
    <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection

@section('layout-script')
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/clients/create"]');
        });
    </script>
@endsection
