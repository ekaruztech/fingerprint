@extends('admin.layout.default')

@section('page-level-css')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('layout-style')
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{ asset('assets/pages/css/profile-2.min.css') }}" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL STYLES -->
@endsection

@section('title', 'Modify Client')

@section('page-title')
<!-- BEGIN PAGE TITLE -->
<div class="page-title">
    <h1>Modify Client</h1>
</div>
<!-- END PAGE TITLE -->
@endsection

@section('breadcrumb')
    <li>
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="{{ url('/clients') }}">Clients</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>Modify Client</span>
    </li>
@stop

@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
    <!-- END PAGE HEADER-->
    <div class="profile">
        <div class="tabbable-line tabbable-full-width">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab_1_1" data-toggle="tab"> Modify Client Info </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1_1">
                    <div class="row profile-account">
                        <div class="col-md-3">
                            <ul class="ver-inline-menu tabbable margin-bottom-10">
                                <li class="{{ (session('active') == 'info') ? 'active' : '' }} {{ (!session()->has('active')) ? 'active' : '' }}">
                                    <a data-toggle="tab" href="#tab_1-1">
                                        <i class="fa fa-cog"></i> Client info
                                    </a>
                                    <span class="after"> </span>
                                </li>
                                <li>
                                    <a href="/form-builders/{{$hashIds->encode($client->client_id)}}">
                                        <i class="fa fa-bold"></i> Form Builder
                                    </a>
                                    <span class="after"> </span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            @include('errors.errors')
                            <div class="tab-content">
                                <div id="tab_1-1" class="tab-pane {{ (session('active') == 'info') ? 'active' : '' }} {{ (!session()->has('active')) ? 'active' : '' }} )">
                                    {!! Form::model(
                                            $client, [
                                            'method'=>'PATCH',
                                            'route'=>['clients.update', $hashIds->encode($client->client_id)],
                                            'class'=>'form-horizontal',
                                            'role'=>'form',
                                            'files'=>true
                                        ])
                                    !!}
                                        <div class="form-body">
                                            {!! Form::hidden('client_id', $client->client_id) !!}
                                            <div class="form-group">
                                                <label class="control-label">Category Type: <span class="font-red">*</span></label>
                                                <select class="form-control selectpicker" name="category_type_id" required>
                                                    <option value="">Select Category Type</option>
                                                    @foreach($category_types as $category)
                                                        @if($client->category_type_id == $category->category_type_id)
                                                            <option selected value="{{$category->category_type_id}}"> {{$category->category_type}}</option>
                                                        @else
                                                            <option value="{{$category->category_type_id}}"> {{$category->category_type}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Client's Status: <span class="font-red">*</span></label>
                                                {!! Form::select('status', [''=>'Select Status', 1=>'Active', 2=>'Not Active'], $client->active, ['class'=>'form-control selectpicker', 'required'=>'required']) !!}
                                            </div>
                                            <div class="form-group">
                                                <label>Client's Short Name: <span class="font-red">*</span></label>
                                                <div class="input-icon input-icon-lg">
                                                    <i class="fa fa-user"></i>
                                                    <input type="text" class="form-control input-lg" required name="name" placeholder="Short Name" value="{{ $client->name }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Client's Full Name:</label>
                                                <div class="input-icon input-icon-lg">
                                                    <i class="fa fa-user"></i>
                                                    <input type="text" class="form-control input-lg" name="full_name" placeholder="Full Name" value="{{ $client->full_name }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Email:  <span class="font-red">*</span></label>
                                                <div class="input-icon input-icon-lg">
                                                    <i class="fa fa-envelope"></i>
                                                    <input type="email" required class="form-control input-lg" placeholder="Email" name="email" value="{{ $client->email }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Mobile Number(s):  <span class="font-red">*</span></label>
                                                <div class="input-icon input-icon-lg">
                                                    <i class="fa fa-phone"></i>
                                                    <input type="text" class="form-control input-lg" required name="phone_no" placeholder="Mobile Number" value="{{ $client->phone_no }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Contact Address: <span class="font-red">*</span></label>
                                                <textarea class="form-control input-lg" rows="4" required placeholder="Contact Address" name="address">{{ $client->address }}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>Client Logo: </label>
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        @if($client->getLogoPath())
                                                            <img src="{{ $client->getLogoPath() }}" class="img-responsive pic-bordered" alt="{{ $client->name }}"/>
                                                        @else
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                                        @endif
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                        <span class="fileinput-new"> Select Logo </span>
                                                        <span class="fileinput-exists"> Change Logo</span>
                                                        <input type="file" name="logo"></span>
                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove Logo </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="margiv-top-10">
                                            <button class="btn green"> Update Client Info</button>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                        <!--end col-md-9-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
@endsection

@section('page-level-js')
    <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection

@section('layout-script')
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/clients/create"]');
        });
    </script>
    <!-- END PAGE LEVEL SCRIPTS -->
@endsection



