@extends('admin.layout.default')

@section('page-level-css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->
@endsection

@section('title', 'Manage Client')

@section('page-title')
        <!-- BEGIN PAGE TITLE -->
<div class="page-title">
    <h1>Manage Client</h1>
</div>
<!-- END PAGE TITLE -->
@endsection

@section('breadcrumb')
    <li>
        <a href="{{ url('/dashboard') }}">Dashboard</a>
        <i class="fa fa-dashboard"></i>
    </li>
    <li><span class="active">Manage Clients</span></li>
@stop

@section('content')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="note note-info">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-list font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Registered Clients</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                                <i class="fa fa-share"></i>
                                <span class="hidden-xs"> Tool Actions</span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right" id="clients_datatable_tools">
                                <li>
                                    <a href="javascript:;" data-action="0" class="tool-action">
                                        <i class="icon-printer"></i> Print</a>
                                </li>
                                <li>
                                    <a href="javascript:;" data-action="1" class="tool-action">
                                        <i class="icon-check"></i> Copy</a>
                                </li>
                                {{--<li>--}}
                                {{--<a href="javascript:;" data-action="2" class="tool-action">--}}
                                {{--<i class="icon-doc"></i> PDF</a>--}}
                                {{--</li>--}}
                                <li class="divider"> </li>
                                <li>
                                    <a href="javascript:;" data-action="3" class="tool-action">
                                        <i class="icon-paper-clip"></i> Excel</a>
                                </li>
                                <li>
                                    <a href="javascript:;" data-action="4" class="tool-action">
                                        <i class="icon-cloud-upload"></i> CSV</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <div class="table-actions-wrapper">
                            <span> </span>
                            Search: <input type="text" class="form-control input-inline input-small input-sm" id="search_param"/>
                        </div>
                        <table class="table table-striped table-bordered table-hover" id="clients_datatable">
                            <thead>
                                <tr role="row" class="heading">
                                    <th width="2%">#</th>
                                    <th width="28%">Names</th>
                                    <th width="10%">Category Type</th>
                                    <th width="15%">Email</th>
                                    <th width="10%">Phone No.</th>
                                    <th width="10%">Status</th>
                                    <th width="5%">View</th>
                                    <th width="5%">Edit</th>
                                    <th width="5%">URL</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr role="row" class="heading">
                                    <th width="2%">#</th>
                                    <th width="28%">Names</th>
                                    <th width="10%">Category Type</th>
                                    <th width="15%">Email</th>
                                    <th width="10%">Phone No.</th>
                                    <th width="10%">Status</th>
                                    <th width="5%">View</th>
                                    <th width="5%">Edit</th>
                                    <th width="5%">URL</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php $i=1; ?>
                                @foreach ($clients as $client)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $client->name }}</td>
                                        <td>{!! (!empty($client->categoryType()->first())) ? $client->categoryType->category_type : '<span class="label label-danger">nil</span>' !!}</td>
                                        <td>{!! ($client->email) ? $client->email : '<span class="label label-danger">nil</span>' !!}</td>
                                        <td>{!! ($client->phone_no) ? $client->phone_no : '<span class="label label-danger">nil</span>' !!}</td>
                                        <td>
                                            @if($client->status == 1)
                                                <button class="btn btn-success btn-rounded btn-xs client_status" rel="2" value="{{$client->client_id}}">
                                                    <span class="fa fa-user-plus"></span> Active
                                                </button>
                                            @else
                                                <button class="btn btn-danger btn-rounded btn-xs client_status" rel="1" value="{{$client->client_id}}">
                                                    <span class="fa fa-user-times"></span> Not Active
                                                </button>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="/clients/{{$hashIds->encode($client->client_id)}}" class="btn btn-info btn-rounded btn-condensed btn-xs">
                                                <span class="fa fa-eye-slash"></span>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="/clients/{{$hashIds->encode($client->client_id)}}/edit" class="btn btn-warning btn-rounded btn-condensed btn-xs">
                                                <span class="fa fa-edit"></span>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="/records/create/{{$hashIds->encode($client->client_id)}}" class="btn btn-success btn-rounded btn-condensed btn-xs">
                                                <span class="fa fa-link"></span>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
@endsection
@section('page-level-js')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/ui-bootbox.min.js') }}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
@endsection

@section('layout-script')
    <script src="{{ asset('assets/custom/js/master-records/clients.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            setTabActive('[href="/clients"]');
            setTableDataTools($('#clients_datatable'), 'clients_datatable_tools').init();
//            TableDatatablesAjax.init();
        });
    </script>
@endsection
