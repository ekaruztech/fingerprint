@extends('admin.partials.email')

@section('title', 'Confirmation Mail')

@section('content')

    <h3>Hi, <strong><i>{{ $user->fullNames() }}</i></strong></h3>
    <h3> Welcome to {{env('APP_NAME')}} Application</h3>
    <div style="margin-bottom: 12px; font-weight: bolder; font-size:18px; line-height:1.8;">
        <?php $content = explode("\n", $content); ?>
        @foreach($content as $line)
            <p>{!! $line !!}</p>
        @endforeach
        <h2>
            <a class='link3' href="{{ env('APP_URL') }}users/confirm/{{ $hashIds->encode($user->user_id) }}/{{ $user->verification_code }}">
                Click here to Confirm
            </a>
        </h2>
    </div>

@endsection