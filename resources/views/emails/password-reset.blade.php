@extends('admin.partials.email')

@section('title', 'Password Reset')

@section('content')

    <h3>Hi, <strong><i>{{ $req->fullNames() }}</i></strong></h3>
    <h3> Welcome to {{env('APP_NAME')}} Application</h3>
    <div style="margin-bottom: 12px; font-weight: bolder; font-size:18px; line-height:1.8;">
        <?php $content = explode("\n", $content); ?>
        @foreach($content as $line)
            <p>{!! $line !!}</p>
        @endforeach
        <h6>
            <a class='link3' href="{{ env('APP_URL') }}login">
                click here to sign in
            </a>
        </h6>
    </div>

@endsection