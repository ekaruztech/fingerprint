@extends('admin.partials.email')

@section('title', 'Invitation Mail')

@section('content')

    <h3>Hi, <strong><i>{{ $user->fullNames() }}</i></strong></h3>
    <div class="lead" style="margin-bottom: 10px; font-weight: normal; font-size:17px; line-height:1.6;">
        <?php $content = explode("\n", $content); ?>
        @foreach($content as $line)
            <p>{!! $line !!}</p>
        @endforeach
        <br>
        <h2>
            <a class='link3' href="{{ env('APP_URL') }}users/confirm/{{ $hashIds->encode($user->user_id) }}/{{ $user->verification_code }}">
                Click here to Accept
            </a>
        </h2>
        <br>
    </div>

@endsection