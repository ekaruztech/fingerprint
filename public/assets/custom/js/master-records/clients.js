/**
 * Created by Cecilee2 on 8/4/2015.
 */

$(function () {
    $(document.body).on('click', '.client_status',function(e){
        e.preventDefault();

        var parent = $(this).parent().parent();
        var client = parent.children(':nth-child(2)').html();
        var client_id = $(this).val();
        var value = $(this).attr('rel');
        var re = (value==1) ? 'Activate' : 'Deactivate';

        bootbox.dialog({
            message: 'Are You sure You want to <span class="bold">'+ re +' Client: '+client+'</span> and all its reference places',
            title: '<span class="bold font-red">Warning Alert</span>',
            buttons: {
                danger: {
                    label: "NO",
                    className: "btn-default",
                    callback: function() {
                        $(this).hide();
                    }
                },
                success: {
                    label: "YES",
                    className: "btn-success",
                    callback: function() {
                        $.ajax({
                            type: 'GET',
                            async: true,
                            url: '/clients/status/' + client_id + '/' + value,
                            success: function(data,textStatus){
                                window.location.replace('/clients');
                            },
                            error: function(xhr,textStatus,error){
                                bootbox.alert("Error encountered pls try again later..", function() {
                                    $(this).hide();
                                });
                            }
                        });
                    }
                }
            }
        });
    });
});