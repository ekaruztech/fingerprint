/**
 * Created by Cecilee2 on 8/4/2015.
 */

$(function () {

    $('.add_log_type').click(function(e){
        e.preventDefault();
        var clone_row = $('#log_table tbody tr:last-child').clone();

        $('#log_table tbody').append(clone_row);

        clone_row.children(':nth-child(1)').html( parseInt(clone_row.children(':nth-child(1)').html())+1);
        clone_row.children(':nth-child(2)').children('input').val('');
        clone_row.children(':nth-child(2)').children('input[type=hidden]').val(-1);
        clone_row.children(':last-child').html('<button class="btn btn-danger btn-rounded btn-condensed btn-xs remove_log_type"><span class="fa fa-times"></span> Remove</button>');
    });

    $(document.body).on('click','.remove_log_type',function(){
        $(this).parent().parent().remove();
    });

    $(document.body).on('click', '.delete_log_type',function(e){
        e.preventDefault();

        var parent = $(this).parent().parent();
        var log_type = parent.children(':nth-child(2)').children('input').val();
        var log_type_id = parent.children(':nth-child(2)').children('input[type=hidden]').val();

        bootbox.dialog({
            message: 'Are You sure You want to permanently delete log type:<span class="bold"> '+log_type+'</span>',
            title: '<span class="bold font-red">Warning Alert</span>',
            buttons: {
                danger: {
                    label: "NO",
                    className: "btn-default",
                    callback: function() {
                        $(this).hide();
                    }
                },
                success: {
                    label: "YES",
                    className: "btn-success",
                    callback: function() {
                        $.ajax({
                            type: 'GET',
                            async: true,
                            url: '/log-types/delete/' + log_type_id,
                            success: function(data,textStatus){
                                window.location.replace('/log-types');
                            },
                            error: function(xhr,textStatus,error){
                                bootbox.alert("Error encountered pls try again later..", function() {
                                    $(this).hide();
                                });
                            }
                        });
                    }
                }
            }
        });
    });
});




