/**
 * Created by Cecilee2 on 8/4/2015.
 */

$(function () {
    //Delete a record
    $(document.body).on('click', '.delete_record',function(e){
        e.preventDefault();

        var parent = $(this).parent().parent();
        var record = parent.children(':nth-child(2)').html();
        var record_id = $(this).val();

        bootbox.dialog({
            message: 'Are You sure You want to permanently delete Record Group: <span class="bold"> '+record+'</span>',
            title: '<span class="bold font-red">Warning Alert</span>',
            buttons: {
                danger: {
                    label: "NO",
                    className: "btn-default",
                    callback: function() {
                        $(this).hide();
                    }
                },
                success: {
                    label: "YES",
                    className: "btn-success",
                    callback: function() {
                        $.ajax({
                            type: 'GET',
                            async: true,
                            url: '/record-groups/delete/' + record_id,
                            success: function(data,textStatus){
                                window.location.replace('/records/list-groups');
                            },
                            error: function(xhr,textStatus,error){
                                bootbox.alert("Error encountered pls try again later..", function() {
                                    $(this).hide();
                                });
                            }
                        });
                    }
                }
            }
        });
    });

    if($('#more_records').is(':checked')){
        $('#show_records').removeClass('hide');
    }
    //Toggle the More Records Checkbox
    $(document.body).on('switchChange.bootstrapSwitch', '#more_records',function(){
        if(this.checked){
            $('#show_records').removeClass('hide');
        }else {
            $('#show_records').addClass('hide');
            $('#category_id').val('');
            $('#available-records').html('');
            $('#selected-records').html('');
        }
    });

    $(document.body).on('change', '#category_id',function(){
        $('#records_table').removeClass('hide');
        var id = $(this).val();
        if(id !== ''){
            $.ajax({
                type: "get",
                async: true,
                //data:parent.serialize(),
                url: '/record-groups/category/' + id,
                dataType: "json",
                success: function (data, textStatus) {
                    var output = '';
                    // console.log(data);
                    if(data.flag == 1){
                        $.each(data.data, function(key, value) {
                            output += '<tr><td>' + value.unique +'</td>';
                            output += '<td>' + value.name +'</td>';
                            output += '<td><div class="checker"><span class=""><input name="records[]" type="checkbox" class="each-record" value="'+value.id+'"></span></div></td></tr>';
                        });
                        $('#available-records').html(output);
                    }else if(data.flag == 0){
                        $('#available-records').html('<tr><td colspan="3">No Record Found for Category: '+$('#category_id').children('option:selected').text()+'</td></tr>');
                    }
                }
            });
        }else {
            $('#available-records').html('');
            $('#selected-records').html('');
        }
    });

    //On check add or remove the record
    $(document.body).on('click', '.each-record', function(){
        var parent_tr = $(this).parent().parent().parent().parent();
        // var tr = $(this).parent().parent().parent().parent().clone();

        if($(this).prop('checked') === true){
            $(this).attr('checked', 'checked');
            $(this).parent().addClass('checked');
            $("#selected-records").prepend($(this).parent().parent().parent().parent().clone());
            parent_tr.remove();

        }else if($(this).prop('checked') === false){
            $(this).removeAttr("checked");
            $(this).parent().removeClass('checked');
            $("#available-records").prepend($(this).parent().parent().parent().parent().clone());
            parent_tr.remove();
        }
    });
});

var ComponentsDropdowns = function () {

    var handleMultiSelect = function () {
        $('#record_multi_select').multiSelect({
            selectableOptgroup: true,
            selectableHeader: '<span class="label label-info"><strong>List of Available Records</strong></span>',
            selectableFooter: '<span class="label label-info"><strong>List of Available Records</strong></span>',
            selectionHeader: '<span class="label label-success"><strong>List of Selected Records</strong></span>',
            selectionFooter: '<span class="label label-success"><strong>List of Selected Records</strong></span>',
            cssClass: 'multi-select-records'
        });
    };
    var handleMultiSelectRefresh = function () {
        $('#record_multi_select').multiSelect('refresh');
    };

    return {
        //main function to initiate the module
        init: function () {
            handleMultiSelect();
        },
        refresh: function() {
            handleMultiSelectRefresh();
        }
    };
}();

var ComponentsDatePicker = function () {

    var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            dateFormat: 'yy/mm/dd',
            autoclose: true
        }).on('changeDate', function(ev) {

            App.blockUI({
                target: '#log_results',
                animate: true
            });
            // var date = new Date(ev.date).toLocaleDateString();
            // console.log('Raw 99: ',  ev.date.getFullYear() + '-' + (ev.date.getMonth() + 1) + '-' + ev.date.getDate());
            var _id = $('#group_id').val();

            $.ajax({
                type: "POST",
                url: '/record-groups/logs',
                data: {_id:_id, year:ev.date.getFullYear(), month: (ev.date.getMonth() + 1), day:ev.date.getDate()},
                success: function (data) {
                    console.log(data);
                    var present = ''; var absent = '';
                    if (data.flag == true && data.log == true){
                        if (data.present) {
                            $.each(data.present, function (key, value) {
                                present += '<tr>' +
                                    '<td>' + (key + 1) + '</td>' +
                                    '<td>' + value.unique + '</td>' +
                                    '<td>' + value.name + '</td>' +
                                    '<td>' + value.gender + '</td>' +
                                    '<td>' + value.type + '</td>' +
                                    '</tr>';
                            });
                            $('#log_present').html(present);
                        }else {
                            $('#log_present').html('<tr><th colspan="4">No Log Record (Present) on: '+data.formatedDate+' </th></tr>');
                        }
                        if (data.absent) {
                            $.each(data.absent, function (key, value) {
                                absent += '<tr>' +
                                    '<td>' + (key + 1) + '</td>' +
                                    '<td>' + value.unique + '</td>' +
                                    '<td>' + value.name + '</td>' +
                                    '<td>' + value.gender + '</td>' +
                                    '</tr>';
                            });
                            $('#log_absent').html(absent);
                        }else {
                            $('#log_absent').html('<tr><th colspan="4">No Log Record (Absent) on: '+data.formatedDate+' </th></tr>');
                        }
                        $('#log_results').removeClass('hide');
                        set_msg_box($('#log_error_msg'), 'Record Logs on: ' + data.formatedDate);
            
                        //Display Pie Chart
                        if ($('#record_log_chart').size() !== 0) {
                            $.plot($("#record_log_chart"), data.chart, {
                                series: {
                                    pie: {
                                        show: true,
                                        radius: 0.8,
                                        label: {
                                            show: true,
                                            radius: 3 / 4,
                                            formatter: function (label, series) {
                                                return '<div style="font-size:10pt;text-align:center;padding:2px;color:white;">' + label + '(' + series.value + ')<br/>' + Math.round(series.percent) + '%</div>';
                                            },
                                            background: {
                                                opacity: 0.8,
                                                color: '#000'
                                            }
                                        }
                                    }
                                },
                                legend: {
                                    show: false
                                }
                            });
                        }
                        //Scroll To Div
                        scroll2Div($('#log_results'));
            
                    }else {
                        $('#log_present').html('');
                        $('#log_absent').html('');
                        $('#record_log_chart').html('');
                        set_msg_box($('#log_error_msg'), 'No Log Record Found / Taken for this Date: ' + data.formatedDate);
                    }
                    window.setTimeout(function() {
                        App.unblockUI('#log_results');
                    }, 2500);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    set_msg_box($('#log_error_msg'), 'Error...Kindly Try Again', 2);
                    App.unblockUI('#log_results');
                }
            });

        });
    };
    return {
        //main function to initiate the module
        init: function () {
            initPickers();
        }
    };
}();
// SELECT SUM(`amount`), COUNT(*), `category_id`, DATE(created) FROM `order_items`
// WHERE `created` >= '2015-01-01' GROUP BY `category_id`, YEAR(created), MONTH(created)