/**
 * Created by Cecilee2 on 8/4/2015.
 */

$(function () {
    //Dependent List Box for Categories
    $(document.body).on('change', '.category-levels',function(e) {
        var parent = $(this);
        var child = $(this).parent().next();
        var lastChild = child.next();
        var lastChild2 = lastChild.next();
        var lastChild3 = lastChild2.next();

        if (parent.val() == '') {
            child.html('');
            lastChild.html('');
            lastChild2.html('');
            lastChild3.html('');
        } else {
            child.html('<img src="/assets/custom/img/admin/loading.gif" alt="Loading..."/>');
            $.ajax({
                type: "get",
                async: true,
                //data:parent.serialize(),
                url: '/list-box/categories/' + parent.val(),
                dataType: "json",
                success: function (data, textStatus) {
                    var output = '';
                    // console.log('Data: '+data, ' Level: ' + data.level);
                    //TODO Custom Bells
                    if(data.level == 3) {
                        output += '<label>Subject Combinations: '+data.parent+' <span class="font-red">*</span></label>';
                        output += '<div class="checkbox-list">';
                        $.each(data.data, function(key, value) {
                            output += '<label>';
                            output += '<input type="checkbox" value="'+value.name+'" name="subjects[]"> <span style="padding-left:10px;"></span>' + value.name + ' </label>';
                        });
                        output += '</div>';
                        child.html(output);
                    }else {
                        if(data.flag == 1){
                            output += '<label class="control-label">Sub Category: '+data.parent+' <span class="font-red">*</span></label>';
                            output += '<select class="form-control category-levels" required name="category_id">';
                            output += '<option value="">Nothing Selected</option>';
                            $.each(data.data, function(key, value) {
                                output += '<option value="'+value.category_id+'">' + value.name +'</option>';
                            });
                            output += '</select>';
                            child.html(output);
                            lastChild.html('');
                            lastChild2.html('');
                            lastChild3.html('');
                            parent.removeAttr('name');
                        }else if(data.flag == 0){
                            parent.attr('name', 'category_id');
                            child.html('');
                            lastChild.html('');
                            lastChild2.html('');
                            lastChild3.html('');
                        }
                    }
                    // console.log(data);
                }
            });
        }
        return false;
    });

    //Delete a record
    $(document.body).on('click', '.delete_record',function(e){
        e.preventDefault();

        var parent = $(this).parent().parent();
        var record = parent.children(':nth-child(2)').html();
        var record_id = $(this).val();

        bootbox.dialog({
            message: 'Are You sure You want to permanently delete Record: <span class="bold"> '+record+'</span> and all its reference places',
            title: '<span class="bold font-red">Warning Alert</span>',
            buttons: {
                danger: {
                    label: "NO",
                    className: "btn-default",
                    callback: function() {
                        $(this).hide();
                    }
                },
                success: {
                    label: "YES",
                    className: "btn-success",
                    callback: function() {
                        $.ajax({
                            type: 'GET',
                            async: true,
                            url: '/records/delete/' + record_id,
                            success: function(data,textStatus){
                                window.location.replace('/records');
                            },
                            error: function(xhr,textStatus,error){
                                bootbox.alert("Error encountered pls try again later..", function() {
                                    $(this).hide();
                                });
                            }
                        });
                    }
                }
            }
        });
    });
});