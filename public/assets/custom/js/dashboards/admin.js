var ChartsGender = function() {

    return {
        //main function to initiate the module
        initPieCharts: function() {
            // Category Types
            if ($('#gender').size() !== 0) {
                $.ajax({
                    type: "GET",
                    url: '/dashboard/record-gender',
                    success: function (data) {
                        //console.log(data);
                        $.plot($("#gender"), data, {
                            series: {
                                pie: {
                                    show: true,
                                    radius: 0.8,
                                    label: {
                                        show: true,
                                        radius: 3 / 4,
                                        formatter: function(label, series) {
                                            return '<div style="font-size:10pt;text-align:center;padding:2px;color:white;">' + label + '(' + series.value + ')<br/>' + Math.round(series.percent) + '%</div>';
                                        },
                                        background: {
                                            opacity: 0.8,
                                            color: '#000'
                                        }
                                    }
                                }
                            },
                            legend: {
                                show: false
                            }
                        });
                    }
                });
            }
        }
    };
}();

jQuery(document).ready(function() {
    ChartsGender.initPieCharts();
});