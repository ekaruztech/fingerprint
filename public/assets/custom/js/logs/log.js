var ChartsLogTypes = function() {

    return {
        //main function to initiate the module
        initPieCharts: function() {
            // Category Types
            if ($('#log_types').size() !== 0) {
                $.ajax({
                    type: "GET",
                    url: '/logs/log-types',
                    success: function (data) {
                        //console.log(data);
                        $.plot($("#log_types"), data, {
                            series: {
                                pie: {
                                    show: true,
                                    radius: 0.8,
                                    label: {
                                        show: true,
                                        radius: 3 / 4,
                                        formatter: function(label, series) {
                                            return '<div style="font-size:10pt;text-align:center;padding:2px;color:white;">' + label + '(' + series.value + ')<br/>' + Math.round(series.percent) + '%</div>';
                                        },
                                        background: {
                                            opacity: 0.8,
                                            color: '#000'
                                        }
                                    }
                                }
                            },
                            legend: {
                                show: false
                            }
                        });
                    }
                });
            }
        }
    };
}();

var ChartsUserLogTypes = function() {

    return {
        //main function to initiate the module
        initPieCharts: function(id) {
            // console.log('DATA::', id);
            // Category Types
            if ($('#log_types').size() !== 0) {
                $.ajax({
                    type: "GET",
                    url: '/logs/users-log-types/' + id,
                    success: function (data) {
                        //console.log(data);
                        $.plot($("#log_types"), data, {
                            series: {
                                pie: {
                                    show: true,
                                    radius: 0.8,
                                    label: {
                                        show: true,
                                        radius: 3 / 4,
                                        formatter: function(label, series) {
                                            return '<div style="font-size:10pt;text-align:center;padding:2px;color:white;">' + label + '(' + series.value + ')<br/>' + Math.round(series.percent) + '%</div>';
                                        },
                                        background: {
                                            opacity: 0.8,
                                            color: '#000'
                                        }
                                    }
                                }
                            },
                            legend: {
                                show: false
                            }
                        });
                    }
                });
            }
        }
    };
}();
var ChartsAmcharts = function() {

    var initChartClients = function() {

        $.ajax({
            type: "GET",
            url: '/logs/logs',
            success: function(data){
                try{
                    var chart = AmCharts.makeChart("logs_records", {
                        "theme": "light",
                        "type": "serial",
                        "startDuration": 2,

                        "fontFamily": 'Open Sans',

                        "color":    '#888',

                        "dataProvider": data,
                        "valueAxes": [{
                            "position": "left",
                            "axisAlpha": 0,
                            "gridAlpha": 0
                        }],
                        "graphs": [{
                            "balloonText": "[[category]]: <b>[[value]] Logs</b>",
                            "colorField": "color",
                            "fillAlphas": 0.85,
                            "lineAlpha": 0.1,
                            "type": "column",
                            "topRadius": 1,
                            "valueField": "records"
                        }],
                        "depth3D": 40,
                        "angle": 30,
                        "chartCursor": {
                            "categoryBalloonEnabled": false,
                            "cursorAlpha": 0,
                            "zoomable": false
                        },
                        "categoryField": "type",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "axisAlpha": 0,
                            "gridAlpha": 0

                        },
                        "exportConfig": {
                            "menuTop": "20px",
                            "menuRight": "20px",
                            "menuItems": [{
                                "icon": '/lib/3/images/export.png',
                                "format": 'png'
                            }]
                        }
                    }, 0);

                    $('#logs_records').closest('.portlet').find('.fullscreen').click(function() {
                        chart.invalidateSize();
                    });
                } catch (exception) {
                    $('#logs_records').html('<div class="info-box  bg-info-dark  text-white"><div class="info-details"><h4>'+data+'</h4></div></div>');
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                $('#logs_records').html(errorThrown);
            }
        });
    };
    return {
        //main function to initiate the module
        init: function() {
            initChartClients();
        }

    };

}();

var ChartsAmchartsUsers = function() {

    var initChartClients = function(id) {
        // console.log('DATA::', id);
        $.ajax({
            type: "GET",
            url: '/logs/users-logs/' + id,
            success: function(data){
                try{
                    var chart = AmCharts.makeChart("logs_records", {
                        "theme": "light",
                        "type": "serial",
                        "startDuration": 2,

                        "fontFamily": 'Open Sans',

                        "color":    '#888',

                        "dataProvider": data,
                        "valueAxes": [{
                            "position": "left",
                            "axisAlpha": 0,
                            "gridAlpha": 0
                        }],
                        "graphs": [{
                            "balloonText": "[[category]]: <b>[[value]] Logs</b>",
                            "colorField": "color",
                            "fillAlphas": 0.85,
                            "lineAlpha": 0.1,
                            "type": "column",
                            "topRadius": 1,
                            "valueField": "records"
                        }],
                        "depth3D": 40,
                        "angle": 30,
                        "chartCursor": {
                            "categoryBalloonEnabled": false,
                            "cursorAlpha": 0,
                            "zoomable": false
                        },
                        "categoryField": "type",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "axisAlpha": 0,
                            "gridAlpha": 0

                        },
                        "exportConfig": {
                            "menuTop": "20px",
                            "menuRight": "20px",
                            "menuItems": [{
                                "icon": '/lib/3/images/export.png',
                                "format": 'png'
                            }]
                        }
                    }, 0);

                    $('#logs_records').closest('.portlet').find('.fullscreen').click(function() {
                        chart.invalidateSize();
                    });
                } catch (exception) {
                    $('#logs_records').html('<div class="info-box  bg-info-dark  text-white"><div class="info-details"><h4>'+data+'</h4></div></div>');
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                $('#logs_records').html(errorThrown);
            }
        });
    };
    return {
        //main function to initiate the module
        init: function(user_id) {
            initChartClients(user_id);
        }

    };

}();

// jQuery(document).ready(function() {
//     ChartsLogTypes.initPieCharts();
//     ChartsAmcharts.init();
// });