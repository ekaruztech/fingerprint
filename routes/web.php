<?php
//https://kheengz@bitbucket.org/ekaruztech/fingerprint.git
//$2y$10$C3QDXfDD5OtqWqTN4T56w.ITaAGYpS83web6bZnwV5W6N0LiXtjYC

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
//Dashboard Route
Route::get('/', 'Admin\Utility\DashboardController@index');
Route::get('/home', 'Admin\Utility\DashboardController@index');
Route::group(['namespace' => 'Admin\Utility', 'prefix'=>'/dashboard'], function() {
    Route::get('/', 'DashboardController@index');
    Route::get('/clients-records', 'DashboardController@clientsRecords');
    Route::get('/category-types', 'DashboardController@categoryTypes');
    Route::get('/record-gender', 'DashboardController@recordGender');
});


//Authentication Route
Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/login2', 'Auth\LoginController@getLogin2');
Route::post('/login2', 'Auth\LoginController@postLogin2');

//All Users and Profile Routes
Route::group(['namespace' => 'Admin\Users'], function() {
    //Users Route
    Route::group(['prefix'=>'users'], function() {
        Route::get('/change', 'UserController@change');
        Route::post('/change', 'UserController@postChange');

        Route::post('/all-users', 'UserController@postAllUsers');
        Route::get('/status/{user_id}/{status}', 'UserController@getStatus');
        Route::get('/confirm/{user_id}/{code}', 'UserController@confirm');
        Route::get('/delete/{id}', 'UserController@getDelete');
        Route::post('/avatar', 'UserController@avatar');
    });
    Route::get('/users/confirm/{id}/{code}', 'UserController@confirm');
    Route::resource('users', 'UserController');

    //Profiles Route
    Route::group(['prefix'=>'profiles'], function() {
        Route::get('/', 'ProfileController@index');
        Route::get('/edit', 'ProfileController@edit');
        Route::post('/edit', 'ProfileController@update');
        Route::post('/avatar', 'ProfileController@avatar');
    });

    //User Types Route
    Route::group(['prefix'=>'user-types'], function() {
        Route::get('/', 'UserTypeController@index');
        Route::get('/index', 'UserTypeController@index');
        Route::post('/', 'UserTypeController@postIndex');
        Route::get('/delete/{user_type_id}', 'UserTypeController@delete');
    });

});

//All Records Routes
Route::group(['namespace' => 'Admin\Records'], function() {
    //Form Builder Route
    Route::group(['prefix'=>'form-builders'], function() {
        Route::get('/preview/{id?}', 'FormBuilderController@preview');
        Route::get('/{id?}', 'FormBuilderController@create');
        Route::post('/{id}', 'FormBuilderController@store');
    });
    //Records Route
    Route::group(['prefix'=>'records'], function() {
        Route::get('/delete/{id}', 'RecordController@delete');
        Route::get('/create/{id}', 'RecordController@create');
        Route::get('/export', 'RecordController@export');
        Route::post('/edit', 'RecordController@update');
    });

    //Records Group Route
    Route::group(['prefix'=>'record-groups'], function() {
        Route::get('/create', 'RecordGroupController@create');
        Route::post('/create', 'RecordGroupController@store');
        Route::get('/', 'RecordGroupController@index');
        Route::get('/edit/{id}', 'RecordGroupController@edit');
        Route::post('/edit', 'RecordGroupController@update');
        Route::get('/view/{id}', 'RecordGroupController@show');
        Route::get('/category/{id}', 'RecordGroupController@getRecordByCategory');
        Route::get('/logs', 'RecordGroupController@getRecordLogs');
        Route::post('/logs', 'RecordGroupController@postRecordLogs');
        Route::get('/delete/{id}', 'RecordGroupController@delete');
    });
    Route::resource('records', 'RecordController');
});


//All Master Record Routes
Route::group(['namespace' => 'Admin\MasterRecords'], function() {
    //Categories Route
    Route::group(['prefix'=>'categories'], function() {
        Route::get('/', 'CategoryController@index');
        Route::get('/index', 'CategoryController@index');

        Route::get('/level-1', 'CategoryController@getLevelOne');
        Route::post('/level-1', 'CategoryController@postLevelOne');

        Route::get('/level-2', 'CategoryController@getLevelTwo');
        Route::post('/level-2', 'CategoryController@postLevelTwo');

        Route::get('/level-3', 'CategoryController@getLevelThree');
        Route::post('/level-3', 'CategoryController@postLevelThree');

        Route::get('/level-4', 'CategoryController@getLevelFour');
        Route::post('/level-4', 'CategoryController@postLevelFour');

        Route::get('/level-5', 'CategoryController@getLevelFive');
        Route::post('/level-5', 'CategoryController@postLevelFive');

        Route::get('/delete/{category_id}', 'CategoryController@deleteCategory');
    });
    //Category Types Route
    Route::group(['prefix'=>'category-types'], function() {
        Route::get('/', 'CategoryTypeController@index');
        Route::get('/index', 'CategoryTypeController@index');
        Route::post('/', 'CategoryTypeController@postIndex');
        Route::get('/delete/{id}', 'CategoryTypeController@delete');
    });
    //Menus Route
    Route::group(['prefix'=>'menus'], function() {
        Route::get('/', 'MenuController@index');
        Route::get('/index', 'MenuController@index');

        Route::get('/level-1', 'MenuController@getLevelOne');
        Route::post('/level-1', 'MenuController@postLevelOne');

        Route::get('/level-2/{id?}', 'MenuController@getLevelTwo');
        Route::post('/level-2/{id?}', 'MenuController@postLevelTwo');

        Route::get('/level-3/{id?}', 'MenuController@getLevelThree');
        Route::post('/level-3/{id?}', 'MenuController@postLevelThree');

        Route::get('/level-4/{id?}', 'MenuController@getLevelFour');
        Route::post('/level-4/{id?}', 'MenuController@postLevelFour');

        Route::get('/level-5/{id?}', 'MenuController@getLevelFive');
        Route::post('/level-5/{id?}', 'MenuController@postLevelFive');

        Route::get('/delete/{menu_id}', 'MenuController@delete');
        Route::post('/filter', 'MenuController@menusFilter');
    });
});

//Clients Route
Route::get('/clients/status/{id}/{value}', 'Admin\MasterRecords\ClientController@status');
Route::resource('clients', 'Admin\MasterRecords\ClientController');

//ListBox Route
Route::group(['namespace' => 'Admin\Utility', 'prefix'=>'list-box'], function() {
    Route::get('/categories/{id}', 'ListBoxController@category');
});

//Roles And Permissions Route
Route::group(['namespace' => 'Admin\RolesAndPermission', 'prefix'=>'roles'], function() {
    Route::get('/', 'RolesController@index');
    Route::get('/index', 'RolesController@index');
    Route::post('/', 'RolesController@postIndex');
    
    Route::get('/users', 'RolesController@getUsers');
    Route::post('/users', 'RolesController@postUsers');
    Route::post('/all-users', 'RolesController@postAllUsers');
    
    Route::get('/delete/{role_id}', 'RolesController@delete');
});

//All Logs Route
Route::group(['namespace' => 'Admin\Logs'], function() {
    //Logs Route
    Route::group(['prefix'=>'logs'], function() {
        Route::get('/', 'LogController@index');
        Route::get('/logs', 'LogController@logs');
        Route::get('/log-types', 'LogController@logTypes');
        Route::get('/users-logs/{id}', 'LogController@usersLogs');
        Route::get('/users-log-types/{id}', 'LogController@usersLogTypes');
//        Route::post('/', 'LogController@postIndex');
//        Route::get('/delete/{id}', 'LogController@delete');
    });
    //Log Types Route
    Route::group(['prefix'=>'log-types'], function() {
        Route::get('/', 'LogTypeController@index');
        Route::get('/index', 'LogTypeController@index');
        Route::post('/', 'LogTypeController@postIndex');
        Route::get('/delete/{id}', 'LogTypeController@delete');
    });
});
