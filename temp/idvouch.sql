-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: databases.ekaruztech.com
-- Generation Time: Nov 14, 2016 at 10:53 PM
-- Server version: 5.6.25-log
-- PHP Version: 7.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ekaruztech_fingerprint`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_type_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `parent_id`, `lft`, `rgt`, `depth`, `name`, `category_type_id`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 12, 0, 'VC CU', 3, '2016-10-20 15:35:48', '2016-11-13 15:21:31'),
(3, 4, 105, 108, 2, ' Department  of Leadership Studies', 1, '2016-10-20 16:14:05', '2016-11-13 15:28:52'),
(4, 76, 104, 127, 1, 'College Of Leadership Development Studies', 1, '2016-10-20 16:16:16', '2016-11-13 15:25:18'),
(13, 76, 128, 155, 1, 'College of Science and Technology', 1, '2016-10-20 17:17:29', '2016-11-13 15:25:18'),
(14, 76, 156, 157, 1, 'School Of Postgraduate Studies', 1, '2016-10-20 17:17:29', '2016-11-13 15:25:18'),
(15, 76, 14, 47, 1, 'College Of Business & Social Sciences', 1, '2016-10-20 17:17:29', '2016-11-13 15:25:18'),
(16, 76, 48, 103, 1, 'College Of Engineering', 1, '2016-10-20 17:17:29', '2016-11-13 15:25:18'),
(17, 4, 119, 126, 2, 'Department of Political Science and International Relations', 1, '2016-10-20 17:23:15', '2016-11-13 15:28:52'),
(18, 4, 109, 114, 2, 'Department  of  Languages and General Studies', 1, '2016-10-20 17:23:15', '2016-11-13 15:28:52'),
(19, 4, 115, 118, 2, 'Department  of Psychology', 1, '2016-10-20 17:23:16', '2016-11-13 15:28:52'),
(20, 16, 97, 102, 2, 'Department Of Petroleum Engineering', 1, '2016-10-20 17:23:16', '2016-11-13 15:28:53'),
(22, 19, 116, 117, 3, 'Psychology ', 1, '2016-10-20 17:25:11', '2016-11-13 15:45:14'),
(23, 16, 77, 82, 2, 'Department Of Electrical and Information Engineering', 1, '2016-10-20 17:37:55', '2016-11-13 15:28:53'),
(24, 16, 83, 96, 2, 'Department Of Mechanical Engineering', 1, '2016-10-20 17:37:55', '2016-11-13 15:28:53'),
(25, 16, 63, 76, 2, 'Department Of Civil Engineering', 1, '2016-10-20 17:37:55', '2016-11-13 15:28:53'),
(26, 18, 110, 111, 3, 'English ', 1, '2016-10-22 18:05:50', '2016-11-13 15:45:14'),
(27, 3, 106, 107, 3, 'Leadership ', 1, '2016-10-22 18:05:50', '2016-11-13 15:45:13'),
(28, 18, 112, 113, 3, 'French', 1, '2016-10-22 18:09:17', '2016-11-13 15:45:14'),
(56, 108, 44, 45, 3, 'Sociology', 1, '2016-10-24 14:07:47', '2016-11-13 15:45:15'),
(57, 107, 40, 41, 3, 'Mass Communication', 1, '2016-10-24 14:07:47', '2016-11-13 15:45:15'),
(58, 106, 34, 35, 3, 'Economics', 1, '2016-10-24 14:07:47', '2016-11-13 15:45:14'),
(59, 106, 32, 33, 3, ' Demography and Social Statistics.', 1, '2016-10-24 14:07:47', '2016-11-13 15:45:14'),
(60, 105, 28, 29, 3, 'Marketing ', 1, '2016-10-24 14:07:47', '2016-11-13 15:45:14'),
(61, 111, 134, 135, 3, 'Biochemistry', 1, '2016-10-24 14:07:47', '2016-11-13 15:45:15'),
(62, 111, 132, 133, 3, 'Applied Biology & Biotechnology ', 1, '2016-10-24 14:07:47', '2016-11-13 15:45:15'),
(63, 25, 64, 75, 3, 'Civil Engineering', 1, '2016-10-24 14:07:47', '2016-11-13 15:45:14'),
(64, 23, 78, 79, 3, 'Electrical and Electronics Engineering', 1, '2016-10-24 14:07:47', '2016-11-13 15:45:14'),
(65, 23, 80, 81, 3, 'Information & Communication Engineering', 1, '2016-10-24 14:07:47', '2016-11-13 15:45:14'),
(66, 24, 84, 95, 3, 'Mechanical Engineering', 1, '2016-10-24 14:07:47', '2016-11-13 15:45:14'),
(67, 20, 100, 101, 3, 'Petroleum Engineering', 1, '2016-10-24 14:07:47', '2016-11-13 15:45:14'),
(68, 82, 50, 61, 3, 'Chemical Engineering', 1, '2016-10-24 14:07:47', '2016-11-13 15:45:14'),
(69, 17, 124, 125, 3, 'Political Science', 1, '2016-10-24 14:07:47', '2016-11-13 15:45:14'),
(70, 17, 122, 123, 3, 'Policy and Strategic Studies', 1, '2016-10-24 14:07:47', '2016-11-13 15:45:14'),
(73, 20, 98, 99, 3, 'Management and Information Science', 1, '2016-10-24 14:07:47', '2016-11-13 15:45:14'),
(74, 116, 146, 147, 3, 'Computer Science', 1, '2016-10-24 14:07:47', '2016-11-13 15:45:15'),
(75, 111, 136, 137, 3, 'Microbiology', 1, '2016-10-24 14:07:47', '2016-11-13 15:45:15'),
(76, NULL, 13, 158, 0, 'CU_VC', 1, '2016-11-04 23:15:26', '2016-11-13 15:21:31'),
(77, 1, 4, 5, 1, 'College Of Engineering', 3, '2016-11-04 23:21:14', '2016-11-13 15:25:18'),
(78, 1, 2, 3, 1, 'College Of Business & Social Sciences', 3, '2016-11-04 23:21:14', '2016-11-13 15:25:18'),
(79, 1, 6, 7, 1, 'College Of Leadership Development Studies', 3, '2016-11-04 23:21:14', '2016-11-13 15:25:18'),
(80, 1, 8, 9, 1, 'College of Science and Technology', 3, '2016-11-04 23:21:14', '2016-11-13 15:25:18'),
(81, 1, 10, 11, 1, 'School Of Postgraduate Studies', 3, '2016-11-04 23:21:14', '2016-11-13 15:25:18'),
(82, 16, 49, 62, 2, 'Department Of Chemical Engineering', 1, '2016-11-04 23:29:41', '2016-11-13 15:28:53'),
(83, 15, 15, 18, 2, 'Department Of Accounting', 1, '2016-11-04 23:29:41', '2016-11-13 15:28:53'),
(84, 17, 120, 121, 3, 'International Relations', 1, '2016-11-04 23:47:43', '2016-11-13 15:45:14'),
(85, 104, 20, 21, 3, 'Banking and Finance', 1, '2016-11-04 23:47:43', '2016-11-13 15:45:14'),
(86, 105, 24, 25, 3, 'Business Administration', 1, '2016-11-04 23:47:43', '2016-11-13 15:45:14'),
(87, 105, 26, 27, 3, 'Industrial Relations & Human Resource Management', 1, '2016-11-04 23:47:43', '2016-11-13 15:45:14'),
(88, 106, 36, 37, 3, 'Entrepreneurship', 1, '2016-11-04 23:47:43', '2016-11-13 15:45:15'),
(89, 83, 16, 17, 3, 'Accounting', 1, '2016-11-04 23:47:43', '2016-11-13 15:45:14'),
(90, 66, 87, 88, 4, '200L/2016', 1, '2016-11-05 17:56:20', '2016-11-13 15:28:53'),
(91, 66, 89, 90, 4, '300L/2016', 1, '2016-11-05 17:56:20', '2016-11-13 15:28:53'),
(92, 66, 93, 94, 4, '500L/2016', 1, '2016-11-05 17:56:20', '2016-11-13 15:28:53'),
(93, 63, 69, 70, 4, '300L/2016', 1, '2016-11-05 17:56:20', '2016-11-13 15:28:53'),
(94, 63, 67, 68, 4, '200L/2016', 1, '2016-11-05 17:56:20', '2016-11-13 15:28:53'),
(95, 63, 73, 74, 4, '500L/2016', 1, '2016-11-05 17:56:20', '2016-11-13 15:28:53'),
(96, 63, 71, 72, 4, '400L/2016', 1, '2016-11-05 17:56:20', '2016-11-13 15:28:53'),
(97, 63, 65, 66, 4, '100L/2016', 1, '2016-11-05 17:56:20', '2016-11-13 15:28:53'),
(98, 66, 91, 92, 4, '400L/2016', 1, '2016-11-05 17:56:20', '2016-11-13 15:28:53'),
(99, 66, 85, 86, 4, '100L/2016', 1, '2016-11-05 17:56:20', '2016-11-13 15:28:53'),
(100, NULL, 159, 160, 0, 'TESTING', 3, '2016-11-06 00:56:14', '2016-11-13 15:21:31'),
(104, 15, 19, 22, 2, 'Department Of Banking and Finance', 1, '2016-11-06 13:41:21', '2016-11-13 15:28:53'),
(105, 15, 23, 30, 2, 'Department Of Business Management', 1, '2016-11-06 13:41:22', '2016-11-13 15:28:53'),
(106, 15, 31, 38, 2, 'Department Of Economics  ', 1, '2016-11-06 13:41:22', '2016-11-13 15:28:53'),
(107, 15, 39, 42, 2, 'Department Of Mass Communication', 1, '2016-11-06 13:41:22', '2016-11-13 15:28:53'),
(108, 15, 43, 46, 2, 'Department Of Sociology', 1, '2016-11-06 13:41:22', '2016-11-13 15:28:53'),
(109, 13, 153, 154, 2, 'Department Of Physics', 1, '2016-11-06 15:48:21', '2016-11-13 15:28:52'),
(110, 13, 141, 144, 2, 'Department Of Chemistry', 1, '2016-11-06 15:53:06', '2016-11-13 15:28:52'),
(111, 13, 131, 138, 2, 'Department Of Biological Sciences', 1, '2016-11-06 15:53:06', '2016-11-13 15:28:52'),
(112, 13, 149, 150, 2, 'Department Of Estate Management', 1, '2016-11-06 15:53:06', '2016-11-13 15:28:52'),
(113, 13, 129, 130, 2, 'Department Of Architecture', 1, '2016-11-06 15:53:06', '2016-11-13 15:28:52'),
(114, 13, 151, 152, 2, 'Department Of Mathematics', 1, '2016-11-06 15:53:06', '2016-11-13 15:28:52'),
(115, 13, 139, 140, 2, 'Department Of Building Technology', 1, '2016-11-06 15:53:06', '2016-11-13 15:28:52'),
(116, 13, 145, 148, 2, 'Department Of Computer and Information Sciences', 1, '2016-11-06 15:53:06', '2016-11-13 15:28:52'),
(117, 68, 51, 52, 4, '100L/2016', 1, '2016-11-06 20:14:12', '2016-11-13 15:28:53'),
(118, 68, 53, 54, 4, '200L/2016', 1, '2016-11-06 20:15:27', '2016-11-13 15:28:53'),
(119, 68, 55, 56, 4, '300L/2016', 1, '2016-11-06 20:15:28', '2016-11-13 15:28:53'),
(120, 68, 57, 58, 4, '400L/2016', 1, '2016-11-06 20:15:28', '2016-11-13 15:28:53'),
(121, 68, 59, 60, 4, '500L/2016', 1, '2016-11-06 20:16:06', '2016-11-13 15:28:53'),
(122, 110, 142, 143, 3, 'Chemistry', 1, '2016-11-06 21:09:55', '2016-11-13 15:45:15'),
(123, NULL, 161, 260, 0, 'VC_Igbinedion University', 4, '2016-11-13 15:21:31', '2016-11-13 15:45:17'),
(124, 123, 162, 247, 1, 'College Of Arts And Social Sciences', 4, '2016-11-13 15:25:18', '2016-11-13 15:45:17'),
(125, 123, 248, 249, 1, 'College Of Business And Management Studies', 4, '2016-11-13 15:25:18', '2016-11-13 15:45:17'),
(126, 123, 250, 251, 1, 'College Of Engineering', 4, '2016-11-13 15:25:18', '2016-11-13 15:45:17'),
(127, 123, 252, 253, 1, 'College Of Law', 4, '2016-11-13 15:25:18', '2016-11-13 15:45:17'),
(128, 123, 254, 255, 1, 'College Of Health Sciences', 4, '2016-11-13 15:25:18', '2016-11-13 15:45:17'),
(129, 123, 256, 257, 1, 'College Of Pharmacy', 4, '2016-11-13 15:25:18', '2016-11-13 15:45:17'),
(130, 123, 258, 259, 1, 'College Of Natural And Applied Sciences', 4, '2016-11-13 15:25:18', '2016-11-13 15:45:17'),
(131, 124, 163, 174, 2, 'ECONOMICS & DEVELOPMENT STUDIES', 4, '2016-11-13 15:28:53', '2016-11-13 15:45:15'),
(132, 124, 175, 186, 2, 'GEOGRAPHY AND REGIONAL PLANNING', 4, '2016-11-13 15:28:53', '2016-11-13 15:45:16'),
(133, 124, 187, 198, 2, 'INTERNATIONAL RELATIONS & STRATEGIC STUDIES', 4, '2016-11-13 15:28:53', '2016-11-13 15:45:16'),
(134, 124, 199, 210, 2, 'MASS COMMUNICATION', 4, '2016-11-13 15:28:53', '2016-11-13 15:45:16'),
(135, 124, 211, 222, 2, 'POLITICAL SCIENCE & PUBLIC ADMINISTRATION', 4, '2016-11-13 15:28:53', '2016-11-13 15:45:16'),
(136, 124, 223, 234, 2, 'SOCIOLOGY & ANTHROPOLOGY', 4, '2016-11-13 15:28:53', '2016-11-13 15:45:17'),
(137, 124, 235, 246, 2, 'THEATRE ARTS', 4, '2016-11-13 15:28:53', '2016-11-13 15:45:17'),
(138, 131, 164, 165, 3, '100L/2016', 4, '2016-11-13 15:45:15', '2016-11-13 15:45:15'),
(139, 131, 166, 167, 3, '200L/2016', 4, '2016-11-13 15:45:15', '2016-11-13 15:45:15'),
(140, 131, 168, 169, 3, '300L/2016', 4, '2016-11-13 15:45:15', '2016-11-13 15:45:15'),
(141, 131, 170, 171, 3, '400L/2016', 4, '2016-11-13 15:45:15', '2016-11-13 15:45:15'),
(142, 131, 172, 173, 3, '500/2016', 4, '2016-11-13 15:45:15', '2016-11-13 15:45:15'),
(143, 132, 176, 177, 3, '100L/2016', 4, '2016-11-13 15:45:15', '2016-11-13 15:45:15'),
(144, 132, 178, 179, 3, '200L/2016', 4, '2016-11-13 15:45:15', '2016-11-13 15:45:15'),
(145, 132, 180, 181, 3, '300L/2016', 4, '2016-11-13 15:45:15', '2016-11-13 15:45:15'),
(146, 132, 182, 183, 3, '400L/2016', 4, '2016-11-13 15:45:15', '2016-11-13 15:45:15'),
(147, 132, 184, 185, 3, '500L/2016', 4, '2016-11-13 15:45:15', '2016-11-13 15:45:16'),
(148, 133, 188, 189, 3, '100L/2016', 4, '2016-11-13 15:45:16', '2016-11-13 15:45:16'),
(149, 133, 190, 191, 3, '200L/2016', 4, '2016-11-13 15:45:16', '2016-11-13 15:45:16'),
(150, 133, 192, 193, 3, '300L/2016', 4, '2016-11-13 15:45:16', '2016-11-13 15:45:16'),
(151, 133, 194, 195, 3, '400L/2016', 4, '2016-11-13 15:45:16', '2016-11-13 15:45:16'),
(152, 133, 196, 197, 3, '500L/2016', 4, '2016-11-13 15:45:16', '2016-11-13 15:45:16'),
(153, 134, 200, 201, 3, '100L/2016', 4, '2016-11-13 15:45:16', '2016-11-13 15:45:16'),
(154, 134, 202, 203, 3, '200L/2016', 4, '2016-11-13 15:45:16', '2016-11-13 15:45:16'),
(155, 134, 204, 205, 3, '300L/2016', 4, '2016-11-13 15:45:16', '2016-11-13 15:45:16'),
(156, 134, 206, 207, 3, '400L/2016', 4, '2016-11-13 15:45:16', '2016-11-13 15:45:16'),
(157, 134, 208, 209, 3, '500L/2016', 4, '2016-11-13 15:45:16', '2016-11-13 15:45:16'),
(158, 135, 212, 213, 3, '100L/2016', 4, '2016-11-13 15:45:16', '2016-11-13 15:45:16'),
(159, 135, 214, 215, 3, '200L/2016', 4, '2016-11-13 15:45:16', '2016-11-13 15:45:16'),
(160, 135, 216, 217, 3, '300L/2016', 4, '2016-11-13 15:45:16', '2016-11-13 15:45:16'),
(161, 135, 218, 219, 3, '400L/2016', 4, '2016-11-13 15:45:16', '2016-11-13 15:45:16'),
(162, 135, 220, 221, 3, '500L/2016', 4, '2016-11-13 15:45:16', '2016-11-13 15:45:16'),
(163, 136, 224, 225, 3, '100L/2016', 4, '2016-11-13 15:45:16', '2016-11-13 15:45:16'),
(164, 136, 226, 227, 3, '200L/2016', 4, '2016-11-13 15:45:16', '2016-11-13 15:45:16'),
(165, 136, 228, 229, 3, '300L/2016', 4, '2016-11-13 15:45:16', '2016-11-13 15:45:16'),
(166, 136, 230, 231, 3, '400L/2016', 4, '2016-11-13 15:45:16', '2016-11-13 15:45:16'),
(167, 136, 232, 233, 3, '500L/2016', 4, '2016-11-13 15:45:16', '2016-11-13 15:45:17'),
(168, 137, 236, 237, 3, '100L/2016', 4, '2016-11-13 15:45:17', '2016-11-13 15:45:17'),
(169, 137, 238, 239, 3, '200L/2016', 4, '2016-11-13 15:45:17', '2016-11-13 15:45:17'),
(170, 137, 240, 241, 3, '300L/2016', 4, '2016-11-13 15:45:17', '2016-11-13 15:45:17'),
(171, 137, 242, 243, 3, '400L/2016', 4, '2016-11-13 15:45:17', '2016-11-13 15:45:17'),
(172, 137, 244, 245, 3, '500L/2016', 4, '2016-11-13 15:45:17', '2016-11-13 15:45:17');

-- --------------------------------------------------------

--
-- Table structure for table `categories_users`
--

CREATE TABLE `categories_users` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories_users`
--

INSERT INTO `categories_users` (`category_id`, `user_id`) VALUES
(17, 17),
(14, 17),
(19, 17),
(3, 20),
(76, 3),
(15, 51),
(4, 51);

-- --------------------------------------------------------

--
-- Table structure for table `category_types`
--

CREATE TABLE `category_types` (
  `category_type_id` int(10) UNSIGNED NOT NULL,
  `category_type` varchar(150) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category_types`
--

INSERT INTO `category_types` (`category_type_id`, `category_type`) VALUES
(1, 'Tertiary Institutions'),
(2, 'Secondary schools'),
(3, 'Bells University Records'),
(4, 'Igbinedion University Records');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_no` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `logo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  `category_type_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`client_id`, `name`, `full_name`, `email`, `phone_no`, `address`, `logo`, `status`, `category_type_id`, `created_at`, `updated_at`) VALUES
(1, 'Covenant University', 'Covenant University ', 'cuictdirector@gmail.com', '08134435535', 'Covenant University, Ota  ', '1_logo.PNG', 1, 1, '2016-10-20 15:46:31', '2016-11-08 14:41:51'),
(2, 'God is good', 'God is Good', 'sec@gmail.com', '03093445432', 'none', NULL, 1, 2, '2016-10-24 03:32:23', '2016-10-24 03:41:39'),
(3, 'KASU', 'Kaduna State University', 'kasu@gmail.com', '08090000000', 'Sabo G.R.A, Kaduna, Kaduna State', '3_logo.jpg', 1, 1, '2016-10-30 02:12:37', '2016-10-30 02:41:47'),
(4, 'LAUTECH', 'Ladoke Akintola University', 'lautech@gmail.com', '09077777777', 'Oyo State.', '4_logo.jpg', 1, 1, '2016-11-02 04:20:27', '2016-11-02 04:20:27'),
(5, 'Covenant ', 'Covenant University ', '2@e.com', '08170177235', 'Covenant University Canaanland Ota', '5_logo.PNG', 2, 1, '2016-11-04 23:05:19', '2016-11-06 20:25:48'),
(6, 'BELLS', 'Bells University Of Technology', 'alisachinedu@gmail.com', '03093445432', 'Bells University Ota', NULL, 1, 3, '2016-11-06 00:55:42', '2016-11-06 20:30:48'),
(7, 'Igbinedion University', 'Igbinedion University Okada', 'aloma.657@yahoo.com', '7990929320', 'Igbinedion University Okada, Mission Road, Okada, Benin City, Nigeria.\r\n', '7_logo.jpg', 1, 4, '2016-11-13 15:54:18', '2016-11-13 22:57:03'),
(8, 'Igbinedion University ', 'Igbinedion University Okada, ', 'aloma.657@yahoo.com', '7990929320', 'Igbinedion University Okada, Mission Road, Okada, Benin City, Nigeria.\r\n', '8_logo.jpg', 1, 4, '2016-11-13 22:59:27', '2016-11-13 22:59:28');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `menu_id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `sequence` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `type` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`menu_id`, `parent_id`, `lft`, `rgt`, `depth`, `name`, `url`, `active`, `sequence`, `type`, `icon`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 6, 0, 'PROFILE', '#', 1, 1, 1, 'fa fa-user', '2016-09-13 04:32:12', '2016-11-08 16:20:15'),
(2, NULL, 7, 12, 0, 'USERS', '#', 1, 4, 1, 'fa fa-users', '2016-09-13 04:32:12', '2016-11-08 16:20:15'),
(3, NULL, 13, 30, 0, 'SETUPS', '#', 1, 10, 1, 'fa fa-gears', '2016-09-13 04:32:12', '2016-11-08 16:20:15'),
(4, 1, 2, 3, 1, 'VIEW', '/profiles', 1, 1, 1, 'fa fa-eye', '2016-09-13 04:39:20', '2016-11-08 15:52:39'),
(5, 1, 4, 5, 1, 'EDIT', '/profiles/edit', 1, 2, 1, 'fa fa-edit', '2016-09-13 04:39:20', '2016-11-08 15:52:39'),
(6, 2, 8, 9, 1, 'MANAGE', '/users', 1, 1, 1, 'fa fa-list-ul', '2016-09-13 04:39:21', '2016-11-08 15:52:39'),
(7, 2, 10, 11, 1, 'CREATE', '/users/create', 1, 2, 1, 'fa fa-user-plus', '2016-09-13 04:39:21', '2016-11-08 15:52:39'),
(39, NULL, 31, 38, 0, 'RECORDS', '#', 1, 5, 1, 'fa fa-list-ul', '2016-09-23 11:17:21', '2016-11-08 16:20:15'),
(40, 39, 32, 33, 1, 'MANAGE', '/records', 1, 1, 1, 'fa fa-floppy-o', '2016-09-23 11:18:13', '2016-11-08 15:52:39'),
(45, 39, 34, 35, 1, 'CREATE GROUP', '/record-groups/create', 1, 2, 1, 'fa fa-subway', '2016-10-08 18:31:44', '2016-11-08 15:52:39'),
(46, 39, 36, 37, 1, 'LIST GROUPS', '/record-groups', 1, 3, 1, 'fa fa-list-ul', '2016-10-08 18:31:44', '2016-11-08 15:52:39'),
(47, NULL, 39, 44, 0, 'CLIENT', '#', 1, 2, 1, 'fa fa-user-secret', '2016-10-19 16:59:05', '2016-11-08 16:20:15'),
(48, 47, 40, 41, 1, 'MANAGE', '/clients', 1, 1, 1, 'fa fa-th-list', '2016-10-19 17:00:43', '2016-11-08 15:52:39'),
(49, 47, 42, 43, 1, 'CREATE', '/clients/create', 1, 2, 1, 'fa fa-plus', '2016-10-19 17:00:43', '2016-11-08 15:52:39'),
(50, NULL, 45, 52, 0, 'MASTER RECORD', '#', 1, 9, 1, 'fa fa-tags', '2016-10-19 17:02:26', '2016-11-08 16:20:15'),
(51, 50, 46, 47, 1, 'USER TYPES', '/user-types', 1, 1, 1, 'fa fa-users', '2016-10-19 17:08:17', '2016-11-08 15:52:39'),
(53, 50, 48, 49, 1, 'CATEGORY TYPE', '/category-types', 1, 2, 1, 'fa fa-object-group', '2016-10-19 17:09:31', '2016-11-08 15:52:39'),
(54, 50, 50, 51, 1, 'LOG TYPE', '/log-types', 1, 3, 1, 'fa fa-check-square-o', '2016-10-19 17:10:41', '2016-11-08 15:52:39'),
(55, NULL, 53, 58, 0, 'FORM BUILDER', '#', 1, 6, 1, 'fa fa-clipboard', '2016-10-19 17:15:41', '2016-11-08 16:20:15'),
(56, 55, 54, 55, 1, 'EDIT', '/form-builders', 1, 1, 1, 'fa fa-edit', '2016-10-19 17:17:22', '2016-11-08 15:52:39'),
(57, 55, 56, 57, 1, 'PREVIEW', '/form-builders/preview', 1, 2, 1, 'fa fa-eye', '2016-10-19 17:17:23', '2016-11-08 15:52:39'),
(58, NULL, 59, 64, 0, 'ROLES', '#', 1, 7, 1, 'fa fa-table', '2016-10-19 17:19:50', '2016-11-08 16:20:15'),
(59, 58, 60, 61, 1, 'MANAGE', '/roles', 1, 1, 1, 'fa fa-th-list', '2016-10-19 17:21:29', '2016-11-08 15:52:39'),
(60, 58, 62, 63, 1, 'ASSIGN', '/roles/users', 1, 2, 1, 'fa fa-lock', '2016-10-19 17:21:29', '2016-11-08 15:52:39'),
(61, NULL, 65, 80, 0, 'CATEGORIES', '#', 1, 8, 1, 'fa fa-object-group', '2016-10-19 17:23:18', '2016-11-08 16:20:15'),
(62, 61, 66, 67, 1, 'DETAILS', '/categories', 1, 1, 1, 'fa fa-eye-slash', '2016-10-19 17:26:13', '2016-11-08 15:52:39'),
(63, 61, 68, 79, 1, 'LEVELS', '#', 1, 2, 1, 'fa fa-level-down', '2016-10-19 17:27:03', '2016-11-08 15:52:39'),
(64, 63, 69, 70, 2, 'LEVEL I', '/categories/level-1', 1, 1, 1, 'fa fa-folder-open-o', '2016-10-19 17:28:59', '2016-11-08 15:52:39'),
(65, 63, 71, 72, 2, 'LEVEL II', '/categories/level-2', 1, 2, 1, 'fa fa-envelope', '2016-10-19 17:29:53', '2016-11-08 15:52:39'),
(66, 63, 73, 74, 2, 'LEVEL III', '/categories/level-3', 1, 3, 1, 'a fa-file-text', '2016-10-19 17:29:54', '2016-11-08 15:52:39'),
(67, 63, 75, 76, 2, 'LEVEL IV', '/categories/level-4', 1, 4, 1, 'fa fa-creative-commons', '2016-10-19 17:30:44', '2016-11-08 15:52:39'),
(68, 63, 77, 78, 2, 'LEVEL V', '/categories/level-5', 1, 5, 1, 'fa fa-optin-monster', '2016-10-19 17:30:44', '2016-11-08 15:52:39'),
(69, 3, 14, 29, 1, 'MENUS', '#', 1, 1, 1, 'fa fa-table', '2016-10-19 17:33:36', '2016-11-08 15:52:40'),
(70, 69, 15, 16, 2, 'DETAILS', '/menus', 1, 1, 1, 'fa fa-eye', '2016-10-19 17:34:46', '2016-10-29 06:01:18'),
(71, 69, 17, 28, 2, 'LEVELS', '#', 1, 2, 1, 'fa fa-level-down', '2016-10-19 17:35:35', '2016-10-29 06:01:18'),
(72, 71, 18, 19, 3, 'LEVEL 1', '/menus/level-1', 1, 1, 1, 'fa fa-folder-open-o', '2016-10-19 17:37:04', '2016-10-29 06:02:47'),
(73, 71, 20, 21, 3, 'LEVEL II', '/menus/level-2', 1, 2, 1, 'fa fa-envelope', '2016-10-19 17:37:04', '2016-10-29 06:02:47'),
(74, 71, 22, 23, 3, 'LEVEL III', '/menus/level-3', 1, 3, 1, 'a fa-file-text', '2016-10-19 17:37:49', '2016-10-29 06:02:47'),
(75, 71, 24, 25, 3, 'LEVEL IV', '/menus/level-4', 1, 4, 1, 'fa fa-creative-commons', '2016-10-19 17:37:49', '2016-10-29 06:02:47'),
(76, 71, 26, 27, 3, 'LEVEL V', '/menus/level-5', 1, 5, 1, 'fa fa-optin-monster', '2016-10-19 17:38:16', '2016-10-29 06:02:47');

-- --------------------------------------------------------

--
-- Table structure for table `menus_roles`
--

CREATE TABLE `menus_roles` (
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menus_roles`
--

INSERT INTO `menus_roles` (`menu_id`, `role_id`) VALUES
(1, 2),
(1, 3),
(1, 1),
(2, 3),
(3, 3),
(4, 2),
(4, 3),
(4, 1),
(5, 2),
(5, 3),
(5, 1),
(6, 2),
(6, 3),
(7, 2),
(7, 3),
(39, 2),
(39, 3),
(40, 2),
(40, 3),
(45, 2),
(45, 3),
(45, 4),
(46, 2),
(46, 3),
(46, 4),
(2, 5),
(39, 5),
(4, 5),
(5, 5),
(6, 5),
(7, 5),
(40, 5),
(1, 4),
(39, 4),
(1, 5),
(4, 4),
(5, 4),
(6, 4),
(7, 4),
(40, 4),
(47, 3),
(47, 5),
(48, 3),
(48, 5),
(49, 3),
(49, 5),
(50, 3),
(50, 5),
(51, 3),
(53, 3),
(53, 5),
(54, 3),
(55, 3),
(55, 4),
(56, 2),
(56, 3),
(56, 5),
(56, 4),
(57, 2),
(57, 3),
(57, 5),
(57, 4),
(58, 2),
(58, 3),
(58, 5),
(59, 3),
(60, 2),
(60, 3),
(60, 5),
(61, 3),
(61, 5),
(62, 3),
(62, 5),
(63, 3),
(63, 5),
(64, 3),
(66, 3),
(67, 3),
(68, 3),
(69, 3),
(70, 3),
(71, 3),
(72, 3),
(73, 3),
(74, 3),
(75, 3),
(76, 3),
(58, 4),
(60, 4),
(65, 3),
(64, 5),
(65, 5),
(66, 5),
(67, 5),
(68, 5),
(47, 2),
(2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_09_09_094654_create_categories_table', 1),
('2016_09_09_096819_roles_and_permissions_tables', 2),
('2016_09_10_162538_create_menus_table', 2),
('2016_09_13_150116_create_records_table', 3),
('2016_09_22_173621_create_clients_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `name`, `display_name`, `description`, `user_type_id`, `created_at`, `updated_at`) VALUES
(1, 'user', 'User', 'User', 1, '2016-09-10 16:17:55', '2016-09-10 16:17:55'),
(2, 'admin', 'Admin', 'Administrator', 2, '2016-09-10 16:17:55', '2016-09-10 16:17:55'),
(3, 'developer', 'Developer', 'Developer', 3, '2016-09-10 17:15:24', '2016-09-10 17:15:24'),
(4, 'super_admin', 'Super Admin', 'Super Admin', 4, '2016-09-23 11:07:12', '2016-09-23 11:07:12'),
(5, 'owner', 'Owner', 'Owner', 5, '2016-10-09 20:24:55', '2016-10-14 21:07:04');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(30, 1),
(32, 1),
(43, 1),
(45, 1),
(46, 1),
(48, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(17, 2),
(20, 2),
(51, 2),
(1, 3),
(3, 4),
(2, 5),
(16, 5);

-- --------------------------------------------------------

--
-- Table structure for table `sub_users`
--

CREATE TABLE `sub_users` (
  `parent_id` int(10) UNSIGNED NOT NULL,
  `child_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `password` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verified` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `status` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `client_id` int(11) DEFAULT NULL,
  `verification_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type_id` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `password`, `email`, `name`, `avatar`, `verified`, `status`, `client_id`, `verification_code`, `user_type_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '$2y$10$bdAnWkRW5IvC3J0er25DSOhOcf1ut8uCWSsutcNhn2cpKzRYXESSu', 'ekaruz@gmail.com', 'ekaruztech', '1_avatar.jpg', 1, 1, NULL, NULL, 3, 'd2DsMSlwQ9ESCyTvajzRwyXw2RPW63igNdLAJxsM4xfIEJMLcZyqu1wNKXkw', '2016-09-09 08:18:43', '2016-11-10 00:06:16'),
(2, '$2y$10$trPEv1MIwC.GkbRmnctEpe5tIWQrP6eCyUBNzdrRGPT9RahZvVEyS', 'admin@gmail.com', 'Admin1', '2_avatar.jpg', 1, 1, 6, '547nKLK8A9G4zut929cVDUOa83nBdyECqBKlh6Vr', 4, 'vjUfFa9v8048oDnHW6LuLMlwz1QF8jjvS8XpATqkS6qb2Myq5s3nXcLLr09a', '2016-10-15 21:07:58', '2016-11-14 00:00:49'),
(3, '$2y$10$C3QDXfDD5OtqWqTN4T56w.ITaAGYpS83web6bZnwV5W6N0LiXtjYC', 'cuictdirector@gmail.com', 'ICT Director Covenant', '3_avatar.png', 1, 1, 1, 'V2fqXCHT0eq094NfHYXlFJNq675BUDcwl4RF9VRa', 4, 'Bfnod5BpqfAwqcRu3x4RYTyOSaF5G2bL77LGEt22CNViR6pcTQbH6J0zMN9l', '2016-10-20 16:23:20', '2016-11-14 17:11:02'),
(16, '$2y$10$bdAnWkRW5IvC3J0er25DSOhOcf1ut8uCWSsutcNhn2cpKzRYXESSu', 'benee@gmail.com', 'Ebenezer', NULL, 1, 1, NULL, '547nKLK8A9G4zut929cVDUOa83nBdyECqBKlh6Vr', 5, '8aykRhPKBsCSC6EYKfepJ5U6F9EdEJyFlu1ckxsvKrPbhswWVWdqw4IEvPxO', '2016-10-15 21:07:58', '2016-11-04 21:23:02'),
(17, '$2y$10$Uii4vSMCVeiBde8HjFDTaejCSRI.ex//2p1u.0zBwpthKNQmhB47W', 'Nouwatin@gmail.com', 'Jacob Nouwatin Sunday', NULL, 1, 1, 3, 'q4l7E3lTPSbcP4yWD86wtUOtcmPRoJXntEz31uIJ', 4, NULL, '2016-10-28 20:22:54', '2016-10-30 02:49:23'),
(20, '$2y$10$h4NJ3ZoKMQZtNLI8qQbaUOkvp3ehQybWEdGfcdNgatRC56tu2vCAK', 'zachangdawuda@gmail.com', 'Oyebiyi Oluwadunsin', NULL, 1, 1, 3, 'Z54RofAhuLM09jGkpszaVYkvjwgNwY3UdrmemY7M', 2, 'IKUHVJYnN4EskBYq1vurIKOPodmznUcdbDEP20bjpTIZNLIN7SnlHFwW3ryA', '2016-11-02 04:10:09', '2016-11-04 21:26:23'),
(30, '$2y$10$hO/l1L0oKxVLn6P7MqiqHOLRzugDFdaWrfAUIxnxnh9Mk5YxDOEPq', 'silversilverhappy@yahoo.com', 'Silver Ibeodo', NULL, 0, 1, 1, '16HThamFrQmEFguQsy3HXEkMjqLvzeaHxVt6NI6b', 1, NULL, '2016-11-06 21:12:26', '2016-11-06 21:12:26'),
(32, '$2y$10$q90rg0cYGCI3WAnSz6Jnye6z0lwW/RHf5FH/jg9iTmRt.YzZiwxke', 'dozycat001@gmail.com', 'Dozie Nnaemeka', NULL, 1, 1, 1, 'F5XgrDD6RNu9SMZdaIgmg0gUWNcyYvP7aPY6n5e1', 1, NULL, '2016-11-08 14:45:34', '2016-11-08 14:53:03'),
(43, '$2y$10$.nn37br6B3FxYDoMXEkZgueY3BzEqmPIahRXT03NNotRHA16Yo29q', 'ekaruztech@gmail.com', 'Omotayo Omotosho', NULL, 0, 1, 1, 'hVu9ORRkBye8t3FCWz43F0lCDnyfvxJfcDtbbKoM', 1, NULL, '2016-11-08 16:35:41', '2016-11-08 16:35:41'),
(45, '$2y$10$sGUgP1Z3grYJvSUw4KCpw.B2cWBdGRSDPvFFSp5ntz/HjSOu4dNRe', 'anyanwu2nneka@yahoo.com', 'Momeka Nnekka', NULL, 0, 1, 1, 'iwqsZ6UDcPQCqZovElYxINkdYoJef705fSOrpVL0', 1, NULL, '2016-11-08 16:43:36', '2016-11-08 16:43:36'),
(46, '$2y$10$f4upGlvgM8c69GhEBGRDnO6vAXZsPtLzQ9t6Xm/RCYPDpFx8rdfsy', 'anchis002@gmail.com', 'David Nwosu', NULL, 0, 1, 1, 'J9p7e8bEy9H4iyZzo8TNgfuWJVvSJrqam1Tj4Ko3', 1, NULL, '2016-11-08 16:44:20', '2016-11-08 16:44:20'),
(48, '$2y$10$XKE/DanyhLm9GfkhwxgA.uC8rs5dUnEUeE3QKrj4AkY0cLskdsPVi', 'nwosu.david01@gmail.com', 'Shaha Nwosu', NULL, 0, 1, 1, 'zAeJG55FgJQxA9IFgxls3DGdYzGB1uPjRppCmxaU', 1, NULL, '2016-11-08 16:45:58', '2016-11-08 16:45:58'),
(50, '$2y$10$VsgBlNmAnIANtWoksNnuEezr4rr28BugEA3zWtRzjfjgq5kxZgIl2', 'engr.obi.onyeka@gmail.com', 'Adesuwa Edosa', NULL, 0, 1, 1, 'TYt6tjRVynPDNcDvJjoPbIFL7VNrQAfPUvaEle7Z', 1, NULL, '2016-11-08 16:52:32', '2016-11-08 16:52:32'),
(51, '$2y$10$HMp7drjLJi8.1/2RdCcIKeAehsm/E5jKZW/T70QVAJ9xgH50hXQHa', 'anyanwu2obinna1@yahoo.com', 'Akintunde Samuel', NULL, 1, 1, 1, 'Igrzms12rhjcdIgFRwQ1ywUVFk7MMBQoK674OZPY', 1, 'LZ7nABQRqsrVZJ6odJ6is8nRBC0vkXqvjPrE0WfJy5I3fnBWn8xO6XcxS6I5', '2016-11-08 16:54:43', '2016-11-13 23:57:33'),
(52, '$2y$10$Zji7ul9qeh0/x4EiZD9sUOHOQvFbJX4FlylilhLUtg92KqupbkJnS', 'chimaclement@gmail.com', 'Clement Chime', NULL, 0, 1, 1, 'TtkiDrrJeyYcT4RRCaC6lf84KwK14RqbDclDQCHD', 1, NULL, '2016-11-08 16:59:40', '2016-11-08 16:59:40'),
(53, '$2y$10$.tORnctHQND25rIxKkxH7u8ZfySvmwpB3hDcWYS4vH0bgsNzBZ9eS', 'alisachinedu@gmail.com', 'Alisa Chinedu', NULL, 0, 1, 1, 'IVILhao333xyKsKkN3VZAswfY31b8Lk1PmvggUwF', 1, NULL, '2016-11-08 17:00:14', '2016-11-08 17:00:14'),
(54, '$2y$10$zUYb17LRMmwAvdHVAbstEueRxcsdkmVXO6PpIcx0OZGll5R23Qte2', 'chrysanders@gmail.com', 'Chrysogonus Ikechukwu', NULL, 0, 1, 1, 'MgpWLHrOaK4p3BCWepi73RPYQzf5wXhd2qCgjCXZ', 1, NULL, '2016-11-08 17:01:10', '2016-11-08 17:01:10'),
(55, '$2y$10$BXEXZx3FWnuWLYsLpBSyHeMHvU2BuhJF.K8KBnwINOUqI0W32Gn.2', 'nondefyde@gmail.com', 'Emmanuel Okafor', NULL, 0, 1, 1, 'DC2SrCBAMG4RIgujBRZdwWnJ4Dv1hWI7yXXXZb0a', 1, NULL, '2016-11-14 17:11:58', '2016-11-14 17:11:58');

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `user_type_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`user_type_id`, `user_type`, `type`) VALUES
(1, 'User', 1),
(2, 'Admin', 1),
(3, 'Developer', 3),
(4, 'Super Admin', 2),
(5, 'Owner', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `categories_parent_id_index` (`parent_id`),
  ADD KEY `categories_lft_index` (`lft`),
  ADD KEY `categories_rgt_index` (`rgt`);

--
-- Indexes for table `categories_users`
--
ALTER TABLE `categories_users`
  ADD KEY `categories_users_category_id_index` (`category_id`),
  ADD KEY `categories_users_user_id_index` (`user_id`);

--
-- Indexes for table `category_types`
--
ALTER TABLE `category_types`
  ADD PRIMARY KEY (`category_type_id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`client_id`),
  ADD KEY `category_type_id` (`category_type_id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`menu_id`),
  ADD KEY `menus_parent_id_index` (`parent_id`),
  ADD KEY `menus_lft_index` (`lft`),
  ADD KEY `menus_rgt_index` (`rgt`),
  ADD KEY `menus_sequence_index` (`sequence`),
  ADD KEY `menus_type_index` (`type`);

--
-- Indexes for table `menus_roles`
--
ALTER TABLE `menus_roles`
  ADD KEY `menus_roles_menu_id_index` (`menu_id`),
  ADD KEY `menus_roles_role_id_index` (`role_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`),
  ADD KEY `roles_user_type_id_index` (`user_type_id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `sub_users`
--
ALTER TABLE `sub_users`
  ADD KEY `sub_users_parent_id_index` (`parent_id`),
  ADD KEY `sub_users_child_id_index` (`child_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `users_user_type_id_index` (`user_type_id`),
  ADD KEY `client_id` (`client_id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`user_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=173;
--
-- AUTO_INCREMENT for table `category_types`
--
ALTER TABLE `category_types`
  MODIFY `category_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `client_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `menu_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `permission_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `user_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories_users`
--
ALTER TABLE `categories_users`
  ADD CONSTRAINT `categories_users_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `categories_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menus_roles`
--
ALTER TABLE `menus_roles`
  ADD CONSTRAINT `menus_roles_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`menu_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `menus_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`permission_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_users`
--
ALTER TABLE `sub_users`
  ADD CONSTRAINT `sub_users_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `users` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
