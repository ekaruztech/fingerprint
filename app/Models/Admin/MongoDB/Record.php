<?php

namespace App\Models\Admin\MongoDB;

use App\Models\Admin\Categories\Category;
use App\Models\Admin\Clients\Client;
use App\Models\Admin\Users\User;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Record extends Eloquent {

    protected $collection = 'records';

    protected $connection = 'mongodb';
    
    /**
     * Dates To Be Treated As Carbon Instance
     * @var array
     */
    protected $dates = ['dob'];

    /**
     * Path to the files
     */
    public $avatar_path = 'uploads/avatars/';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'client_id', 'last_name', 'first_name', 'unique_id', 'gender', 'dob', 'email', 'phone_no', 'category_id', 'subjects',
        'status', 'data', 'user_id', 'hashed_id', 'avatar'
    ];

    /**
     * Concatenate the first, last and the other names to get full names
     * @return mixed|string
     */
    public function fullNames()
    {
        return (isset($this->name)) ? ucwords($this->name) : ucwords(strtolower($this->first_name . ' ' . $this->last_name . ' ' . $this->other_name));
    }

    /**
     * Concatenate the first and last names to get full names
     * @return mixed|string
     */
    public function simpleName()
    {
        return (isset($this->name)) ? ucwords($this->name) : ucwords(strtolower($this->first_name . ' ' . $this->last_name));
    }

    /**
     * User Avatar full avatar path
     */
    public function getAvatarPath(){
        return ($this->avatar) ? $this->avatar : false;
    }
    
    /**
     * A Record belongs to a Client
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client(){
        return (isset($this->client_id) and Client::where('client_id', $this->client_id)->count() > 0) 
            ? Client::where('client_id', $this->client_id)->first() : null;
    }

    /**
     * A Record belongs to a User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return (isset($this->user_id) and User::where('user_id', $this->user_id)->count() > 0)
            ? User::where('user_id', $this->user_id)->first() : null;
    }

    /**
     * A Record belongs to a Category
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category(){
        return (isset($this->category_id) and Category::where('category_id', $this->category_id)->count() > 0)
            ? Category::where('category_id', $this->category_id)->first() : null;
    }

    /**
     * Get the categories associated with the given Record
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
//    public function categories()
//    {
//        return $this->belongsToMany('App\Models\Admin\Categories\Category', null, '_id', 'category_id');
//    }
}
