<?php

namespace App\Models\Admin\MongoDB;

use App\Models\Admin\MongoDB\Logs\Log;
use App\Models\Admin\Users\User;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class RecordGroup extends Eloquent {

    protected $collection = 'record_groups';

    protected $connection = 'mongodb';

    /**
     * A Record belongs to a Client
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return (isset($this->user_id) and User::where('user_id', (int) $this->user_id)->count() > 0)
            ? User::where('user_id', (int) $this->user_id)->first() : null;
    }

    /**
     * A Record Group Has Many Logs
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logs(){
        return (isset($this->_id)) ? Log::where('group_id', $this->_id)->get() : null;
    }
}
