<?php

namespace App\Models\Admin\MongoDB;

use App\Models\Admin\Clients\Client;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class FormBuilder extends Eloquent {

    protected $collection = 'form_builder';

    protected $connection = 'mongodb';

    /**
     * Client that the form builder belongs
     * @return mixed|string
     */
    public function client()
    {
        return (empty($this->client_id)) ? null : Client::findOrFail($this->client_id);
    }
}
