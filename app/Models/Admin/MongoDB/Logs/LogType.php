<?php

namespace App\Models\Admin\MongoDB\Logs;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class LogType extends Eloquent {

    protected $collection = 'log_types';

    protected $connection = 'mongodb';

    protected $dates = ['log_time'];

    /**
     * A Log Type Has Many Logs
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function logs(){
        return (isset($this->_id)) ? Log::where('log_type_id', $this->_id)->get() : null;
    }
}
