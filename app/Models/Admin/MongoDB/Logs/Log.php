<?php

namespace App\Models\Admin\MongoDB\Logs;

use App\Models\Admin\Clients\Client;
use App\Models\Admin\MongoDB\Record;
use App\Models\Admin\MongoDB\RecordGroup;
use App\Models\Admin\Users\User;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Log extends Eloquent {

    protected $collection = 'logs';

    protected $connection = 'mongodb';

    /**
     * A Log belongs to a Log Type
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function logType(){
        return (isset($this->log_type_id) and LogType::where('_id', $this->log_type_id)->count() > 0)
            ? LogType::where('_id', $this->log_type_id)->first() : null;
    }

    /**
     * A Log belongs to a Client
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client(){
        return (isset($this->client_id) and Client::where('client_id', $this->client_id)->count() > 0)
            ? Client::where('client_id', $this->client_id)->first() : null;
    }

    /**
     * A Log belongs to a User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return (isset($this->user_id) and User::where('user_id', $this->user_id)->count() > 0)
            ? User::where('user_id', $this->user_id)->first() : null;
    }

    /**
     * A Log belongs to a Record
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function record(){
        return (isset($this->record_id) and Record::where('_id', $this->record_id)->count() > 0)
            ? Record::where('_id', $this->record_id)->first() : null;
    }

    /**
     * A Log belongs to a Record Grouping
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group(){
        return (isset($this->group_id) and RecordGroup::where('_id', $this->group_id)->count() > 0)
            ? RecordGroup::where('_id', $this->group_id)->first() : null;
    }
}
