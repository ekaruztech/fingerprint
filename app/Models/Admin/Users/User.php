<?php

namespace App\Models\Admin\Users;

use App\Models\Admin\MongoDB\Record;
use App\Models\Admin\MongoDB\RecordGroup;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable, EntrustUserTrait, SoftDeletes;
    /**
     * The table users primary key
     * @var string
     */
    protected $primaryKey = 'user_id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    protected $dates = ['deleted_at'];

    /**
     * User Type ID
     */
    const OWNER = 5;
    const SUPER_ADMIN = 4;
    const DEVELOPER = 3;
    const ADMIN = 2;
    const USER = 1;
    
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [ 'email', 'password', 'name', 'avatar', 'user_type_id', 'client_id', 'verification_code', 'verified'];

    /**
     * Path to the files
     */
    public $avatar_path = 'uploads/avatars/';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'status', 'verified',
    ];

    /**
     * User Avatar full image path
     */
    public function getAvatarPath(){
        if($this->record() and isset($this->record()->avatar)){
            return $this->record()->avatar;
        }else{
            return ($this->avatar) ? env('AWS_PATH') . $this->avatar_path . $this->avatar : null;
        }
    }

    /**
     * Concatenate the first, last and the other names to get full names
     * @return mixed|string
     */
    public function fullNames()
    {
        return ucwords(strtolower($this->name));
    }

    /**
     * Concatenate the first and last names to get full names
     * @return mixed|string
     */
    public function simpleName()
    {
        return ucwords(strtolower($this->name));
    }
    
    /**
     * A User belongs to a User Type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userType(){
        return $this->belongsTo('App\Models\Admin\Users\UserType');
    }

    /**
     * A User belongs to a Client
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client(){
        return $this->belongsTo('App\Models\Admin\Clients\Client');
    }

    /**
     * Get the sub users associated with the given User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function subUsers()
    {
        return $this->belongsToMany('App\Models\Admin\Users\User', 'sub_users', 'parent_id', 'child_id');
    }
    
    /**
     * Get the roles associated with the given User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Models\Admin\RolesAndPermissions\Role', 'role_user', 'user_id', 'role_id');
    }

    /**
     * Get the categories associated with the given User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('App\Models\Admin\Categories\Category', 'categories_users', 'user_id', 'category_id');
    }

    /**
     * Get the record groups associated with the given user
     */
    public function recordGroups()
    {
        return (isset($this->user_id)) ? 
            ((RecordGroup::where('user_id', (int) $this->user_id)->count() > 0) 
            ? RecordGroup::where('user_id', (int) $this->user_id) : false) : false;
    }

    /**
     * Get the record associated with the given user
     */
    public function record()
    {
        return (isset($this->user_id)) ?
            ((Record::where('status', '1')->where('user_id', (int) $this->user_id)->count() > 0)
                ? Record::where('status', '1')->where('user_id', (int) $this->user_id)->first() : false) : false;
    }

    /**
     * Restore a soft-deleted model instance.
     *
     * @return bool|null
     */
    public function restore()
    {
        // If the restoring event does not return false, we will proceed with this
        // restore operation. Otherwise, we bail out so the developer will stop
        // the restore totally. We will clear the deleted timestamp and save.
        if ($this->fireModelEvent('restoring') === false) {
            return false;
        }

        $this->{$this->getDeletedAtColumn()} = null;

        // Once we have saved the model, we will fire the "restored" event so this
        // developer will do anything they need to after a restore operation is
        // totally finished. Then we will return the result of the save call.
        $this->exists = true;

        $result = $this->save();

        $this->fireModelEvent('restored', false);

        return $result;
    }
}
