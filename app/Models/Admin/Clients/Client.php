<?php

namespace App\Models\Admin\Clients;

use App\Models\Admin\MongoDB\FormBuilder;
use App\Models\Admin\MongoDB\Record;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'clients';
    /**
     * The table permissions primary key
     * @var int
     */
    protected $primaryKey = 'client_id';

    protected $dates = ['deleted_at'];

    /**
     * Path to the image
     */
    public $logo_path = 'uploads/clients/';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'full_name', 'email', 'phone_no', 'address', 'logo', 'status', 'category_type_id'
    ];

    /**
     * Client Logo full image path
     */
    public function getLogoPath(){
        return ($this->logo) ? env('AWS_PATH') . $this->logo_path . $this->logo : false;
    }

    /**
     * Get the category type associated with the given client
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categoryType()
    {
        return $this->belongsTo('App\Models\Admin\Categories\CategoryType');
    }

    /**
     * Client that has the Form Builder
     * @return mixed|string
     */
    public function formBuilder()
    {
        return (empty($this->client_id)) ? null : FormBuilder::where('client_id', $this->client_id);
    }

    /**
     * Client that has the Form Builder
     * @return mixed|string
     */
    public function records()
    {
        return (empty($this->client_id)) ? null : Record::where('status', '1')->where('client_id', $this->client_id);
    }
}
