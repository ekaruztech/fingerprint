<?php

namespace App\Models\Admin\Categories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryType extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'category_types';

    protected $dates = ['deleted_at'];
    /**
     * The table permissions primary key
     * @var int
     */
    protected $primaryKey = 'category_type_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_type'
    ];

    /**
     * disable the time stamps
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * A User Type has many Users
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function categories(){
        return $this->hasMany('App\Models\Admin\Categories\Category');
    }
}
