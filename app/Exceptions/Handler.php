<?php

namespace App\Exceptions;

use Exception;
use ErrorException;
use InvalidArgumentException;
use Illuminate\Database\QueryException;
use MongoDB\Driver\Exception\ConnectionTimeoutException;
use ReflectionException;
use Symfony\Component\Debug\Exception\FatalErrorException;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Facades\Auth;
use PDOException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        ////////////////////////////////////////////////// starts: KHEENGZ CUSTOM CODE ///////////////////////////////////////
        //Invalid Record Request Exception
        if ($exception instanceof ModelNotFoundException) {
            return response()->view('errors.custom', [
                'code'=>'304.1',
                'header'=>'Invalid Record Request',
                'message'=>'The Record You Are Looking For Does Not Exist'
            ]);
        }
        //File For Download Not Found Exception
        if ($exception instanceof FileNotFoundException){
            return response()->view('errors.custom', [
                'code'=>'501.4',
                'header'=>'File Not Found',
                'message'=>'The File You Are Looking For Or Trying To Download Does Not Exist On Our Server'
            ]);
        }
        //File For Download Not Found Exception
        if ($exception instanceof FatalErrorException){
            return response()->view('errors.custom', [
                'code'=>'503',
                'header'=>'Fatal Error',
                'message'=>'<strong>Whoops!!!</strong> Something went wrong kindly retry again<br>' . $exception->getMessage()
            ]);
        }

        //File For Download Not Found Exception
        if ($exception instanceof InvalidArgumentException){
            return response()->view('errors.custom', [
                'code'=>'207',
                'header'=>'Unexpected data found',
                'message'=>'<strong>Whoops!!!</strong> Something went wrong kindly retry again.<br>' . $exception->getMessage()
            ]);
        }
        //Query Exception
        if ($exception instanceof QueryException){
            return response()->view('errors.custom', [
                'code'=>'207',
                'header'=>'Query Exception',
                'message'=>'<strong>Whoops!!!</strong> Something went wrong kindly retry again<br>' . $exception->getMessage()
            ]);
        }
        //Logical Error Exception
        if ($exception instanceof ErrorException){
            return response()->view('errors.custom', [
                'code'=>'504.3',
                'header'=>'Critical Error',
                'message'=>'<strong>Whoops!!!</strong> Something went wrong kindly retry again<br>' . $exception->getMessage()
            ]);
        }
        // Bad Network Issues Method NotAllowed HttpException
        if ($exception instanceof MethodNotAllowedHttpException){
            return response()->view('errors.custom', [
                'code'=>'507.3',
                'header'=>'Bad or Poor Network Issues',
                'message'=>'<strong>Whoops!!!</strong> Something went wrong with your network kindly retry again and 
                    <strong>allow the page to load completely</strong><br>' . $exception->getMessage()
            ]);
        }

        // MySQL Database Connection Issues PDOException
        if ($exception instanceof PDOException){
            return response()->view('errors.custom', [
                'code'=>'509',
                'header'=>'Error establishing a database connection',
                'message'=>'<strong>Whoops!!!</strong> Something went wrong with your our server kindly retry few minutes later or 
                    <strong> Contact your systems administrator</strong><br>' . $exception->getMessage()
            ]);
        }

        // MongoDB Database Connection Issues ConnectionTimeoutException
        if ($exception instanceof ConnectionTimeoutException){
            return response()->view('errors.custom', [
                'code'=>'508',
                'header'=>'Error establishing a MongoDB database connection',
                'message'=>'<strong>Whoops!!!</strong> Something went wrong with your our server kindly retry few minutes later or 
                    <strong> Contact your systems administrator</strong><br>' . $exception->getMessage()
            ]);
        }
        // MongoDB Database Connection Issues ConnectionTimeoutException
        if ($exception instanceof ReflectionException){
            return response()->view('errors.custom', [
                'code'=>'404',
                'header'=>'404 Page Not Found',
                'message'=>'<strong>Whoops!!!</strong> We can not find the page you\'re looking for.<br/><a href="/">Return home </a><br/>
                    The link you followed may be broken, or the page may have been removed.<br/>
                    Please wait a moment and try again or use navigation side bar.<br/>' . $exception->getMessage()
            ]);
        }
        //If Token Mismatch Exception Occur i.e csrf error
        if ($exception instanceof TokenMismatchException){
            Auth::logout();
            return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
        }
        ////////////////////////////////////////////////// end: KHEENGZ CUSTOM CODE ///////////////////////////////////////
        
        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest('login');
    }
}
