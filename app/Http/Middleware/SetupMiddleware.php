<?php

namespace App\Http\Middleware;

use App\Models\Admin\Categories\Category;
use App\Models\Admin\Categories\CategoryType;
use App\Models\Admin\Clients\Client;
use Closure;

class SetupMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $categories = Category::all()->count();
        $types = CategoryType::all()->count();
        $clients = Client::all()->count();

        if ($types == 0) {
            
            $this->setFlashMessage('Warning!!! Kindly Setup the category types', 1);
            return redirect('/category-types');
        } elseif ($categories == 0){
            
            $this->setFlashMessage('Warning!!! Kindly Setup the categories', 1);
            return redirect('/categories');
        } elseif ($clients == 0){
            
            $this->setFlashMessage('Warning!!! Kindly Setup some clients', 1);
            return redirect('/clients');
        }
        
        return $next($request);
    }
}
