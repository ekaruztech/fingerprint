<?php

namespace App\Http\Controllers\Admin\Utility;

use App\Http\Controllers\Controller;
use App\Models\Admin\Categories\Category;
use App\Models\Admin\Categories\CategoryType;
use App\Models\Admin\Clients\Client;
use App\Models\Admin\MongoDB\Record;
use App\Models\Admin\RolesAndPermissions\Role;
use App\Models\Admin\Users\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    private $colors;

    /**
     * A list of colors for representing charts
     */
    public function __construct()
    {
        $this->colors = [
            '#FF0F00', '#FF6600', '#FF9E01', '#FCD202', '#F8FF01', '#B0DE09', '#04D215', '#0D8ECF', '#0D52D1', '#2A0CD0', '#8A0CCF',
            '#CD0D74', '#754DEB', '#DDDDDD', '#CCCCCC', '#999999', '#333333', '#000000'
        ];

        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $clients = Client::all()->count();
        $records = Record::where('status', '1')->count();
        $users = User::all()->count() - 1;

        if(Auth::user()->hasRole(Role::OWNER_DEV)){
            
            return view('admin.dashboards.dashboard', compact('clients', 'records', 'users'));
            
        }else if(Auth::user()->hasRole(Role::SUPER_ADMIN)) {
            $records = Record::where('status', '1')->where('client_id', Auth::user()->client_id)->count();
            $users = User::where('client_id', Auth::user()->client_id)->count();

            return view('admin.dashboards.admin', compact('clients', 'records', 'users'));
            
        }else if(Auth::user()->hasRole(Role::ADMIN)) {
            $records = Record::where('status', '1')->where('client_id', Auth::user()->client_id)->count();
            $users = User::whereIn('user_id', Auth::user()->subUsers()->pluck('user_id')->toArray())->count();

            return view('admin.dashboards.admin', compact('records', 'users'));
        }else {
            
            return view('admin.dashboards.user', compact('records', 'users'));
        }
    }

    /**
     * Gets The Records based on their clients
     * @return Response
     */
    public function clientsRecords()
    {
        $clients = Client::all();
        $response = [];
        $color = 0;
        foreach($clients as $client){
            $sum = $client->records()->get()->count();
            $response[] = array(
                'client'=>$client->name,
                'records'=>$sum,
                'color'=>$this->colors[$color++]
            );
        }
        return response()->json($response);
    }

    /**
     * Gets The Category Types
     * @return Response
     */
    public function categoryTypes()
    {
        $category_types = CategoryType::all();
        $response = [];
        $color = 0;
        foreach ($category_types as $category_type){
            $category = Category::where('category_type_id', $category_type->category_type_id)->get()->count();
            $response[] = ['label'=>$category_type->category_type, 'color'=>$this->colors[$color++], 'data'=>$category, 'value'=>$category];
        }

        return response()->json($response);
    }

    /**
     * Gets The Gender Ratio Based on Client Records
     * @return Response
     */
    public function recordGender()
    {
        $male = Record::where('status', '1')->where('client_id', Auth::user()->client_id)->where('gender', 'Male')->count();
        $female = Record::where('status', '1')->where('client_id', Auth::user()->client_id)->where('gender', 'Female')->count();
        $response[] = ['label'=>'Male', 'color'=>'#CCC', 'data'=>$male, 'value'=>$male];
        $response[] = ['label'=>'Female', 'color'=>'#3CF', 'data'=>$female, 'value'=>$female];

        return response()->json($response);
    }
}
