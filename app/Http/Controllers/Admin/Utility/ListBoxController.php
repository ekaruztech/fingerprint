<?php

namespace App\Http\Controllers\Admin\Utility;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Admin\Categories\Category;

class ListBoxController extends Controller
{
    /**
     *
     * Make sure the user is logged in
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }
    
    /**
     * Get category level based on the category parent id
     * @param int $id
     * @return Response
     */
    public function category($id)
    {
        $categories = Category::orderBy('name')->where('parent_id', $id)->get();//->pluck('name', 'category_id');
        $parent = (count($categories) > 0) ? $categories[0]->parent()->first()->name : null;

        $response['parent'] = $parent;
        $response['data'] = $categories;
        $response['flag'] = ($parent) ? 1 : 0;
        $response['level'] = (count($categories) > 0) ? $categories[0]->parent()->first()->getLevel() : 0;
        return response()->json($response);
    }
}
