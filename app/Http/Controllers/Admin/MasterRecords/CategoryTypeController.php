<?php

namespace App\Http\Controllers\Admin\MasterRecords;

use App\Models\Admin\Categories\CategoryType;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoryTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $category_types = CategoryType::all();
        return view('admin.master-records.category-types', compact('category_types'));
    }

    /**
     * Insert or Update the user type records
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postIndex(Request $request)
    {
        $inputs = $request->all();
        $count = 0;

        for($i = 0; $i < count($inputs['category_type_id']); $i++){
            $category_type = ($inputs['category_type_id'][$i] > 0) ? CategoryType::find($inputs['category_type_id'][$i]) : new CategoryType();
            $category_type->category_type = $inputs['category_type'][$i];
            $count = ($category_type->save()) ? $count+1 : '';
        }
        // Set the flash message
        $this->setFlashMessage($count . ' Category Type has been successfully updated.', 1);
        // redirect to the create a new inmate page
        return redirect('/category-types');
    }

    /**
     * Delete a User type from the list of user Types using a given user type id
     * @param $id
     */
    public function delete($id)
    {
        $category_type = CategoryType::findOrFail($id);
        //Delete The Category Type Record
        $delete = ($category_type !== null) ? $category_type->delete() : null;
        if($delete){
            //Delete its Equivalent Users Record
            $this->setFlashMessage('  Deleted!!! '.$category_type->category_type.' Category Type have been deleted.', 1);
        }else{
            $this->setFlashMessage('Error!!! Unable to delete record.', 2);
        }
    }
}
