<?php

namespace App\Http\Controllers\Admin\MasterRecords;

use App\Http\Controllers\Controller;
use App\Models\Admin\Categories\CategoryType;
use App\Models\Admin\Clients\Client;
use GrahamCampbell\Flysystem\Facades\Flysystem;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use Illuminate\Support\Facades\File;


class ClientController extends Controller
{
    /**
     * Redirects To The Record Default Page
     * @var string
     */
    protected $redirectTo = '/clients';

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'category_type_id.required' => 'Category Type is Required!',
            'name.required' => 'Short Name is Required!',
            'email.required' => 'E-Mail Address is Required!',
            'phone_no.required' => 'Mobile Number is Required!',
            'address.required' => 'Contact Address is Required!',
            'logo.mimes' => 'Client Logo must be either jpeg, bmp or png!',
            'logo.max' => 'Client Logo must not exceed 200KB!',
        ];
        return Validator::make($data, [
            'category_type_id' => 'required',
            'name' => 'required',
            'email' => 'required|email|max:255',
            'phone_no' => 'required',
            'address' => 'required',
            'logo' => 'mimes:jpeg,bmp,png|max:200',
        ], $messages);
    }

    /**
     * Display a listing of the Records.
     * @return Response
     */
    public function index()
    {
        $clients = Client::orderBy('name')->get();
        return view('admin.clients.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $category_types = CategoryType::orderBy('category_type')->get();
        return view('admin.clients.create', compact('category_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        //Validate Request Inputs
        if ($this->validator($inputs)->fails())
        {
            $this->setFlashMessage('Error!!! You have error(s) while filling the form.', 2);
            return redirect('/clients/create')->withErrors($this->validator($inputs))->withInput();
        }

        // Store the Client...
        $client = Client::create($inputs);
        if($client){
            if($request->hasFile('logo'))
            {
                $image = $client->client_id . '_logo.' . $request->file('logo')->getClientOriginalExtension();
                $request->file('logo')->move($client->logo_path, $image);

                $file = File::get($client->logo_path . $image);
                Flysystem::connection('awss3')->put($client->logo_path . $image, $file);

                //Update File Path
                $client->logo = $image;
                $client->save();
            }
            // Set the flash message
            $this->setFlashMessage('Saved!!! '.$client->name.' have successfully been created', 1);
        }
        return redirect('/clients');
    }

    /**
     * Displays the record details
     * @param String $encodeId
     * @return \Illuminate\View\View
     */
    public function show($encodeId)
    {
        $decodeId = $this->getHashIds()->decode($encodeId);
        $client = (empty($decodeId)) ? abort(305) : Client::findOrFail($decodeId[0]);
        return view('admin.clients.show', compact('client'));
    }

    /**
     * Displays the record profiles details for editing
     * @param String $encodeId
     * @return \Illuminate\View\View
     */
    public function edit($encodeId)
    {
        $decodeId = $this->getHashIds()->decode($encodeId);
        $category_types = CategoryType::orderBy('category_type')->get();
        $client = (empty($decodeId)) ? abort(305) : Client::findOrFail($decodeId[0]);

        return view('admin.clients.edit', compact('client', 'category_types'));
    }

    /**
     * Store the form for creating a new resource.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        //Keep track of selected tab
        session()->put('active', 'info');
        $inputs = $request->all();
        $client = (empty($inputs['client_id'])) ? abort(403) : Client::findOrFail($inputs['client_id']);

        //Validate Request Inputs
        if ($this->validator($inputs)->fails()) {
            $this->setFlashMessage('  Error!!! You have error(s) while filling the form.', 2);
            return redirect('/clients/'.$this->getHashIds()->encode($inputs['client_id']).'/edit')->withErrors($this->validator($inputs))->withInput();
        }
        // Update the record record
        $client->update($inputs);

        if ($client) {
            if($request->hasFile('logo'))
            {
                $image = $client->client_id . '_logo.' . $request->file('logo')->getClientOriginalExtension();
                $request->file('logo')->move($client->logo_path, $image);

                $file = File::get($client->logo_path . $image);
                Flysystem::connection('awss3')->put($client->logo_path . $image, $file);
                
                //Update File Path
                $client->logo = $image;
                $client->save();
            }
            // Set the flash message
            $this->setFlashMessage('  Updated!!! Client ' . $client->name . ' have successfully been updated.', 1);
        }
        // redirect to the create Committee page and enable the take roll call link
        return redirect('/clients');
    }

    /**
     * Change Status of a Clients Record
     * @param $id
     * @param $value
     */
    public function status($id, $value)
    {
        $client = Client::findOrFail($id);
        $client->status = $value;

        $client->save();
        $this->setFlashMessage('  Updated!!! Client '.$client->name.' Record have been successfully modified.', 1);
    }
}