<?php

namespace App\Http\Controllers\Admin\MasterRecords;

use App\Models\Admin\Categories\Category;
use App\Models\Admin\Categories\CategoryType;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the Level One for Master Records.
     *
     * @return Response
     */
    public function index()
    {
        $categories = Category::roots()->get();
        $max = Category::max('depth');
        return view('admin.categories.index', compact('categories', 'max'));
    }
    
    /**
     * Display a listing of the Level One for Master Records.
     *
     * @return Response
     */
    public function getLevelOne()
    {
        $category_types = CategoryType::orderBy('category_type')->pluck('category_type', 'category_type_id')->prepend('Select Category Type', '');
        $categories = Category::roots()->get();
        return view('admin.categories.level-1', compact('categories', 'category_types'));
    }

    /**
     * Insert or Update the category Level One records
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postLevelOne(Request $request)
    {
        $inputs = $request->all();
        $count = 0;

        for($i = 0; $i < count($inputs['category_id']); $i++){
            $category = ($inputs['category_id'][$i] > 0) ? Category::find($inputs['category_id'][$i]) : new Category();
            $category->name = $inputs['name'][$i];
            $category->category_type_id = $inputs['category_type_id'][$i];

            if($category->save()){
                $count = $count+1;
            }
        }
        // Set the flash message
        if($count > 0)
            $this->setFlashMessage($count . ' Categories Level One has been successfully updated.', 1);
        // redirect to the create a new inmate page
        return redirect('/categories/level-1');
    }

    /**
     * Display a listing of the Level Two for Master Records.
     *
     * @return Response
     */
    public function getLevelTwo()
    {
        $level_ones = Category::roots()->get();
        $sub = Category::whereNotNull('parent_id')->count();
        $levels = Category::roots()->get()->pluck('name', 'category_id')->prepend('Select Parent Level', '');
        return view('admin.categories.level-2', compact('level_ones', 'levels', 'sub'));
    }

    /**
     * Insert or Update the category Level Two records also Assigning a Level One as the parent
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postLevelTwo(Request $request)
    {
        $inputs = $request->all();
        $count = $this->saveSubLevels($inputs);

        // Set the flash message
        if($count > 0)
            $this->setFlashMessage($count . ' Categories Level Two has been successfully updated.', 1);
        // redirect to the create a new inmate page
        return redirect('/categories/level-2');
    }

    /**
     * Display a listing of the Level Three for Master Records.
     *
     * @return Response
     */
    public function getLevelThree()
    {
        $level_ones = Category::where('depth', 0)->get();
        $level_twos = Category::where('depth', 1)->get();
        $sub = Category::where('depth', 2)->count();
        return view('admin.categories.level-3', compact('level_twos', 'level_ones', 'sub'));
    }

    /**
     * Insert or Update the category Level Three records also Assigning a Level Two as the parent
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postLevelThree(Request $request)
    {
        $inputs = $request->all();
        $count = $this->saveSubLevels($inputs);

        // Set the flash message
        if($count > 0)
            $this->setFlashMessage($count . ' Categories Level Three has been successfully updated.', 1);
        // redirect to the create a new inmate page
        return redirect('/categories/level-3');
    }

    /**
     * Display a listing of the Level Four for Master Records.
     *
     * @return Response
     */
    public function getLevelFour()
    {
        $level_twos = Category::where('depth', 1)->get();
        $level_threes = Category::where('depth', 2)->get();
        $sub = Category::where('depth', 3)->count();
        return view('admin.categories.level-4', compact('level_threes', 'sub', 'level_twos'));
    }

    /**
     * Insert or Update the category Level Four records also Assigning a Level Two as the parent
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postLevelFour(Request $request)
    {
        $inputs = $request->all();
        $count = $this->saveSubLevels($inputs);

        // Set the flash message
        if($count > 0)
            $this->setFlashMessage($count . ' Categories Level Four has been successfully updated.', 1);
        // redirect to the create a new inmate page
        return redirect('/categories/level-4');
    }

    /**
     * Display a listing of the Level Five for Master Records.
     *
     * @return Response
     */
    public function getLevelFive()
    {
        $level_threes = Category::where('depth', 2)->get();
        $level_fours = Category::where('depth', 3)->get();
        $sub = Category::where('depth', 4)->count();
        return view('admin.categories.level-5', compact('level_threes', 'sub', 'level_fours'));
    }

    /**
     * Insert or Update the category Level Five records also Assigning a Level Two as the parent
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postLevelFive(Request $request)
    {
        $inputs = $request->all();
        $count = $this->saveSubLevels($inputs);

        // Set the flash message
        if($count > 0)
            $this->setFlashMessage($count . ' Categories Level Five has been successfully updated.', 1);
        // redirect to the create a new inmate page
        return redirect('/categories/level-5');
    }

    /**
     * Delete a Category from the list of Categories using a given category id
     * @param $category_id
     */
    public function deleteCategory($category_id)
    {
        $category = Category::findOrFail($category_id);
        //Delete The Category Record
        $delete = ($category !== null) ? $category->delete() : null;
        if($delete){
            //Delete its Equivalent Users Record
            $this->setFlashMessage('  Deleted!!! '.$category->name.' Category have been deleted.', 1);
        }else{
            $this->setFlashMessage('Error!!! Unable to delete record.', 2);
        }
    }

    /**
     * Helper Method for saving sub levels of a category
     * @param mixed $inputs
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    private function saveSubLevels($inputs)
    {
        $count = 0;
        for($i = 0; $i < count($inputs['category_id']); $i++){
            $root = Category::find($inputs['parent_id'][$i]);
            //Modify Existing Sub Level
            if($inputs['category_id'][$i] > 0){
                $category = Category::find($inputs['category_id'][$i]);
                $category->name = $inputs['name'][$i];
                $category->category_type_id = $root->category_type_id;
                $category->save();
            }else{ //Create New Sub Level
                $category = Category::create(['name' => $inputs['name'][$i], 'category_type_id'=>$root->category_type_id]);
            }
            //Attach a Parent(Level) to it
            if($category->makeChildOf($root)){
                $count = $count+1;
            }
        }
        return $count;
    }
}
