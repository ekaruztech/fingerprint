<?php

namespace App\Http\Controllers\Admin\Users;

use App\Mail\UserInvitation;
use GrahamCampbell\Flysystem\Facades\Flysystem;
use Illuminate\Support\Facades\File;
use App\Models\Admin\Categories\Category;
use App\Models\Admin\Clients\Client;
use App\Models\Admin\RolesAndPermissions\Role;
use App\Models\Admin\Users\User;
use App\Models\Admin\Users\UserType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;


class UserController extends Controller
{
    /**
     * Redirects To The Users Default Page
     * @var string
     */
    protected $redirectTo = '/users';

    /**
     *
     * Make sure the user is logged in and Has Permission
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => [
            'confirm'
        ]]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'name.required' => 'Full Name is Required!',
            'email.unique' => 'This E-Mail Address Has Already Been Assigned!',
        ];
        return Validator::make($data, [
            'name' => 'required',
            'email' => 'required|email|max:255|unique:users,email',
        ], $messages);
    }

    /**
     * Display a listing of the Users.
     * @return Response
     */
    public function index()
    {
        $users = User::orderBy('name')->get();
        return view('admin.users.index', compact('users'));
    }

    /**
     * Display a listing of the Users using Ajax Datatable.
     * @return Response
     */
    public function postAllUsers()
    {

        $iTotalRecords = User::where(function ($query) {
            if (Auth::user()->userType->type == UserType::USER) {
                $query->whereIn('user_id', Auth::user()->subUsers()->pluck('user_id')->toArray());
            } elseif (Auth::user()->hasRole(Role::SUPER_ADMIN)) {
                $query->where('client_id', Auth::user()->client_id);
            }elseif (Auth::user()->hasRole(Role::OWNER)) {
                $query->whereNotIn('user_type_id', [UserType::DEVELOPER]);
            }
        })->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $q = @$_REQUEST['sSearch'];

        $users = User::orderBy('name')->where(function ($query) use ($q) {
            if (!Auth::user()->hasRole(Role::ALL_ADMIN)) {
                $query->whereIn('user_id', Auth::user()->subUsers()->pluck('user_id')->toArray());
            } elseif (Auth::user()->hasRole(Role::SUPER_ADMIN)) {
                $query->where('client_id', Auth::user()->client_id);
            } elseif (Auth::user()->hasRole(Role::OWNER)) {
                $query->whereNotIn('user_type_id', [UserType::DEVELOPER]);
            }
            //Filter by either email, name or phone number
            if (!empty($q)){
                $query->where(function ($query2) use ($q) {
                    $query2->orWhere('name', 'like', '%' . $q . '%')->orWhere('email', 'like', '%' . $q . '%');
                });
            }
        });

        // iTotalDisplayRecords = filtered result count
        $iTotalDisplayRecords = $users->get()->count();

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $i = $iDisplayStart;
        $allUsers = $users->skip($iDisplayStart)->take($iDisplayLength)->get();
        foreach ($allUsers as $user) {
            $status = ($user->status == 1)
                ? '<button value="' . $user->user_id . '" rel="2" class="btn btn-success btn-rounded btn-condensed btn-xs user_status">Deactivate</button>'
                : '<button value="' . $user->user_id . '" rel="1" class="btn btn-danger btn-rounded btn-condensed btn-xs user_status">Activate</button>';
            $delete = ($user->user_id != Auth::user()->user_id)
                ? '<button class="btn btn-danger btn-rounded btn-xs delete_user" value="' . $user->user_id . '"> <span class="fa fa-trash-o"></span></button>'
                : '<label class="label label-danger">nil</label>';
            $client = (isset($user->client_id)) ? $user->client->name : '<label class="label label-danger">nil</label>';

            $data = array(
                ($i + 1),
                $user->fullNames(),
                $client,
                $user->email,
                $user->userType()->first()->user_type,
                $status,
                '<a target="_blank" href="/users/' . $this->getHashIds()->encode($user->user_id) . '" class="btn btn-info btn-rounded btn-condensed btn-xs">
                         <span class="fa fa-eye-slash"></span>
                     </a>',
                '<a target="_blank" href="/users/' . $this->getHashIds()->encode($user->user_id) . '/edit" class="btn btn-warning btn-rounded btn-condensed btn-xs">
                         <span class="fa fa-edit"></span>
                     </a>',
                $delete
            );
            if (!$user->hasRole(Role::OWNER_DEV)) {
                $records["data"][] = $data;
                $i++;
            }else if(Auth::user()->hasRole(Role::OWNER_DEV)) {
                $records["data"][] = $data;
                $i++;
            }
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = isset($iTotalDisplayRecords) ? $iTotalDisplayRecords : $iTotalRecords;

        echo json_encode($records);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $categories = (Auth::user()->hasRole(Role::OWNER_DEV)) ? Category::all() : Auth::user()->categories()->get();
        $user_types = (Auth::user()->hasRole(Role::OWNER_DEV))
            ? UserType::orderBy('user_type')->pluck('user_type', 'user_type_id')->prepend('Select User Type', '')
            : UserType::where('type', UserType::USER)->orderBy('user_type')->pluck('user_type', 'user_type_id')->prepend('Select User Type', '');
        $clients = Client::orderBy('name')->pluck('full_name', 'client_id')->prepend('Select Client', '');
        return view('admin.users.create', compact('categories', 'user_types', 'clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        //Validate Request Inputs
        if ($this->validator($input)->fails()) {
            $this->setFlashMessage('Error!!! You have error(s) while filling the form.', 2);
            return redirect('/users/create')->withErrors($this->validator($input))->withInput();
        }

        //Set the verification code to any random 40 characters and password to random 8 characters
        $verification_code = str_random(40);
        $password = strtoupper(str_random(8));
//        $password = 'password';
        $input['verification_code'] = $verification_code;
        $input['password'] = bcrypt($password);

        // Store the User...
        $user = User::create($input);
        if (isset($user)) {
            //Attach Categories to the User
            if (isset($input['category_id'])) $user->categories()->sync($input['category_id']);
            //Assign a role to the user
            $role = Role::where('user_type_id', $input['user_type_id'])->first();
            $user->attachRole($role);
            //Add as a sub user under the current logged in user
            if (Auth::user()->hasRole(Role::SUPER_ADMIN) || Auth::user()->hasRole(Role::ADMIN))
                Auth::user()->subUsers()->attach($user->user_id);

            //Verification Mail Sending
            $content = 'Find below your login credentials for ' . env('APP_NAME') . '<br> Login: ' . $user->email . '<br> Password: ' . $password;
            $content .= '<br><br> <strong>Kindly click the link to complete your invitation</strong>';
            Mail::to($user)->send(new UserInvitation($user, $content));
            $temp = ' and a mail has been sent to '.$user->email;
        }
        // Set the flash message
        $this->setFlashMessage('Saved!!! ' . $user->fullNames() . ' have successfully been saved, ' . $temp, 1);
        return redirect('/users/create');
    }

    /**
     * Display the password change form
     * @return \Illuminate\View\View
     */
    public function change()
    {
        return view('admin.users.change-password');
    }

    /**
     * Change password form via logged in user
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postChange(Request $request)
    {
        $inputs = $request->all();
        $user = Auth::user();

        //Validate if the password match the current password
        if (!Hash::check($inputs['password'], $user->password)) {
            return redirect('/users/change')->withErrors([
                'password' => 'Warning!!! ' . $user->first_name . ', Your Old Password Credential did not match your current'
            ]);
        }
        if ($request->password_confirmation !== $request->new_password) {
            return redirect('/users/change')->withErrors([
                'password' => 'Warning!!! ' . $user->first_name . ', Your New and Confirm Password Credential did not match'
            ]);
        }
//         Store the password...
        $user->fill(['password' => bcrypt($request->new_password)])->save();
        // Set the flash message
        $this->setFlashMessage('Changed!!! ' . $user->first_name . ' Your password change was successful.', 1);
        // redirect to the create a new inmate page
        return redirect('/users/change/');
    }

    /**
     * Activate or Deactivate a User. Activate : 1, Deactivate : 0
     * @param  int $user_id
     * @param  int $status
     * @return Response
     */
    public function getStatus($user_id, $status)
    {
        $user = User::findOrFail($user_id);
        if ($user !== null) {
            $user->status = $status;
            //Save The Project
            $user->save();
            ($status === '1')
                ? $this->setFlashMessage(' Activated!!! ' . $user->fullNames() . ' have been activated.', 1)
                : $this->setFlashMessage(' Deactivated!!! ' . $user->fullNames() . ' have been deactivated.', 1);
        } else {
            $this->setFlashMessage('Error!!! Unable to perform task try again.', 2);
        }
    }

    /**
     * Displays the user profiles details
     * @param String $encodeId
     * @return \Illuminate\View\View
     */
    public function show($encodeId)
    {
        $decodeId = $this->getHashIds()->decode($encodeId);
        $userView = (empty($decodeId)) ? abort(305) : User::findOrFail($decodeId[0]);
        $record = ($userView->record()) ? $userView->record() : null;
        return view('admin.users.show', compact('userView', 'record'));
    }

    /**
     * Displays the user profiles details for editing
     * @param String $encodeId
     * @return \Illuminate\View\View
     */
    public function edit($encodeId)
    {
        $decodeId = $this->getHashIds()->decode($encodeId);
        $user = (empty($decodeId)) ? abort(305) : User::findOrFail($decodeId[0]);
        $categories = (Auth::user()->hasRole(Role::OWNER_DEV)) ? Category::all() : Auth::user()->categories()->get();
        $user_types = (Auth::user()->hasRole(Role::OWNER_DEV))
            ? UserType::orderBy('user_type')->pluck('user_type', 'user_type_id')->prepend('Select User Type', '')
            : UserType::where('type', UserType::USER)->orderBy('user_type')->pluck('user_type', 'user_type_id')->prepend('Select User Type', '');
        $clients = Client::orderBy('name')->pluck('name', 'client_id')->prepend('Select Client', '');

        return view('admin.users.edit', compact('user', 'user_types', 'categories', 'clients'));
    }

    /**
     * Store the form for creating a new resource.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        //Keep track of selected tab
        session()->put('active', 'info');

        $inputs = $request->all();
        $user = (empty($inputs['user_id'])) ? abort(403) : User::findOrFail($inputs['user_id']);
        $messages = [
            'status.required' => 'The User Status is Required!',
            'name.required' => 'Full Name is Required!',
            'email.unique' => 'This E-Mail Address Has Already Been Assigned!',
        ];
        $validator = Validator::make($inputs, [
            'name' => 'required',
            'email' => 'required|email|max:255|unique:users,email,' . $user->user_id . ',user_id',
            'status' => 'required',
        ], $messages);
        //Validate Request Inputs
        if ($validator->fails()) {
            $this->setFlashMessage('  Error!!! You have error(s) while filling the form.', 2);
            return redirect('/users/' . $this->getHashIds()->encode($inputs['user_id']) . '/edit')->withErrors($validator)->withInput();
        }
        // Update the user record
        $user->update($inputs);

        if (isset($user)) {
            //Modify The Roles Assigned
//            if (isset($inputs['role_id'])) $user->roles()->sync($inputs['role_id']);
            //Attach Categories to the User
            if (isset($inputs['category_id'])) $user->categories()->sync($inputs['category_id']);
            // Set the flash message
            $this->setFlashMessage('  Updated!!! ' . $user->fullNames() . ' have successfully been updated.', 1);
            // redirect to the create Committee page and enable the take roll call link
            return redirect('/users');
        }
    }

    /**
     * User Profile Picture Upload
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function avatar(Request $request)
    {
        $inputs = Input::all();
        if ($request->file('avatar')) {
            $file = $request->file('avatar');
            $filename = $file->getClientOriginalName();
            $img_ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));

            $user = (empty($inputs['user_id'])) ? abort(403) : User::findOrFail($inputs['user_id']);
            $user->avatar = $user->user_id . '_avatar.' . $img_ext;
            Input::file('avatar')->move($user->avatar_path, $user->avatar);

            $file = File::get($user->avatar_path . $user->avatar);
            Flysystem::connection('awss3')->put($user->avatar_path . $user->avatar, $file);

            $user->save();
            $this->setFlashMessage($user->fullNames() . '  profile picture has been successfully uploaded.', 1);
            return redirect('/users/' . $this->getHashIds()->encode($inputs['user_id']));
        }
    }

    /**
     * Delete a Users Record
     * @param $id
     */
    public function getDelete($id)
    {
        $user = User::findOrFail($id);
        //Delete The Record
        if ($user) {
            if($user->record()) $user->record()->delete();
            //Detach the user from its list of sub users
            Auth::user()->subUsers()->detach($user->user_id);
            $user->delete();
            $this->setFlashMessage('  Deleted!!! ' . $user->fullNames() . ' User have been deleted.', 1);
        } else {
            $this->setFlashMessage('Error!!! Unable to delete record.', 2);
        }
    }

    /**
     * Confirm the user and update its status to a valid authentication
     * @param String $encodeId
     * @param String $code
     * @return \Illuminate\Http\RedirectResponse
     */
    public function confirm($encodeId, $code)
    {
        $decodeId = $this->getHashIds()->decode($encodeId);
        $user = (empty($decodeId)) ? abort(305) : User::findOrFail($decodeId[0]);
        if($user->verification_code == $code){
            $user->verified = 1;
            $user->status = 1;
            $user->save();
            Auth::login($user);
            $this->setFlashMessage('Welcome!!! <i>'.$user->name.'</i>, Kindly update your profile information. Thank You', 1);
            return redirect('/profiles/edit');
        }else{
            $this->setFlashMessage('Warning!!! Kindly follow the link provided via the mail <i>.'
                . $user->email . '</i> to confirm your registration. Thank You', 2);
            return redirect('/login');
        }
    }
}