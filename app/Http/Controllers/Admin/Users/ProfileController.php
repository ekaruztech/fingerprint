<?php

namespace App\Http\Controllers\Admin\Users;

use App\Http\Controllers\Controller;
use App\Models\Admin\Categories\Category;
use App\Models\Admin\MongoDB\Record;
use GrahamCampbell\Flysystem\Facades\Flysystem;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;


class ProfileController extends Controller
{
    /**
     * Displays the user profiles details
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = Auth::user();
        $record = ($user->record()) ? $user->record() : null;
        return view('admin.profile.view', compact('user', 'record'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function edit()
    {
        $user = Auth::user();
        if(!$user) session()->put('active', 'avatar');
        $record = ($user->record()) ? $user->record() : false;
        $builder = ($user->client()->count() > 0) ? $user->client()->first()->formBuilder()->first() : false;

        if($user->client()->count() > 0){
            $categories = Category::orderBy('name')->where('depth', env('APP_CATEGORY_DEPTH'))
                ->where('category_type_id', $user->client()->first()->category_type_id)->get();
        }

        return view('admin.profile.edit', compact('user', 'record', 'builder', 'categories'));
    }

    /**
     * Update the users profile
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $inputs = $request->all();
        $user = Auth::user();
        $messages = [
            'first_name.required' => 'First Name is Required!',
            'last_name.required' => 'Last Name is Required!',
            'unique_id.unique' => 'Your Personal Identification Number is Required!',
            'gender.required' => 'Gender is Required!',
            'dob.required' => 'Date of Birth is Required!',
//            'email.required' => 'Email Address is Required!',
        ];
        $validator = Validator::make($inputs, [
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'unique_id' => 'required',
            'gender' => 'required',
            'dob' => 'required',
//            'email' => 'required|email|max:255',
        ], $messages);

        if(isset($inputs['record_id'])) {
            if ($validator->fails()) {
                $this->setFlashMessage('Error!!! You have error(s) while filling the form.', 2);
                return redirect('/profiles/edit')->withErrors($validator)->withInput();
            }

            $data = $inputs['data'];
            unset($inputs['_token']);
            unset($inputs['data']);

            // Store the Record...
            $record = Record::where('_id', $inputs['record_id'])->first();
            foreach ($inputs as $key => $value) {
                $record->$key = ($key == 'category_id' || $key == 'client_id') ? (int)$value : $value;
            }
            $record->data = $data;

            if ($record->save()) {
                $user->name = $record->simpleName();
//                $user->email = $record->email;
                $user->save();
            }
        }else{
            $user->update($inputs);
        }
        $this->setFlashMessage($user->fullNames() . ', Your Profile has been successfully updated.', 1);
        return redirect('/profiles');
    }
    
    /**
     * Profile Picture Upload
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
    */
    public function avatar(Request $request)
    {    
        if ($request->file('avatar')) {
            $file = $request->file('avatar');
            $filename = $file->getClientOriginalName();
            $img_ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));

            $user = Auth::user();
            $user->avatar = $user->user_id . '_avatar.' . $img_ext;
            Input::file('avatar')->move($user->avatar_path, $user->avatar);

            $file = File::get($user->avatar_path . $user->avatar);
            Flysystem::connection('awss3')->put($user->avatar_path . $user->avatar, $file);

            $user->save();
            $this->setFlashMessage(' Your profile picture has been successfully uploaded.', 1);
            return redirect('/profiles');
        }
    }
}