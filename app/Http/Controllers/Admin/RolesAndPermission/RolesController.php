<?php

namespace App\Http\Controllers\Admin\RolesAndPermission;

use App\Models\Admin\RolesAndPermissions\Role;
use App\Models\Admin\RolesAndPermissions\RoleUser;
use App\Models\Admin\Users\User;
use App\Models\Admin\Users\UserType;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RolesController extends Controller
{
    /**
     * Display a listing of the Roles for Master Records.
     *
     * @return Response
     */
    public function index()
    {
        $roles = Role::orderBy('name')->get();
        $user_types = UserType::orderBy('user_type')->pluck('user_type', 'user_type_id')->prepend('Select User Type', '');;

        return view('admin.roles-permissions.roles', compact('roles', 'user_types'));
    }

    /**
     * Insert or Update the Roles items records
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postIndex(Request $request)
    {
        $inputs = $request->all();
        $count = 0;

        for ($i = 0; $i < count($inputs['role_id']); $i++) {
            $role = ($inputs['role_id'][$i] > 0) ? Role::find($inputs['role_id'][$i]) : new Role();
            $role->name = $inputs['name'][$i];
            $role->display_name = $inputs['display_name'][$i];
            $role->description = $inputs['description'][$i];
            $role->user_type_id = $inputs['user_type_id'][$i];
            $role->save();
            $count++;
        }
        // Set the flash message
        if ($count > 0)
            $this->setFlashMessage($count . ' Roles has been successfully updated.', 1);
        // redirect to the modify a new user role page
        return redirect('/roles');
    }

    /**
     * Delete a Menu from the list of Role using a given role id
     * @param $id
     */
    public function delete($id)
    {
        $roles = Role::findOrFail($id);
        //Delete The Role Record
        $delete = ($roles !== null) ? $roles->delete() : null;

        if ($delete) {
            $this->setFlashMessage('  Deleted!!! ' . $roles->role . ' role have been deleted.', 1);
        } else {
            $this->setFlashMessage('Error!!! Unable to delete record.', 2);
        }
    }

    /**
     * Display a listing of the Users using Ajax Datatable.
     * @return Response
     */
    public function postAllUsers()
    {

        $iTotalRecords = User::where(function ($query) {
            if (!Auth::user()->hasRole(Role::ALL_ADMIN)) {
                $query->whereIn('user_id', Auth::user()->subUsers()->pluck('user_id')->toArray());
            } elseif (Auth::user()->hasRole(Role::SUPER_ADMIN)) {
                $query->where('client_id', Auth::user()->client_id);
            }elseif (Auth::user()->hasRole(Role::OWNER)) {
                $query->whereNotIn('user_type_id', [UserType::DEVELOPER]);
            }
        })->count();
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        
        $q = @$_REQUEST['sSearch'];
        $role_id = @$_REQUEST['search']['role_id'];
        $user_type_id = @$_REQUEST['search']['user_type_id'];

        $users = User::orderBy('name')->where(function ($query) use ($q, $role_id, $user_type_id) {
            if (!Auth::user()->hasRole(Role::ALL_ADMIN)) {
                $query->whereIn('user_id', Auth::user()->subUsers()->pluck('user_id')->toArray());
            } elseif (Auth::user()->hasRole(Role::SUPER_ADMIN)) {
                $query->where('client_id', Auth::user()->client_id);
            } elseif (Auth::user()->hasRole(Role::OWNER)) {
                $query->whereNotIn('user_type_id', [UserType::DEVELOPER]);
            }
            //Filter by either email, name or phone number
            if (!empty($q)){
                $query->where(function ($query2) use ($q) {
                    $query2->orWhere('name', 'like', '%' . $q . '%')->orWhere('email', 'like', '%' . $q . '%');
                });
            }
            //Filter by Role
            if (!empty($role_id))
                $query->whereIn('user_id', RoleUser::where('role_id', $role_id)->get(['user_id'])->toArray());
            //Filter by User Type
            if (!empty($user_type_id))
                $query->where('user_type_id', $user_type_id);
        });
        // iTotalDisplayRecords = filtered result count
        $iTotalDisplayRecords = $users->get()->count();
        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $i = $iDisplayStart;
        $allUsers = $users->skip($iDisplayStart)->take($iDisplayLength)->get();
        if (!Auth::user()->hasRole(Role::ALL_ADMIN)) {
            $roles = Role::whereIn('name', [Role::ADMIN, Role::USER])->get();
        } elseif (Auth::user()->hasRole(Role::SUPER_ADMIN)) {
            $roles = Role::whereNotIn('name', Role::OWNER_DEV)->get();
        } elseif (Auth::user()->hasRole(Role::OWNER)) {
            $roles = Role::whereNotIn('name', [Role::DEVELOPER])->get();
        }else{
            $roles = Role::all();
        }

        foreach ($allUsers as $user) {
            $select = '';
            foreach ($roles as $ro){
                $selected = ($user->roles() && in_array($ro->role_id, $user->roles()->get()->pluck('role_id')->toArray())) ? 'selected' : '';
                $select .= '<option ' . $selected . ' value="' . $ro->role_id . '">' . $ro->display_name . '</option>';
            }

            $role = '<select class="form-control selectpicker" multiple name="role_id[role'.$user->user_id.'][]">
                        '.$select.'
                    </select>
                    <input name="user_id[]" type="hidden" value="'.$user->user_id.'">';

            $data = array(
                ($i + 1),
                $user->fullNames(),
                $user->email,
                $user->userType()->first()->user_type,
                $role,
                '<a target="_blank" href="/users/' . $this->getHashIds()->encode($user->user_id) . '" class="btn btn-info btn-rounded btn-condensed btn-xs">
                         <span class="fa fa-eye-slash"></span>
                     </a>',
                '<a target="_blank" href="/users/' . $this->getHashIds()->encode($user->user_id) . '/edit" class="btn btn-warning btn-rounded btn-condensed btn-xs">
                         <span class="fa fa-edit"></span>
                     </a>'
            );
            if (!$user->hasRole(Role::OWNER_DEV)) {
                $records["data"][] = $data;
                $i++;
            }else if(Auth::user()->hasRole(Role::OWNER_DEV)) {
                $records["data"][] = $data;
                $i++;
            }
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = isset($iTotalDisplayRecords) ? $iTotalDisplayRecords : $iTotalRecords;

        echo json_encode($records);
    }

    /**
     * Display a listing of the Users and Roles for Master Records.
     * @param $encodeId
     * @return Response
     */
    public function getUsers()
    {
        if (!Auth::user()->hasRole(Role::ALL_ADMIN)) {
            $roles = Role::orderBy('display_name')->whereIn('name', [Role::ADMIN, Role::USER])->pluck('display_name', 'role_id')->prepend('Select Role', '');
            $user_types = UserType::orderBy('user_type')->whereIn('user_type_id', [UserType::ADMIN, UserType::USER])->pluck('user_type', 'user_type_id')->prepend('User Type', '');
        } elseif (Auth::user()->hasRole(Role::SUPER_ADMIN)) {
            $roles = Role::orderBy('display_name')->whereNotIn('name', Role::OWNER_DEV)->pluck('display_name', 'role_id')->prepend('Select Role', '');
            $user_types = UserType::orderBy('user_type')->whereNotIn('user_type_id', [UserType::OWNER, UserType::DEVELOPER])->pluck('user_type', 'user_type_id')->prepend('User Type', '');
        } elseif (Auth::user()->hasRole(Role::OWNER)) {
            $roles = Role::orderBy('display_name')->whereNotIn('name', [Role::DEVELOPER])->pluck('display_name', 'role_id')->prepend('Select Role', '');
            $user_types = UserType::orderBy('user_type')->whereNotIn('user_type_id', [UserType::DEVELOPER])->pluck('user_type', 'user_type_id')->prepend('User Type', '');
        }else{
            $roles = Role::orderBy('display_name')->pluck('display_name', 'role_id')->prepend('Select Role', '');
            $user_types = UserType::orderBy('user_type')->pluck('user_type', 'user_type_id')->prepend('User Type', '');
        }
        return view('admin.roles-permissions.users', compact('roles', 'user_types'));
    }

    /**
     * Display a listing of the Permissions for Master Records.
     * @param Request $request
     * @return Response
     */
    public function postUsers(Request $request)
    {
        $inputs = $request->all();
        for ($i = 0; $i < count($inputs['user_id']); $i++) {
            $user = ($inputs['user_id'][$i] > 0) ? User::find($inputs['user_id'][$i]) : null;

            if(isset($inputs['role_id']['role'.$inputs['user_id'][$i]]) and $user)
                $user->roles()->sync($inputs['role_id']['role'.$inputs['user_id'][$i]]);
        }
        // Set the flash message
        $this->setFlashMessage(' User Roles has been successfully added Modified.', 1);

        return redirect($request->fullUrl());
    }
}
