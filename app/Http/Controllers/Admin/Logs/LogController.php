<?php

namespace App\Http\Controllers\Admin\Logs;

use App\Models\Admin\MongoDB\Logs\Log;
use App\Models\Admin\MongoDB\Logs\LogType;
use App\Models\Admin\MongoDB\Record;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LogController extends Controller
{
    private $colors;

    /**
     * A list of colors for representing charts
     */
    public function __construct()
    {
        $this->colors = [
            '#FF0F00', '#FF6600', '#FF9E01', '#FCD202', '#F8FF01', '#B0DE09', '#04D215', '#0D8ECF', '#0D52D1', '#2A0CD0', '#8A0CCF',
            '#CD0D74', '#754DEB', '#DDDDDD', '#CCCCCC', '#999999', '#333333', '#000000'
        ];

        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
//        $type = LogType::pluck('_id')->toArray();
//        $records = Record::all();
//        foreach ($records as $record){
//            $no = rand(0, 3);
//            $log = new Log();
//            $log->client_id = (isset($record->client_id)) ? (int) $record->client_id : 1;
//            $log->user_id = (isset($record->user_id)) ? (int) $record->user_id : 5;
//            $log->record_id = $record->_id;
//            $log->log_type_id = $type[$no];
//            $log->save();
//        }
        $logs = Log::all();
//        dd($logs);
        return view('admin.logs.index', compact('logs'));
    }

    /**
     * Gets The Logs based on their Log Types
     * @return Response
     */
    public function logs()
    {
        $types = LogType::all();
        $response = [];
        $color = 0;
        foreach($types as $type){
            $sum = Log::where('log_type_id', $type->_id)->where(function ($query) {
                if (isset(Auth::user()->client_id)) {
                    $query->where('client_id', Auth::user()->client_id);
                }
            })->count();

            $response[] = array(
                'type'=>$type->log_type,
                'records'=>$sum,
                'color'=>$this->colors[$color++]
            );
        }
        return response()->json($response);
    }

    /**
     * Gets The Log Types by Percentage
     * @return Response
     */
    public function logTypes()
    {
        $types = LogType::all();
        $response = [];
        $color = 0;
        foreach ($types as $type){
            $log = Log::where('log_type_id', $type->_id)->where(function ($query) {
                if (isset(Auth::user()->client_id)) {
                    $query->where('client_id', Auth::user()->client_id);
                }
            })->count();
            $response[] = ['label'=>$type->log_type, 'color'=>$this->colors[$color++], 'data'=>$log, 'value'=>$log];
        }

        return response()->json($response);
    }

    /**
     * Gets The Logs based on their Log Types
     * @param $record_id
     * @return Response
     */
    public function usersLogs($record_id)
    {
        $types = LogType::all();
        $response = [];
        $color = 0;
        foreach($types as $type){
            $sum = Log::where('log_type_id', $type->_id)->where('record_id', $record_id)->count();

            $response[] = array(
                'type'=>$type->log_type,
                'records'=>$sum,
                'color'=>$this->colors[$color++]
            );
        }
        return response()->json($response);
    }

    /**
     * Gets The Log Types by Percentage
     * @param $record_id
     * @return Response
     */
    public function usersLogTypes($record_id)
    {
        $types = LogType::all();
        $response = [];
        $color = 0;
        foreach ($types as $type){
            $log = Log::where('log_type_id', $type->_id)->where('record_id', $record_id)->count();
            $response[] = ['label'=>$type->log_type, 'color'=>$this->colors[$color++], 'data'=>$log, 'value'=>$log];
        }

        return response()->json($response);
    }
}
