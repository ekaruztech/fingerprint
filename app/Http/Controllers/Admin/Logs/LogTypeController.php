<?php

namespace App\Http\Controllers\Admin\Logs;

use App\Models\Admin\MongoDB\Logs\LogType;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LogTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $log_types = LogType::all();
        return view('admin.master-records.log-types', compact('log_types'));
    }

    /**
     * Insert or Update the user type records
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postIndex(Request $request)
    {
        $inputs = $request->all();
        $count = 0;

        for($i = 0; $i < count($inputs['_id']); $i++){
            $log_type = ($inputs['_id'][$i] > 0) ? LogType::find($inputs['_id'][$i]) : new LogType();
            $log_type->log_type = $inputs['log_type'][$i];
            $count = ($log_type->save()) ? $count+1 : '';
        }
        // Set the flash message
        $this->setFlashMessage($count . ' Log Type has been successfully updated.', 1);
        // redirect to the create a new inmate page
        return redirect('/log-types');
    }

    /**
     * Delete a User type from the list of user Types using a given user type id
     * @param $id
     */
    public function delete($id)
    {
        $log_type = LogType::findOrFail($id);
        //Delete The Log Type Record
        $delete = ($log_type !== null) ? $log_type->delete() : null;
        if($delete){
            //Delete its Equivalent Users Record
            $this->setFlashMessage('  Deleted!!! '.$log_type->log_type.' Log Type have been deleted.', 1);
        }else{
            $this->setFlashMessage('Error!!! Unable to delete record.', 2);
        }
    }
}
