<?php

namespace App\Http\Controllers\Admin\Records;

use App\Http\Controllers\Controller;
use App\Mail\UserConfirm;
use App\Models\Admin\Categories\Category;
use App\Models\Admin\Clients\Client;
use App\Models\Admin\MongoDB\Record;
use App\Models\Admin\RolesAndPermissions\Role;
use App\Models\Admin\Users\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Validator;


class RecordController extends Controller
{
    /**
     * Redirects To The Record Default Page
     * @var string
     */
    protected $redirectTo = '/records';

    /**
     *
     * Make sure the user is logged in and Has Permission
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => [
            'create', 'store'
        ]]);
    }

    /**
     * Get a validator for an incoming registration request.
     * @param  array  $data
     * @param $builder
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data, $builder)
    {
        $messages = [
            'first_name.required' => 'First Name is Required!',
            'last_name.required' => 'Last Name is Required!',
            'unique_id.required' => 'Your Personal Identification Number is Required!',
            'gender.required' => 'Gender is Required!',
            'dob.required' => 'Date of Birth is Required!',
            'email.required' => 'Email Address is Required!',
//            'category_id.required'=>'Category and its Sub Categories are Required',
        ];
        $validate = [
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'unique_id' => 'required',
            'gender' => 'required',
            'dob' => 'required',
            'email' => 'required|email|max:255',
//            'category_id' => 'required',
        ];
//        foreach($builder->schema as $element){
//            if((isset($element['required']) and $element['required'])){
//                $validate = array_add($validate, 'data['.$element["name"].']',  "required");
//                $messages = array_merge($messages, array('data['.$element["name"].']'.'.required'=> $element['label'] . ' is Required'));
//            }
//        }
        return Validator::make($data, $validate, $messages);
    }

    /**
     * Display a listing of the Records.
     * @return Response
     */
    public function index()
    {

//        $files1 = scandir('avatars');
//        for ($i = 3; $i<count($files1); $i++){
//            $record = Record::where('status', '1')->where('hashed_id', (int) substr($files1[$i], 0, 10))->where('client_id', 6)->first();

//            $old = 'avatars' . DIRECTORY_SEPARATOR . $files1[$i];
//            $new = 'avatars' . DIRECTORY_SEPARATOR . 'JUPEB-2016-'.substr($record->unique_id, 11)  . '.jpg';
//            rename($old , $new);
//        }
//        dd($files1);


        $ids = [];
        $categories = Auth::user()->categories()->get();
        foreach ($categories as $category){
            foreach ($category->getDescendantsAndSelf() as $sub) {
                if (!in_array($sub->category_id, $ids))
                    $ids[] = $sub->category_id;
            }
        }
        $records = Record::orderBy('last_name')->where('status', '1')->where(function ($query) use($ids) {
//        $records = Record::orderBy('first_name')->where(function ($query) use($ids) {
            if (Auth::user()->hasRole(Role::SUPER_ADMIN)) {
                $query->where('client_id', Auth::user()->client_id);
            }
            if (!Auth::user()->hasRole(Role::ALL_ADMIN)) {
                $query->whereIn('category_id', $ids);
            }
        })->get();
        $client = (empty(Auth::user()->client_id)) ? false : Client::findOrFail(Auth::user()->client_id);
        $builder = ($client) ? $client->formBuilder()->first() : false;

        return view('admin.records.index', compact('records', 'builder'));
    }

    /**
     * Display a listing of the Records.
     * @return Response
     */
    public function export()
    {
        $ids = [];
        $categories = Auth::user()->categories()->get();
        foreach ($categories as $category){
            foreach ($category->getDescendantsAndSelf() as $sub) {
                if (!in_array($sub->category_id, $ids))
                    $ids[] = $sub->category_id;
            }
        }

        $records = Record::orderBy('first_name')->where('status', '1')->where(function ($query) use($ids) {
            if (Auth::user()->hasRole(Role::SUPER_ADMIN)) {
                $query->where('client_id', Auth::user()->client_id);
            }
            if (!Auth::user()->hasRole(Role::ALL_ADMIN)) {
                $query->whereIn('category_id', $ids);
            }
        })->get();
        $client = (empty(Auth::user()->client_id)) ? false : Client::findOrFail(Auth::user()->client_id);
        $builder = ($client) ? $client->formBuilder()->first() : false;

        return view('admin.records.export', compact('records', 'builder'));
    }
    
    /**
     * Show the form for creating a new resource.
     * @param $client_id
     * @return Response
     */
    public function create($client_id=null)
    {
        $decodeId = ($client_id == null) ? abort(404) : $this->getHashIds()->decode($client_id);
        $client = (empty($decodeId)) ? abort(305) : Client::findOrFail($decodeId[0]);
        $builder = $client->formBuilder()->first();
        $categories = Category::orderBy('name')->where('depth', env('APP_CATEGORY_DEPTH'))->where('category_type_id', $client->category_type_id)->get();

//        $user = new User();
//        $user->email = 'kheengz@gmail.com';
//        $user->verification_code = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
//        $user->user_id = 2;
//        $user->name = 'KayOh China';
//        $content = 'Kindly click the verify link to complete your registration';
//        Mail::to($user)->send(new UserConfirm($user, $content));

        return view('admin.records.create', compact('client', 'builder', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();

        $client = Client::findOrFail($inputs['client_id']);
        $builder = $client->formBuilder()->first();

        //Validate Request Inputs
        if ($this->validator($inputs, $builder)->fails())
        {
            $this->setFlashMessage('Error!!! You have error(s) while filling the form.', 2);
            return redirect('/records/create/'.$this->getHashIds()->encode($client->client_id))->withErrors($this->validator($inputs, $builder))->withInput();
        }

        //TODO Custom Bells
        if (count($inputs['subjects']) !== 3)
        {
            $this->setFlashMessage('Error!!! You have error(s) while filling the form.', 2);
            return redirect('/records/create/'.$this->getHashIds()->encode($client->client_id))
                ->withErrors(['messages'=>'Your Subject Combinations Must Be 3 (Three) Selected'])->withInput();
        }
        //Unique ID
        if (isset($inputs['unique_id']))
        {
            $check = Record::where('unique_id', (int) $inputs['unique_id'])->count();
            if($check > 0)
            {
                $this->setFlashMessage('Error!!! You have error(s) while filling the form.', 2);
                return redirect('/records/create/'.$this->getHashIds()->encode($client->client_id))
                    ->withErrors(['messages'=>'Record with I.D: '. $inputs['unique_id'] . ' already exist.'])->withInput();
            }
        }

        unset($inputs['_token']);
        if(isset($inputs['data'])){
            $data = $inputs['data'];
            unset($inputs['data']);
        }

//        dd($inputs);

        // Store the Record...
        $record = new Record();
        foreach ($inputs as $key => $value){
            $record->$key = ($key == 'category_id' || $key == 'client_id') ? (int) $value : $value;
        }
        $record->status = '0';
        $record->data = isset($data) ? $data : null;
        $password = strtoupper(str_random(8));

        if($record->save()){
            $hashed_id = $record->created_at->timestamp;
            $user = new User();
            $user->name = $record->simpleName();
            $user->email = $record->email;
            $user->user_type_id = User::USER;
            $user->client_id = $record->client_id;
            $user->verified = 0;
            $user->password = bcrypt($password);
            $user->verification_code = str_random(40);
            $user->save();

            //Assign a role to the user
            $role = Role::find(User::USER)->first();
            $user->attachRole($role);

            $record->user_id = (int) $user->user_id;
            $record->hashed_id = $hashed_id;
            $record->save();
            
            //Verification Mail Sending
            $content = 'Find below your login credentials for ' . env('APP_NAME') . '<br> Login: ' . $user->email . '<br> Password: ' . $password;
            $content .= '<br><br> Copy out this Verification Code: "<strong>' . $hashed_id .'</strong>" it will be needed at the image / finger print capturing stage';
            $content .= '<br><br> <strong>Kindly click the link to complete your registration</strong>';
            Mail::to($user)->send(new UserConfirm($user, $content));
            $temp = ' and a mail has been sent to '.$user->email;

            session()->put('registration_message', 'Registered Successfully!!! Kindly Copy Out This Code "<strong>' . $hashed_id .'</strong>"');
            // Set the flash message
            $this->setFlashMessage('Saved!!! '.$record->fullNames().' have successfully been saved, ' . $temp, 1);
        }
        return redirect('/records/create/'.$this->getHashIds()->encode($client->client_id));
    }

    /**
     * Displays the record details
     * @param String $encodeId
     * @return \Illuminate\View\View
     */
    public function show($encodeId)
    {
//        $decodeId = $this->getHashIds()->decode($encodeId);
        $record = (empty($encodeId)) ? abort(305) : Record::find($encodeId);
        return view('admin.records.show', compact('record'));
    }

    /**
     * Displays the record profiles details for editing
     * @param String $encodeId
     * @return \Illuminate\View\View
     */
    public function edit($encodeId)
    {
        $record = Record::find($encodeId);
        $builder = ($record->client()) ? $record->client()->formBuilder()->first() : null;
//        $categories = Category::orderBy('name')->where('depth', env('APP_CATEGORY_DEPTH'))
//            ->where('category_type_id', $record->client()->first()->category_type_id)->get();

        return view('admin.records.edit', compact('record', 'categories', 'builder'));
    }

    /**
     * Store the form for creating a new resource.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        //Keep track of selected tab
        session()->put('active', 'info');

        $inputs = $request->all();
//        dd($inputs);
        $record = (empty($inputs['record_id'])) ? abort(403) : Record::findOrFail($inputs['record_id']);
        $builder = ($record->client()) ? $record->client()->formBuilder()->first() : null;

        //Validate Request Inputs
        if ($this->validator($inputs, $builder)->fails()) {
            $this->setFlashMessage('  Error!!! You have error(s) while filling the form.', 2);
            return redirect('/records/'.$this->getHashIds()->encode($inputs['record_id']).'/edit')->withErrors($this->validator($inputs, $builder))->withInput();
        }
        // Update the record record
        $record->update($inputs);

        if ($record) {
            // Set the flash message
            $this->setFlashMessage('  Updated!!! ' . $record->fullNames() . ' have successfully been updated.', 1);
        }
        // redirect to the create Committee page and enable the take roll call link
        return redirect('/records');
    }

    /**
     * Delete a Records Record
     * @param $id
     */
    public function delete($id)
    {
        $record = Record::findOrFail($id);
        //Delete The Record
        if($record){
            $record->delete();
            $this->setFlashMessage('  Deleted!!! '.$record->fullNames().' Record have been deleted.', 1);
        }else{
            $this->setFlashMessage('Error!!! Unable to delete record.', 2);
        }
    }
}