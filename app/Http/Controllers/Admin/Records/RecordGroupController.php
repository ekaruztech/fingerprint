<?php

namespace App\Http\Controllers\Admin\Records;

use App\Http\Controllers\Controller;
use App\Models\Admin\Categories\Category;
use App\Models\Admin\Clients\Client;
use App\Models\Admin\MongoDB\Logs\Log;
use App\Models\Admin\MongoDB\Record;
use App\Models\Admin\MongoDB\RecordGroup;
use App\Models\Admin\RolesAndPermissions\Role;
use Carbon\Carbon;
use Faker\Provider\DateTime;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Validator;


class RecordGroupController extends Controller
{
    /**
     * Redirects To The Record Default Page
     * @var string
     */
    protected $redirectTo = '/record-groups';

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function index()
    {
        $groups = (Auth::user()->recordGroups()) ? Auth::user()->recordGroups()->get() : null;
        return view('admin.records.groups.index', compact('groups'));
    }
    
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $client = (empty(Auth::user()->client_id)) ? false : Client::findOrFail(Auth::user()->client_id);

        if($client){
            $categories = Category::orderBy('name')->where(function ($query) {
                if (Auth::user()->hasRole(Role::SUPER_ADMIN)) {
                    $query->where('category_type_id', Auth::user()->client()->first()->category_type_id);
                }
    
                if (!Auth::user()->hasRole(Role::ALL_ADMIN)) {
                    $query->whereIn('category_id', Auth::user()->categories()->get(['categories_users.category_id'])->toArray());
                }
            })->get();
            $records = Record::where('status', '1')->where('client_id', (int) $client->client_id)->get();
            $cate = array();
            foreach ($records as $record){
                if($record->category()){
                    $object = new \stdClass();
                    $object->name = $record->category()->name;
                    $object->id = $record->category()->category_id;
                    if(!in_array($object, $cate)) $cate[] = $object;
                }
            }
        }
        return view('admin.records.groups.create', compact('client', 'categories', 'cate'));
    }

    /**
     * Show the form for grouping records
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        $validate = ['title'=>'required'];
        $messages = ['title.required'=>'Group Title is Required'];
        $validate = Validator::make($inputs, $validate, $messages);

        //Validate Request Inputs
        if ($validate->fails())
        {
            $this->setFlashMessage('Error!!! You have error(s) while filling the form.', 2);
            return redirect('/records/group')->withErrors($validate)->withInput();
        }

        $record = new RecordGroup();
        $record->title = $inputs['title'];
        $record->user_id = (int) Auth::user()->user_id;
        $record->records = array_unique($inputs['records']);//$inputs['records'];

        if($record->save()){
            // Set the flash message
            $this->setFlashMessage('Saved!!! '.$record->title.' record grouping have successfully been saved', 1);
        }
        return redirect('/record-groups');
    }

    /**
     * Show the form for creating a new resource.
     * @param $encodeId
     * @return Response
     */
    public function edit($encodeId)
    {
        $group = (empty($encodeId)) ? abort(305) : RecordGroup::find($encodeId);
        $client = (empty(Auth::user()->client_id)) ? null : Client::findOrFail(Auth::user()->client_id);
        $categories = Category::orderBy('name')->where(function ($query) {
            if (Auth::user()->hasRole(Role::SUPER_ADMIN)) {
                $query->where('category_type_id', Auth::user()->client()->first()->category_type_id);
            }

            if (!Auth::user()->hasRole(Role::ALL_ADMIN)) {
                $query->whereIn('category_id', Auth::user()->categories()->get(['categories_users.category_id'])->toArray());
            }
        })->get();
        $records = Record::where('status', '1')->where('client_id', (int) $client->client_id)->get();
        $cate = array();
        foreach ($records as $record){
            if($record->category()){
                $object = new \stdClass();
                $object->name = $record->category()->name;
                $object->id = $record->category()->category_id;
                if(!in_array($object, $cate)) $cate[] = $object;
            }
        }
        return view('admin.records.groups.edit', compact('client', 'categories', 'group', 'cate'));
    }

    /**
     * Show the form for grouping records
     * @param Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $inputs = $request->all();
        $validate = ['title'=>'required'];
        $messages = ['title.required'=>'Group Title is Required'];
        $validate = Validator::make($inputs, $validate, $messages);

        //Validate Request Inputs
        if ($validate->fails())
        {
            $this->setFlashMessage('Error!!! You have error(s) while filling the form.', 2);
            return redirect('/record-groups/edit/'.$inputs['group_id'])->withErrors($validate)->withInput();
        }

        $group = RecordGroup::find($inputs['group_id']);
        $group->title = $inputs['title'];
        $group->records = array_unique($inputs['records']);

        if($group->save()){
            // Set the flash message
            $this->setFlashMessage('Saved!!! '.$group->title.' record grouping have successfully been saved', 1);
        }
        return redirect('/record-groups');
    }

    /**
     * Displays the record details
     * @param String $encodeId
     * @return \Illuminate\View\View
     */
    public function show($encodeId)
    {
        $group = (empty($encodeId)) ? abort(305) : RecordGroup::find($encodeId);

        return view('admin.records.groups.show', compact('group'));
    }

    /**
     * Delete a Record Groups
     * @param $id
     */
    public function delete($id)
    {
        $record = RecordGroup::findOrFail($id);
        //Delete The Record
        if($record){
            $record->delete();
            $this->setFlashMessage('  Deleted!!! '.$record->title.' Record Group have been deleted.', 1);
        }else{
            $this->setFlashMessage('Error!!! Unable to delete record.', 2);
        }
    }

    /**
     * Displays the record details
     * @param Int $id
     * @return \Illuminate\View\View
     */
    public function getRecordByCategory($id)
    {
        $client = (empty(Auth::user()->client_id)) ? null : Client::findOrFail(Auth::user()->client_id);
        if($client){
            $records = Record::where('status', '1')->where('client_id', (int) $client->client_id)->where('category_id', (int) $id)->get();
//            $records = Record::where('status', '1')->where('category_id', (int) $id)->get();
            $response['flag'] = (count($records) > 0) ? 1 : 0;
            $rec = null;
            foreach ($records as $record){
                $rec[] = (object) ['name'=>$record->simpleName(), 'id'=>$record->_id, 'unique'=>$record->unique_id];
            }
            $response['data'] = $rec;
        }
        return response()->json($response);
    }

    /**
     * Displays the record details
     * @return \Illuminate\View\View
     */
    public function getRecordLogs()
    {
        if (Auth::user()->hasRole(Role::USER)) {
            $logs = Log::where('user_id', ''.Auth::id())->groupBy('group_id')->get();
        }else{
            $groups = (Auth::user()->recordGroups()) ? Auth::user()->recordGroups()->get() : null;
        }
        return view('admin.records.groups.log', compact('groups', 'logs'));
    }
    /**
     * Displays the record details
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function postRecordLogs(Request $request)
    {
        $inputs = $request->all();
        $response['flag'] = false;

        if(isset($inputs['_id']) and isset($inputs['year']) and isset($inputs['month']) and isset($inputs['day'])){
            $group = RecordGroup::find($inputs['_id']);
            $date = Carbon::createFromDate($inputs['year'], $inputs['month'], $inputs['day']);//, 'jS M, Y');
            $logs = Log::where('group_id', $inputs['_id'])->whereBetween('log_time', [$date->toDateString() . ' 00:00:00', $date->toDateString() . ' 23:59:99']);
//            if(!Auth::user()->hasRole(Role::USER)){
//                $logs = Log::where('group_id', $inputs['_id'])->whereBetween('log_time', [$date->toDateString() . ' 00:00:00', $date->toDateString() . ' 23:59:99']);
//            }else{
//                $logs = Log::where('user_id', ''.Auth::id())->where('group_id', $inputs['_id'])->whereBetween('log_time', [$date->toDateString() . ' 00:00:00', $date->toDateString() . ' 23:59:99']);
//            }
            $logs2 = [];
            $rec = $rec2 = null;
            foreach ($logs->get() as $log){
                if($log->record()) {
                    $rec[] = (object) [
                        'name'=>$log->record()->simpleName(),
                        'id'=>$log->record()->_id,
                        'unique'=>$log->record()->unique_id,
                        'gender'=>$log->record()->gender,
                        'type'=>$log->logType()->log_type
                    ];
                    $logs2[] = $log->record()->_id;
                }
            }

            foreach ($group->records as $id) {
                if(!in_array($id, $logs2)) {
                    $record = Record::find($id);
                    $rec2[] = (object) [
                        'name'=>$record->simpleName(),
                        'id'=>$record->_id,
                        'gender'=>$record->gender,
                        'unique'=>$record->unique_id
                    ];
                }
            }
            $chart = [
                ['label'=>'Present', 'color'=>'#32C5D2', 'data'=>count($rec), 'value'=>count($rec)],
                ['label'=>'Absent', 'color'=>'#FF0F00', 'data'=>count($rec2), 'value'=>count($rec2)],
            ];
            
            $response['present'] = $rec;
            $response['absent'] = $rec2;
            $response['flag'] = true;
            $response['log'] = ($logs->count() > 0) ? true : false;
            $response['chart'] = $chart;
            $response['formatedDate'] = $date->format('jS, M Y');
        }
        return response()->json($response);
    }
}