<?php

namespace App\Http\Controllers\Admin\Records;

use App\Http\Controllers\Controller;
use App\Models\Admin\Categories\Category;
use App\Models\Admin\Clients\Client;
use App\Models\Admin\MongoDB\FormBuilder;
use App\Models\Admin\MongoDB\Record;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Validator;


class FormBuilderController extends Controller
{
    /**
     * Redirects To The Record Default Page
     * @var string
     */
    protected $redirectTo = '/form-builder';
    
    /**
     * Preview the form for creating a new resource.
     * @param String $encodeId
     * @return Response
     */
    public function preview($encodeId = null)
    {
        $client_id = ($encodeId === null) ? Auth::user()->client_id : $this->getHashIds()->decode($encodeId)[0];
        $client = (empty($client_id)) ? false : Client::findOrFail($client_id);
        $builder = ($client) ? $client->formBuilder()->first() : false;
//        $cate = Category::where('category_type_id', $client->category_type_id)->get();
//        $depth = 0;
//        foreach ($cate as $cat){
//            foreach ($cat->getLeaves() as $ca){
//                $depth = ($ca->getLevel() > $depth) ? $ca->getLevel() : $depth;
//            }
//        }
        $categories = Category::orderBy('name')->where('depth', env('APP_CATEGORY_DEPTH'))->where('category_type_id', $client->category_type_id)->get();
        return view('admin.records.form-builder.preview', compact('client', 'builder', 'categories'));
    }

    /**
     * Show the form for creating a new resource (Dynamic Form).
     * @param String $encodeId
     * @return Response
     */
    public function create($encodeId = null)
    {
        $client_id = ($encodeId === null) ? Auth::user()->client_id : $this->getHashIds()->decode($encodeId)[0];
        $client = Client::findOrFail($client_id);
        $builder = ($client) ? $client->formBuilder()->first() : null;
        $schema = ($builder) ? json_encode($builder->schema) : null;
        return view('admin.records.form-builder.create', compact('client', 'builder', 'schema'));
    }

    /**
     * Show the form for creating a new resource (Dynamic Form).
     * @param Request $request
     * @param Int $id
     * @return Response
     */
    public function store(Request $request, $id)
    {
        $inputs = $request->all();
        $schema = \GuzzleHttp\json_decode($inputs[0], true);
        $builder = FormBuilder::where('client_id', (int) $id)->first();
        if(!$builder){
            $builder = new FormBuilder();
        }
        $builder->schema = $schema;
        $builder->client_id = (int) $id;//Auth::user()->client_id;

        if($builder->save()) {
            // Set the flash message
            $this->setFlashMessage('Saved!!! Form Builder have successfully been saved', 1);
            return $schema;
        }else{
            $this->setFlashMessage('Error!!! System Failure, Kindly Contact your Admin', 2);
        }
    }
}