<?php

namespace App\Providers;

use App\Models\Admin\Menus\Menu;
use Hashids\Hashids;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Preload the Menu Level One
        if(Schema::hasTable('menus')){
            $menus = Menu::roots()->where('active', 1)->where('type', 1)->get();
            view()->share('active_menus', $menus);
        }
        //Set The HashIds Secret Key, Length and Possible Characters Combinations To Be Accessible to every View
        view()->share('hashIds', new Hashids(env('APP_KEY'), 15, env('APP_CHAR')));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
