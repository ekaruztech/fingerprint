<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_types', function (Blueprint $table) {
            $table->increments('user_type_id');
            $table->string('user_type', 150);
            $table->integer('type')->default(1);
        });

        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('password', 150);
            $table->string('email')->index();
            $table->string('name',150)->nullable();
            $table->string('avatar',150)->nullable();
            $table->integer('verified')->unsigned()->default(1);
            $table->integer('status')->unsigned()->default(1);
            $table->integer('client_id')->nullable()->index();
            $table->string('verification_code')->nullable();
            $table->integer('user_type_id')->unsigned()->index();
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('sub_users', function (Blueprint $table) {
            $table->integer('parent_id')->unsigned()->index();
            $table->integer('child_id')->unsigned()->index();

            $table->foreign('parent_id')->references('user_id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_users');
        Schema::dropIfExists('users');
        Schema::dropIfExists('user_types');
    }
}
