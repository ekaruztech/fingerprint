<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() 
  {

      Schema::create('category_types', function (Blueprint $table) {
          $table->increments('category_type_id');
          $table->string('category_type', 150);
      });
    
    
      Schema::create('categories', function(Blueprint $table) {
          // These columns are needed for Baum's Nested Set implementation to work.
          // Column names may be changed, but they *must* all exist and be modified
          // in the model.
          // Take a look at the model scaffold comments for details.
          // We add indexes on parent_id, lft, rgt columns by default.
          $table->increments('category_id');
          $table->integer('parent_id')->nullable()->index();
          $table->integer('lft')->nullable()->index();
          $table->integer('rgt')->nullable()->index();
          $table->integer('depth')->nullable();
  
          // Add needed columns here (f.ex: name, slug, path, etc.)
          $table->string('name', 255);
          $table->integer('category_type_id')->nullable();
    
          $table->timestamps();
      });

      Schema::create('categories_users', function (Blueprint $table) {
            $table->integer('category_id')->unsigned()->index();
            $table->foreign('category_id')->references('category_id')->on('categories')->onDelete('cascade')->onUpdate('cascade');;
    
            $table->integer('user_id')->nullable()->unsigned()->index();
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade')->onUpdate('cascade');;
      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
    public function down() {
        Schema::dropIfExists('category_types');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('categories_users');
    }

}
