<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeletedAtToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->timestamp('deleted_at')->nullable();
        });
        Schema::table('user_types', function (Blueprint $table) {
            $table->timestamp('deleted_at')->nullable();
        });
        Schema::table('roles', function (Blueprint $table) {
            $table->timestamp('deleted_at')->nullable();
        });
        Schema::table('clients', function (Blueprint $table) {
            $table->timestamp('deleted_at')->nullable();
        });
        Schema::table('categories', function (Blueprint $table) {
            $table->timestamp('deleted_at')->nullable();
        });
        Schema::table('category_types', function (Blueprint $table) {
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
        Schema::table('user_types', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
        Schema::table('roles', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
        Schema::table('category_types', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
    }
}
